package com.thinkit.cms.authen.auth;

import cn.dev33.satoken.stp.StpUtil;
import com.thinkit.cms.authen.annotation.UserSvr;
import com.thinkit.cms.authen.enums.LoginTypes;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.user.UserDetailService;

import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
public class AuthenProvide implements Authentication{

    List<UserDetailService> userDetailServices;

    public AuthenProvide(List<UserDetailService> userDetailServices){
        this.userDetailServices = userDetailServices;
    }

    public AuthenProvide(){

    }

    protected String getUserId(){
        return StpUtil.getLoginIdAsString();
    }


    protected String getClientId(){
          Object clientId = StpUtil.getExtra(UserTypeDef.USER_CLIENT.getType());
          return (clientId!=null )?clientId.toString():null;
    }

    @Override
    public Map<String,Object> authen(SecretParam secretParam) {
        PassManager psManager= passManager(secretParam);
        Map<String, Object> result = psManager.login();
        return result;
    }

    private PassManager passManager(SecretParam secretParam){
        return switch (secretParam.grantType()){
            case  PASSWORD ->  PassWordProvide.buildManager(secretParam,userDetailServices);
            case  OAUTH_CODE -> OauthProvide.buildManager(secretParam,userDetailServices);
            default -> throw new RuntimeException("can not find provided");
        };
    }


    protected UserDetailService loadUserDetailService(LoginTypes loginTypes){
        for(UserDetailService userDetailService:userDetailServices){
            UserSvr userSvr =  userDetailService.getClass().getAnnotation(UserSvr.class);
            if(userSvr!=null && userSvr.plat().equals(loginTypes)){
                return userDetailService;
            }
        }
        return null;
    }

    /**
     * passManager
     * @return
     */
    @Override
    public PassManager passManager(){
        return null;
    };
}
