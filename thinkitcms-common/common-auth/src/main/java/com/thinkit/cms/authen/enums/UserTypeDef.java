package com.thinkit.cms.authen.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum UserTypeDef {

    USER_CLIENT("USER_CLIENT", "用户类型"),

    USER_CLIENT_DETAIL("USER_CLIENT_DETAIL", "用户类型详情");

    @Getter
    private String type;
    @Getter
    private String tips;

    UserTypeDef(String type, String tips) {
        this.tips = tips;
        this.type = type;
    }
}
