package com.thinkit.cms.authen.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum LoginTypes {

    PLAT_USER("PLAT_USER", "平台管理人员登录"),

    MEM_USER("MEM_USER", "普通用户登录");

    @Getter
    private String type;
    @Getter
    private String tips;

    LoginTypes(String type, String tips) {
        this.tips = tips;
        this.type = type;
    }
}
