package com.thinkit.cms.authen.auth;

import com.thinkit.cms.user.UserDetail;

import java.util.Map;

/**
 * @author LONG
 */
public interface PassManager {
    /**
     * 登录验证
     * @return
     */
    Map<String, Object> login();

    /**
     * 获取 userDetail
     * @return
     */
    UserDetail getUserDetail();
}
