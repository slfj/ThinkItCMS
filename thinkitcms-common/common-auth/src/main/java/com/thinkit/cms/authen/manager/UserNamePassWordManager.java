package com.thinkit.cms.authen.manager;

import cn.dev33.satoken.stp.SaLoginModel;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.thinkit.cms.authen.annotation.UserSvr;
import com.thinkit.cms.authen.auth.PassManager;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.authen.constant.ConfConst;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.authen.model.LoginModel;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.user.UserDetail;
import com.thinkit.cms.user.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.nio.charset.Charset;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
public class UserNamePassWordManager implements PassManager {


    private String privateKey= CmsConf.get(ConfConst.privateKey);
    private String publicKey= CmsConf.get(ConfConst.publicKey);

    private SecretParam secretParam;

    private UserDetailService userDetailService;

    private UserDetail userDetail;

    public UserNamePassWordManager(SecretParam secretParam,UserDetailService userDetailService){
        this.secretParam = secretParam;
        this.userDetailService = userDetailService;
    }

    @Override
    public Map<String, Object> login() {
        //公钥加密，私钥解密
        RSA rsa = new RSA(privateKey,publicKey);
        LoginModel loginModel = secretParam.getLoginParam();
        if(loginModel==null || StringUtils.isBlank(loginModel.getUserName()) || StringUtils.isBlank(loginModel.getPw())){
            throw new AuthenException(403,"请输入正确的账号或者密码!");
        }
        if(loginModel==null || StringUtils.isBlank(loginModel.getVerifyUid()) || StringUtils.isBlank(loginModel.getVerifyCode())){
            throw new AuthenException(403,"请输入正确的验证码!");
        }
        byte[] pwByte = rsa.decrypt(loginModel.getPw(), KeyType.PrivateKey);
        byte[] unameByte = rsa.decrypt(loginModel.getUserName(), KeyType.PrivateKey);
        String userName = new String(unameByte, Charset.forName("UTF-8"));
        String pw = new String(pwByte, Charset.forName("UTF-8"));
        String verifyCode = loginModel.getVerifyCode(),verifyUid = loginModel.getVerifyUid();
        boolean isLegal = userDetailService.checkVerifyCode(verifyUid,verifyCode);
        if(!isLegal){
            log.error("验证码不正确或已过期");
            throw new AuthenException(403,"验证码不正确或已过期!");
        }
        UserDetail userDetail = userDetailService.loadUserName(userName);
        if(userDetail==null){
            throw new AuthenException(403,"账号或者密码不正确!");
        }
        if(userDetail.lock(ConfConst.lockStatus)){
            log.error("用户被锁定");
            throw new AuthenException(403,"用户已经被禁用,请联系管理员!");
        }
        String password = SecureUtil.md5(userName+pw+privateKey);
        if(!userDetail.passCk(password)){
            log.error("用户或账号密码不对");
            throw new AuthenException(403,"账号或者密码不正确!");
        }
        boolean allowLoginNoRole = Boolean.valueOf(CmsConf.get(SysConst.allowLoginNoRole));
        if(!allowLoginNoRole){
            if(userDetail.roles().equals(0)){
                log.error("用户不存在任何角色");
                throw new AuthenException(403,"请联系管理员给当前用户分配角色后登录!");
            }
        }
        String loginId = userDetail.getId();
        UserSvr userSvr =userDetailService.getClass().getAnnotation(UserSvr.class);
        SaLoginModel saLoginModel = new SaLoginModel();
        saLoginModel.setExtra(UserTypeDef.USER_CLIENT.getType(), userSvr.plat().getType());
        saLoginModel.setExtra(UserTypeDef.USER_CLIENT_DETAIL.getType(),userDetail);
        StpUtil.login(loginId,saLoginModel);
        this.userDetail = userDetail;
        SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
        Map<String, Object> result = new HashMap<>(16);
        result.put(SysConst.token,tokenInfo.tokenValue);
        userDetailService.destroyVerifyCode(verifyUid,verifyCode);
        return result;
    }

    @Override
    public UserDetail getUserDetail() {
        return this.userDetail;
    }

    public static void main(String[] args) {
        RSA rsa = new RSA();
        PrivateKey privateKey = rsa.getPrivateKey();
        System.out.println("私钥:"+rsa.getPrivateKeyBase64());
//获得公钥
        PublicKey publicKey= rsa.getPublicKey();
        System.out.println("公钥:"+rsa.getPublicKeyBase64());
    }
}
