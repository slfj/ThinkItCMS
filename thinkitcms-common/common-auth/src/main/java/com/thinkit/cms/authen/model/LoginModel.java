package com.thinkit.cms.authen.model;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class LoginModel {

    /**
     * 账户
     */
    private String userName;

    /**
     * 密码
     */
    private String pw;

    /**
     * 验证码
     */
    private String verifyCode;


    /**
     * uid
     */
    private String verifyUid;

    /**
     *
     */
    private String uniqueId;

}
