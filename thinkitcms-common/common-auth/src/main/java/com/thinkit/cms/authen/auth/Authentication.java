package com.thinkit.cms.authen.auth;

import com.thinkit.cms.authen.secret.SecretParam;

import java.util.Map;

/**
 * 认证接口
 * @author LONG
 */
public interface Authentication {

    /**
     * 登录认证
     * @param secretParam
     * @return
     */
    Map<String,Object> authen(SecretParam secretParam);


    /**
     * 获取manager
     * @return
     */
    PassManager passManager();
}
