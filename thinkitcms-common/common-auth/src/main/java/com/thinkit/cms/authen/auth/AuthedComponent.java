package com.thinkit.cms.authen.auth;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import com.thinkit.cms.authen.enums.LoginTypes;
import com.thinkit.cms.authen.secret.SecretParam;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.user.UserDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author LONG
 */
@Slf4j
@Component
public class AuthedComponent extends AuthenProvide  implements StpInterface {


    @Autowired
    private RedisTemplate redisTemplate;

    public AuthedComponent(List<UserDetailService> userDetailServices) {
        super(userDetailServices);
    }

    public Map<String,Object> login(SecretParam secretParam){
        return authen(secretParam);
    }

    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        String clientId = getClientId();
        if(clientId==null){
            log.error("clientId 解析失败：返回空的权限组");
            return new ArrayList<>();
        }
        UserDetailService userDetailService = loadUserDetailService(LoginTypes.valueOf(clientId));
        if(userDetailService!=null){
            return userDetailService.getPermissionList(String.valueOf(loginId),loginType);
        }
        return new ArrayList<>();
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        String clientId = getClientId();
        if(clientId==null){
            log.error("clientId 解析失败：返回空的角色组");
            return new ArrayList<>();
        }
        UserDetailService userDetailService = loadUserDetailService(LoginTypes.valueOf(clientId));
        if(userDetailService!=null){
            return userDetailService.getRoleList(loginId,loginType);
        }
        return new ArrayList<>();
    }

    public Map<String, Object> verifyCode() {
        Map<String, Object> params = new HashMap<>(16);
        String verifyUid = UUID.randomUUID().toString();
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 20);
        String code = captcha.getCode();
        log.info(" login JSON: UUID and Captcha:{},{}", verifyUid, code);
        redisTemplate.opsForValue().set((SysConst.verifyScope + verifyUid), code, 300L, TimeUnit.SECONDS);
        params.put(SysConst.verifyUid, verifyUid);
        params.put(SysConst.verifyCode, captcha.getImageBase64Data());
        return params;

    }
}
