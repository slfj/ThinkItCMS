package com.thinkit.cms.authen.annotation;
import com.thinkit.cms.authen.enums.GrantTypes;
import com.thinkit.cms.authen.enums.LoginTypes;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author LONG
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserSvr {

	LoginTypes plat() default LoginTypes.PLAT_USER;

	GrantTypes grant() default GrantTypes.PASSWORD;
}
