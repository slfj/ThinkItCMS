package com.thinkit.cms.authen.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum GrantTypes {

    PASSWORD("PASSWORD", "密码模式"),

    OAUTH_CODE("OAUTH_CODE", "授权码模式");

    @Getter
    private String type;
    @Getter
    private String tips;

    GrantTypes(String type, String tips) {
        this.tips = tips;
        this.type = type;
    }
}
