package com.thinkit.cms.netio.clients;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import com.thinkit.cms.annotation.Client;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.netio.callback.QiNiuYunCallBack;
import com.thinkit.cms.netio.callback.UploadCallBack;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@Data
@Client(value = "qiNiuyunOss")
public class QiNiuYunOssClient implements OssClient{

    private UploadManager  ossClient;

    private BucketManager bucketManager;

    private UploadCallBack uploadCallBack;

    private String accessKeyId;

    private String accessKeySecret;

    private String endpoint;
    private Auth auth;

    private static final String token = "token:qiniyun:";

    private StringRedisTemplate stringRedisTemplate = SpringUtil.getBean(StringRedisTemplate.class);

    @Override
    public OssClient build(String endpoint, String accessKeyId, String accessKeySecret) {
        if(Checker.BeNull(this.ossClient)){
            Configuration cfg = new Configuration(Region.region1());
            cfg.useHttpsDomains = CmsConf.getAsBoolean(SysConst.enableSSL);
            UploadManager uploadManager = new UploadManager(cfg);
            this.ossClient = uploadManager;
            this.accessKeyId = accessKeyId;
            this.accessKeySecret = accessKeySecret;
            this.endpoint = endpoint;
            this.auth = Auth.create(accessKeyId, accessKeySecret);
            this.bucketManager = new BucketManager(auth, cfg);
        }
        if(Checker.BeNull(this.uploadCallBack)){
            this.uploadCallBack = new QiNiuYunCallBack();
        }
        return this;
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes,String siteId) {
        Response response=null;
        String reNameFile = getReName(objectName);
        try {
              response = ossClient.put(bytes, reNameFile, token(bucketName));
              Map<String, Object> objectMap = params(response,bucketName,objectName,reNameFile,bytes.length);
              objectMap.put(SysConst.siteId,siteId);
              uploadCallBack.callBack(true,objectMap);
              return CmsResult.result(getFileUrl(objectMap));
        } catch (QiniuException e) {
              log.error("文件上传异常:"+e.getMessage());
              uploadCallBack.callBack(false,params(null,bucketName,objectName,reNameFile,bytes.length));
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, InputStream inputStream,long size,String siteId) {
        Response response=null;
        String reNameFile = getReName(objectName);
        try {
            response= ossClient.put( inputStream,reNameFile,token(bucketName),null,null);
            Map<String, Object> objectMap = params(response,bucketName,objectName,reNameFile,size);
            objectMap.put(SysConst.siteId,siteId);
            uploadCallBack.callBack(true,objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (QiniuException e) {
            log.error("文件上传异常:"+e.getMessage());
            uploadCallBack.callBack(false,params(null,bucketName,objectName,reNameFile,size));
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult uploadFile(Chunk chunk) {
        return null;
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes, UploadCallBack callBack,String siteId) {
        Response response=null;
        String reNameFile = getReName(objectName);
        try {
            response = ossClient.put(bytes, reNameFile, token(bucketName));
            Map<String, Object> objectMap = params(response,bucketName,objectName,reNameFile,bytes.length);
            objectMap.put(SysConst.siteId,siteId);
            callBack.callBack(true,objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (QiniuException e) {
            log.error("文件上传异常:{}"+e.getMessage());
            callBack.callBack(false,params(null,bucketName,objectName,reNameFile,bytes.length));
        }
        return null;
    }

    @Override
    public CmsResult deleteFile(String bucketName, String objectName,String siteId) {
        try {
            Response response = bucketManager.delete(bucketName, objectName);
            Map<String, Object> objectMap = params(response,bucketName,objectName,null,0);
            uploadCallBack.callBack(true,objectMap);
        } catch (QiniuException e) {
            log.error("删除文件失败:{}",e.getMessage());
        }
        return CmsResult.result();
    }

    @Override
    public CmsResult merge(Chunk chunk) {
        return null;
    }

    @Override
    public void cancel(Chunk chunk) {

    }

    private Map<String, Object> params(Response response,String bucketName,String objectName,String reNameFile,long size){
        Map<String, Object> params = new HashMap<>(16);
        if(response!=null){
            try {
                DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
                params.put("requestId",response.reqId);
                params.put("key",putRet.key);
            } catch (QiniuException e) {
                throw new RuntimeException(e);
            }
        }
        params.put("bucket",bucketName);
        if(Checker.BeNotBlank(objectName)){
            params.put("objectName",objectName);
        }
        if(Checker.BeNotBlank(reNameFile)){
            params.put("reNameFile",reNameFile);
        }
        params.put("size",size);
        params.put("fileUrl",fileUrl(CmsConf.getAsBoolean(SysConst.enableSSL),bucketName,reNameFile));
        return params;
    }

    private String token(String bucketName){
        String accessToken = stringRedisTemplate.opsForValue().get(token+bucketName);
        if(Checker.BeBlank(accessToken)){
            String upToken = auth.uploadToken(bucketName);
            stringRedisTemplate.opsForValue().set(token+bucketName,upToken,7200, TimeUnit.SECONDS);
            accessToken= upToken;
        }
        return accessToken;
    }

    private String fileUrl(boolean ssl,String bucketName,String reNameFile){
        String prefix= "http://";
        if(ssl){
            prefix="https://";
        }
        return prefix+endpoint+"/"+reNameFile;
    }

    private String getFileUrl(Map<String,Object> objectMap){
        if(Checker.BeNotEmpty(objectMap) && objectMap.containsKey("fileUrl")){
            Object fileUel = objectMap.get("fileUrl");
            return Checker.BeNotNull(fileUel)?fileUel.toString():null;
        }
        return null;
    }

    private String getReName(String objectName){
        String suffix = FileUtil.getSuffix(objectName);
        LocalDate now = LocalDate.now();
        String data = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        return data+"/"+ UUID.randomUUID().toString()+"."+suffix;
    }

    public static void main(String[] args) {
        System.out.println(FileUtil.getSuffix("11.png"));
    }
}
