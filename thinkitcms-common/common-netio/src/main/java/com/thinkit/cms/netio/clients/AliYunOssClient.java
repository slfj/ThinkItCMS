package com.thinkit.cms.netio.clients;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.*;
import com.thinkit.cms.annotation.Client;
import com.thinkit.cms.api.attach.AttachService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.attach.AttachDto;
import com.thinkit.cms.dto.netio.Chunk;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.netio.callback.AliYunCallBack;
import com.thinkit.cms.netio.callback.UploadCallBack;
import com.thinkit.cms.netio.request.AliPartETag;
import com.thinkit.cms.netio.request.UploadRequest;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Data
@Client(value = "aliyunOss")
public class AliYunOssClient implements OssClient{

    private OSS ossClient;

    private UploadCallBack uploadCallBack;

    private String endpoint;

    private AttachService attachService = SpringUtil.getBean(AttachService.class);

    private RedisTemplate redisTemplate = SpringUtil.getBean("redisTemplate");

    @Override
    public OssClient build(String endpoint, String accessKeyId, String accessKeySecret) {
        if(Checker.BeNull(this.ossClient)){
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            this.ossClient = ossClient;
            this.endpoint = endpoint;
        }
        if(Checker.BeNull(this.uploadCallBack)){
            this.uploadCallBack = new AliYunCallBack();
        }
        return this;
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes,String siteId) {
        PutObjectResult putObjectResult=null;
        String reNameFile = getReName(objectName);
        try {
              putObjectResult= ossClient.putObject(bucketName, reNameFile, new ByteArrayInputStream(bytes));
              Map<String, Object> objectMap = params(putObjectResult,bucketName,objectName,reNameFile,bytes.length);
              objectMap.put(SysConst.siteId,siteId);
              uploadCallBack.callBack(true,objectMap);
              return CmsResult.result(getFileUrl(objectMap));
        } catch (OSSException | ClientException e) {
              log.error("文件上传异常:"+e.getMessage());
              uploadCallBack.callBack(false,params(null,bucketName,objectName,reNameFile,bytes.length));
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, InputStream inputStream,long size,String siteId) {
        PutObjectResult putObjectResult=null;
        String reNameFile = getReName(objectName);
        try {
            putObjectResult= ossClient.putObject(bucketName, reNameFile, inputStream);
            Map<String, Object> objectMap = params(putObjectResult,bucketName,objectName,reNameFile,size);
            objectMap.put(SysConst.siteId,siteId);
            uploadCallBack.callBack(true,objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (OSSException | ClientException e) {
            log.error("文件上传异常:"+e.getMessage());
            uploadCallBack.callBack(false,params(null,bucketName,objectName,reNameFile,size));
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult uploadFile(Chunk chunk) throws IOException {
        try {
            UploadRequest request = buildRequest(chunk);
            putObject(chunk,request);
        }catch (IOException e){
            log.error("文件上传异常:" + e.getMessage());
            throw e;
        }
        return CmsResult.result();
    }

    private UploadRequest buildRequest(Chunk chunk){
        UploadRequest uploadRequest = new UploadRequest();
        String identifier = chunk.getIdentifier();
        String key = SysConst.fileFlatAli+identifier;
        String objectName = chunk.getFilename();
        String reNameFile = getReName(objectName);
        String bucketName = chunk.getBucketName();
        bucketName = Checker.BeBlank(bucketName)? CmsConf.get(SysConst.bucketName):bucketName;
        if(!redisTemplate.hasKey(key)){
            InitiateMultipartUploadRequest request = new InitiateMultipartUploadRequest(bucketName, reNameFile);
            InitiateMultipartUploadResult upresult = ossClient.initiateMultipartUpload(request);
            String uploadId = upresult.getUploadId();
            uploadRequest.setUploadId(uploadId);
            uploadRequest.setBucketName(bucketName);
            uploadRequest.setObjectName(reNameFile);
            uploadRequest.setPartETags(new ArrayList<>());
        }else {
            uploadRequest = (UploadRequest) redisTemplate.opsForValue().get(key);
        }
        return uploadRequest;
    }

    private void putObject(Chunk chunk,UploadRequest uploadRequest) throws IOException {
        String identifier = chunk.getIdentifier();
        String key = SysConst.fileFlatAli+identifier;
        String uploadId = uploadRequest.getUploadId();
        UploadPartRequest uploadPartRequest = new UploadPartRequest();
        uploadPartRequest.setBucketName(uploadRequest.getBucketName());
        uploadPartRequest.setKey(uploadRequest.getObjectName());
        uploadPartRequest.setUploadId(uploadId);
        // 设置上传的分片流。
        // 以本地文件为例说明如何创建FIleInputstream，并通过InputStream.skip()方法跳过指定数据。
        try {
            InputStream instream = chunk.getFile().getInputStream();
            // instream.skip(startPos);
            uploadPartRequest.setInputStream(instream);
            // 设置分片大小。除了最后一个分片没有大小限制，其他的分片最小为100 KB。
            uploadPartRequest.setPartSize(chunk.getCurrentChunkSize());
            // 设置分片号。每一个上传的分片都有一个分片号，取值范围是1~10000，如果超出此范围，OSS将返回InvalidArgument错误码。
            uploadPartRequest.setPartNumber(chunk.getChunkNumber());
            // 每个分片不需要按顺序上传，甚至可以在不同客户端上传，OSS会按照分片号排序组成完整的文件。
            UploadPartResult uploadPartResult = ossClient.uploadPart(uploadPartRequest);
            // 每次上传分片之后，OSS的返回结果包含PartETag。PartETag将被保存在partETags中。
            AliPartETag aliPartETag = new AliPartETag();
            PartETag partETag = uploadPartResult.getPartETag();
            aliPartETag.setETag(partETag.getETag());
            aliPartETag.setPartCRC(partETag.getPartCRC());
            aliPartETag.setPartNumber(partETag.getPartNumber());
            aliPartETag.setPartSize(partETag.getPartSize());
            uploadRequest.getPartETags().add(aliPartETag);
            redisTemplate.opsForValue().set(key,uploadRequest);
        } catch (OSSException| ClientException|IOException e) {
            log.error("上传失败：{}",e.getMessage());
            throw e;
        }
    }

    @Override
    public CmsResult uploadFile(String bucketName, String objectName, byte[] bytes, UploadCallBack callBack,String siteId) {
        PutObjectResult putObjectResult = null;
        String reNameFile = getReName(objectName);
        try {
            putObjectResult= ossClient.putObject(bucketName, objectName, new ByteArrayInputStream(bytes));
            Map<String, Object> objectMap = params(putObjectResult,bucketName,objectName,reNameFile,bytes.length);
            objectMap.put(SysConst.siteId,siteId);
            callBack.callBack(true,objectMap);
            return CmsResult.result(getFileUrl(objectMap));
        } catch (OSSException | ClientException e) {
            log.error("文件上传异常:{}"+e.getMessage());
            callBack.callBack(false,params(null,bucketName,objectName,reNameFile,bytes.length));
        }
        return null;
    }

    @Override
    public CmsResult deleteFile(String bucketName, String objectName,String siteId) {
        Map<String, Object> objectMap = params(null,bucketName,objectName,objectName,0);
        try {
            AttachDto attachDto = attachService.getByField("full_path",objectName);
            if(Checker.BeNotNull(attachDto)){
                ossClient.deleteObject(bucketName, attachDto.getRelativePath());
                attachService.deleteByPk(attachDto.getId());
                uploadCallBack.callAsyncDelBack(true,objectMap);
            }
        } catch (OSSException e) {
            uploadCallBack.callAsyncDelBack(false,objectMap);
            log.error("删除文件失败:{}",e.getMessage());
        }
        return CmsResult.result();
    }

    @Override
    public CmsResult merge(Chunk chunk) {
        try {
            String identifier = chunk.getIdentifier();
            String key = SysConst.fileFlatAli+identifier;
            if(redisTemplate.hasKey(key)){
                UploadRequest uploadRequest = (UploadRequest) redisTemplate.opsForValue().get(key);
                List<PartETag> partETags = new ArrayList<>();
                AtomicLong size = new AtomicLong(0L);
                uploadRequest.getPartETags().forEach(partETag->{
                    PartETag partTag = new PartETag(partETag.getPartNumber(),partETag.getETag(),
                    partETag.getPartSize(),partETag.getPartCRC());
                    partETags.add(partTag);
                    size.addAndGet(partTag.getPartSize());
                });
                CompleteMultipartUploadRequest completeMultipartUploadRequest =
                new CompleteMultipartUploadRequest(uploadRequest.getBucketName(),
                uploadRequest.getObjectName(), uploadRequest.getUploadId(),partETags);
                CompleteMultipartUploadResult completeMultipartUpload =
                ossClient.completeMultipartUpload(completeMultipartUploadRequest);
                Map map = params(null,uploadRequest.getBucketName(),chunk.getFilename(),
                uploadRequest.getObjectName(),size.get());
                map.put("fileName",chunk.getFilename());
                map.put("fileUrl",completeMultipartUpload.getLocation());
                redisTemplate.delete(key);
                return CmsResult.result(map);
            }
        }catch (Exception e){
            cancel(chunk);
            log.error("文件上传异常:" + e.getMessage());
        }
        return CmsResult.result();
    }

    public void cancel(Chunk chunk){
        String identifier = chunk.getIdentifier();
        String key = SysConst.fileFlatAli+identifier;
        try {
            if(redisTemplate.hasKey(key)){
                UploadRequest uploadRequest = (UploadRequest) redisTemplate.opsForValue().get(key);
                AbortMultipartUploadRequest abortMultipartUploadRequest =
                new AbortMultipartUploadRequest(uploadRequest.getBucketName(), uploadRequest.getObjectName(), uploadRequest.getUploadId());
                ossClient.abortMultipartUpload(abortMultipartUploadRequest);
            }
        }catch (Exception e){
            log.error("取消异常：{}",e.getMessage());
        }finally {
            redisTemplate.delete(key);
        }
    }


    private Map<String, Object> params(PutObjectResult putObjectResult,String bucketName,String objectName,String reNameFile,long size){
        Map<String, Object> params = new HashMap<>(16);
        if(putObjectResult!=null){
            params.put("requestId",putObjectResult.getRequestId());
            params.put("versionId",putObjectResult.getVersionId());
            params.put("serverCRC",putObjectResult.getServerCRC());
        }
        params.put("bucket",bucketName);
        params.put("objectName",objectName);
        params.put("reNameFile",reNameFile);
        params.put("size",size);
        params.put("fileUrl",fileUrl(CmsConf.getAsBoolean(SysConst.enableSSL),bucketName,reNameFile));
        return params;
    }

    private String fileUrl(boolean ssl,String bucketName,String reNameFile){
        String prefix= "http://";
        if(ssl){
            prefix="https://";
        }
        return prefix+bucketName+"."+endpoint+"/"+reNameFile;
    }

    private String getFileUrl(Map<String,Object> objectMap){
        if(Checker.BeNotEmpty(objectMap) && objectMap.containsKey("fileUrl")){
            Object fileUel = objectMap.get("fileUrl");
            return Checker.BeNotNull(fileUel)?fileUel.toString():null;
        }
        return null;
    }

    private String getReName(String objectName){
        String suffix = FileUtil.getSuffix(objectName);
        LocalDate now = LocalDate.now();
        String data = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        return data+"/"+ UUID.randomUUID()+"."+suffix;
    }

    public static void main(String[] args) {
        System.out.println(FileUtil.getSuffix("11.png"));
    }
}
