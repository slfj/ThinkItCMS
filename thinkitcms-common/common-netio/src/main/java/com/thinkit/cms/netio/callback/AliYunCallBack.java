package com.thinkit.cms.netio.callback;
import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.api.attach.AttachService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.attach.AttachDto;

import java.util.Map;

/**
 * @author LONG
 */

public class AliYunCallBack extends UploadCallBack{

    private final AttachService attachService = SpringUtil.getBean(AttachService.class);

    @Override
    public void callBack(boolean isOk, Map<String, Object> params) {
        if(isOk){
            AttachDto attachDto = buildAttach(params);
            attachService.insert(attachDto);
            System.out.println("上传成功");
            System.out.println(JSONUtil.toJsonStr(params));
        }else{
            System.out.println("上传失败！！！！！！！！");
        }

    }

    @Override
    public void callDelBack(boolean isOk, Map<String, Object> params) {
        if(isOk){
            System.out.println("删除成功");
            System.out.println(JSONUtil.toJsonStr(params));
        }else{
            System.out.println("删除失败！！！！！！！！");
        }
    }

    private AttachDto buildAttach(Map<String, Object> params){
        AttachDto attachDto = new AttachDto();
        String bucket = params.get("bucket").toString();
        String relativePath = params.get("reNameFile").toString();
        String objectName = params.get("objectName").toString();
        String fileUrl = params.get("fileUrl").toString();
        Integer size = Integer.valueOf(params.get("size").toString());
        String siteId =  params.get(SysConst.siteId).toString();
        String suffix = FileUtil.getSuffix(objectName);
        attachDto.setBucket(bucket).setSuffix(suffix).setFullPath(fileUrl).setRelativePath(relativePath).
        setObjectName(objectName).setFileSize(size).setSiteId(siteId);
        return attachDto;
    }

    @Override
    public void asyncDelBack(boolean isOk, Map<String, Object> params) {
        attachService.deleteByFiled("","");
        System.out.println("异步执行删除");
    }
}
