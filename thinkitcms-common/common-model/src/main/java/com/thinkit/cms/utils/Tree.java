package com.thinkit.cms.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Tree<T> implements Serializable {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Tree<T>> children = new ArrayList<Tree<T>>();

    private String id;

    private String pid;

    private String name;

    private Object meta;
}