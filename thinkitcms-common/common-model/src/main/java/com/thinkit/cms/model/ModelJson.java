package com.thinkit.cms.model;

import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
@Getter
@Setter
public class ModelJson {

    private Map objectMap;

    private String pk;

    private String prefix;

    private String indexName;

    private String key;

    private List<String> keys;

    public ModelJson(String pk,Map  objectMap,String prefix){
        this.objectMap = objectMap;
        this.pk = pk;
        this.prefix = prefix;
    }

    public ModelJson(){

    }

    public String buildKey(String ... keys){
        String k =  this.prefix;
        if(Checker.BeNotEmpty(keys)){
            for(String key:keys){
                k+=key;
            }
        }
        return k + this.pk;
    }

    public String buildKey(boolean appendLast,String ... keys){
         if(appendLast){
             String k =  this.prefix;
             k+=this.pk;
             if(Checker.BeNotEmpty(keys)){
                 for(String key:keys){
                     k+=key;
                 }
             }
             return k;
         }else{
             return buildKey(keys);
         }
    }
}
