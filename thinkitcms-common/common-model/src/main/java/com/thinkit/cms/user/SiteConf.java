package com.thinkit.cms.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class SiteConf implements Serializable {

    private String siteId;

    private String templateId;

    private String code;

    private String dir;

    private String mobileDir;

    private String domain;

    public SiteConf(){

    }

    public SiteConf(String siteId, String templateId) {
        this.siteId = siteId;
        this.templateId = templateId;
    }
}
