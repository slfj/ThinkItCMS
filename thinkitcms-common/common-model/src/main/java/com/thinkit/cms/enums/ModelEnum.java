package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

@Getter
public enum ModelEnum {

    ARTICLE("1", "文章模型"),
    CATEGORY("2", "栏目模型"),
    FRAGMENT("3", "页面片段");

    /**
     * 名称
     */
    private String name;

    /**
     * 值
     */
    private String val;


    ModelEnum(String val, String name) {
        this.val = val;
        this.name = name;
    }


    public static ModelEnum getEnums(String val) {
        for (ModelEnum modelEnum : ModelEnum.values()) {
            if(modelEnum.getVal().equals(val)){
                return modelEnum;
            }
        }
        return ModelEnum.ARTICLE;
    }
}
