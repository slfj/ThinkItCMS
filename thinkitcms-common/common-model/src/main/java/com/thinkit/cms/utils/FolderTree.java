package com.thinkit.cms.utils;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class FolderTree {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<FolderTree> children = new ArrayList<FolderTree>();

    private String id;


    private String pid;


    private String name;

    /**
     * 相对路径
     */
    private String relativePath;


    /**
     * 模板 相对路径
     */
    private String templateRelativePath;


    /**
     * 绝对路径
     */
    private String absolutePath;

    private Boolean isDirectory;

    private long lastModified;


    private String backgroundColor="#ffffff00";

    private String rename;

    private Boolean readonly = true;

    List<String> ids;

    private Boolean disabled;

    private Boolean root=false;
}
