
package com.thinkit.cms.annotation;

import java.lang.annotation.*;

/**
 * @author LONG
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD})
public @interface License {

     boolean enable() default true;
}