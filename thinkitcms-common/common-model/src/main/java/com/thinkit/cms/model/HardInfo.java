package com.thinkit.cms.model;

import lombok.Data;

@Data
public class HardInfo {

    private String identification;
    private String name;
    private String type;
    private Integer major;
    private long size;

}
