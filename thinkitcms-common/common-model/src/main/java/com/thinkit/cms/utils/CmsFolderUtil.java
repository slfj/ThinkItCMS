package com.thinkit.cms.utils;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.thinkit.cms.constant.SysConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LONG
 */
@Slf4j
public class CmsFolderUtil {


    /**
     * 初始化模板路径
     * @return
     * @throws IOException
     */
    public static File initBaseFolder() throws IOException {
        String templatesFolder = CmsConf.get(SysConst.cmsTemplate);
        templatesFolder= Checker.BeNotBlank(templatesFolder)?templatesFolder:SysConst.templates;
        templatesFolder=prefix(removeLast(templatesFolder,SysConst.prefix));
        String initPath = trimAll(baseFolder()+templatesFolder);
        File file = new File(initPath);
        if(!file.exists()){
            boolean isOk = file.mkdirs();
            if(!isOk){
                log.error("初始化路径:{} 失败!",initPath);
            }
        }
        ClassPathResource classPathResource = new ClassPathResource("templates/index.ts.ftl");
        try {
            InputStream inputStream = classPathResource.getInputStream();
            FileUtil.writeFromStream(inputStream,new File(file.getPath()+"/index.ts.ftl"));
        } catch (IOException e) {
            log.error("读取 directives JSON 配置文件异常!{}",e.getMessage());
            e.printStackTrace();
        }
        return file;
    }

    /**
     * 初始化模板路径
     * @return
     * @throws IOException
     */
    public static File initTemplateFolder(String ...tpmCode) throws IOException {
        String templatesFolder = CmsConf.get(SysConst.cmsTemplate);
        templatesFolder= Checker.BeNotBlank(templatesFolder)?templatesFolder:SysConst.templates;
        templatesFolder=prefix(removeLast(templatesFolder,SysConst.prefix));
        String initPath = trimAll(baseFolder()+templatesFolder);
        for(String tmpf :tpmCode){
            initPath+=prefix(tmpf);
        }
        File file = new File(initPath);
        if(!file.exists()){
            boolean isOk = file.mkdirs();
            if(!isOk){
                log.error("初始化路径:{} 失败!",initPath);
            }
        }
        return file;
    }


    /**
     * 初始化站点文件夹
     * @return
     * @throws IOException
     */
    public static String initSiteFolder(String sitePath) throws IOException {
        String siteFolder = CmsConf.get(SysConst.cmsSites);
        siteFolder= Checker.BeNotBlank(siteFolder)?siteFolder:SysConst.sites;
        siteFolder=prefix(removeLast(siteFolder,SysConst.prefix));
        String initPath = trimAll(baseFolder()+siteFolder);
        if(Checker.BeNotBlank(sitePath)){
            sitePath=prefix(sitePath);
            initPath = initPath+sitePath;
        }
        File file = new File(initPath);
        if(!file.exists()){
            file.mkdirs();
        }
        return initPath;
    }


    private static String trimAll(String folder){
        if(Checker.BeNotBlank(folder)){
            return folder.replaceAll(SysConst.blankReplace, "").replace("\"", "");
        }
        return folder;
    }

    public static String baseFolder() throws IOException {
        String cmsFolder = CmsConf.get(SysConst.cmsFolder);
        if(Checker.BeBlank(cmsFolder)){
            throw new IOException("初始化模板加载路径异常,请检查 config.properties 中是否存在 [cmsFolder] 配置项，并且是否存在该路径!");
        }
        return cmsFolder;
    }

    public static String prefix(String folder){
        folder=folder.startsWith(SysConst.prefix)?folder:SysConst.prefix+folder;
        return folder;
    }

    public static String removePrefix(String folder){
        folder=folder.startsWith(SysConst.prefix)?folder.substring(1):folder;
        return folder;
    }

    public static String removeTemplate(String folder){
        String templatePath = removePrefix(CmsConf.get(SysConst.cmsTemplate));
        folder = removePrefix(folder);
        folder=folder.startsWith(templatePath)?folder.replace(templatePath,""):folder;
        return folder;
    }


    public static String suffix(String folder){
        folder=folder.endsWith(SysConst.prefix)?folder:folder+SysConst.prefix;
        return folder;
    }

    private static String removeLast(String folder,String charType){
       if(Checker.BeNotBlank(folder)&&folder.contains(charType) && folder.length()>1){
           return folder.substring(0,folder.length()-1);
       }
       return folder;
    }

    public static void create(boolean appendFrist,String ...dirs) {
        if(Checker.BeNotEmpty(dirs)){
            String finalPath="";
            int i=0;
            for(String dir:dirs){
                if(appendFrist&&i==0){
                    dir = prefix(dir);
                }else if(i!=0){
                    dir = prefix(dir);
                }
                finalPath+=dir;
                i+=1;
            }
            File file = new File(finalPath);
            if(!file.exists()){
                file.mkdirs();
            }
        }
    }

    public static String appendFolder(boolean appendFrist,String ...dirs) {
        if(Checker.BeNotEmpty(dirs)){
            String finalPath="";
            int i=0;
            for(String dir:dirs){
                if(appendFrist&&i==0){
                    dir = prefix(dir);
                }else if(i!=0){
                    dir = prefix(dir);
                }
                finalPath+=dir;
                i+=1;
            }
            return finalPath;
        }
        return null;
    }

    public static String appendFolder(String ...dirs) {
        if(Checker.BeNotEmpty(dirs)){
            String finalPath="";
            for(String dir:dirs){
                finalPath+=dir;
            }
            return finalPath;
        }
        return null;
    }

    public static String getRelativePath(String path) {
        path = path.replace("\\", "/");
        String root = trimAll(CmsConf.get(SysConst.cmsFolder));
        if(path.contains(root)){
            path =  path.replace(root,SysConst.blank).replace(SysConst.brefix,SysConst.prefix);
        }else{
            root=root.replace(SysConst.pprefix,SysConst.brefix);
            path =  path.replace(root,SysConst.blank).replace(SysConst.brefix,SysConst.prefix);
        }
        return path;
    }

    public static String getAbsolutePath(String path) {
        String root = trimAll(CmsConf.get(SysConst.cmsFolder));
        if(Checker.BeNotBlank(path)){
            return root+prefix(path);
        }
        return root;
    }

    public static String getSitePath(String path,String ... paths) {
        String sitePath = getAbsolutePath(path)+prefix(trimAll(CmsConf.get(SysConst.cmsSites)));
        if(Checker.BeNotEmpty(paths)){
            for(String ph:paths){
                sitePath+=prefix(ph);
            }
        }
        return sitePath;
    }

    public static String getUpload() {
        String baseFolder = getAbsolutePath(null);
        if(Checker.BeNotBlank(baseFolder)){
             return suffix(appendFolder(suffix(baseFolder),CmsConf.get(SysConst.cmsUpload)));
        }
        return "";
    }

    public static String getSitePathHtml(String path,String ... paths) {
        return getSitePath(path,paths)+SysConst.html;
    }

    public static String getSitePathUrl(String path,String ... paths) {
        return getSitePath(path,paths);
    }

    public static String getSiteDomain(String domain) {
        return Boolean.valueOf(CmsConf.get(SysConst.enableSSL)) ?SysConst.https+domain:SysConst.http+domain;
    }

    public static String getName( String path, String name){
        String appendFolder =  appendFolder(false,path,name);
        if(FileUtil.exist(appendFolder)){
            String parent =  appendFolder(false,path);
            File[]  files = new File(parent).listFiles();
            int count=0;
            for(File file:files){
                if(file.isDirectory() && file.getName().startsWith(name)){
                    count+=1;
                }
            }
            String finalName =  name+"("+count+")";
            while (FileUtil.exist(appendFolder(false,path,finalName))){
                count+=1;
                finalName=name+"("+count+")";
            }
            return finalName;
        }
        return name;
    }

    public static String getFileName( String path, String name,String suffix){
        File file = FileUtil.file(path);
        if(FileUtil.exist(file) && !file.isDirectory()){
            String parent =  file.getParent();
            File[]  files = new File(parent).listFiles();
            int count=0;
            for(File fl:files){
                if(FileUtil.isFile(fl) && suffix.equalsIgnoreCase( FileUtil.getSuffix(fl))){
                    boolean isExit = !fl.isDirectory() && fl.getName().startsWith(name);
                    if(isExit){
                        count+=1;
                    }
                }
            }
            String finalName =  name+"("+count+")"+"."+suffix;
            while (FileUtil.exist(file.getParent()+prefix(finalName))){
                count+=1;
                finalName =  name+"("+count+")"+"."+suffix;
            }
            return file.getParent()+prefix(finalName);
        }
        return path;
    }

    public static String clearBlank(String name){
        if(Checker.BeNotBlank(name)){
           return name.replaceAll("\\s*", "");
        }
        return name;
    }

    public static FolderTree loopFile(boolean isAppend, String tmpDir) {
        if(isAppend){
            tmpDir =  getAbsolutePath(tmpDir);
        }
        List<File> fileList = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        File file = new File(tmpDir);
        FolderTree folder = folderTree(file,ids);
        listFile(folder,file,fileList,ids);
        folder.setIds(ids);
        return folder;
    }


    public static List<File> listFile(FolderTree folder,File dir, List<File> fileList,List<String> ids){
        File[] files = dir.listFiles();
        for (File fileItem : files){
            if(!fileItem.getName().equals(SysConst.fragmentName)){
                if (fileItem.isDirectory()){
                    fileList.add(fileItem);
                    FolderTree folderTree=folderTree(fileItem,ids);
                    folder.getChildren().add(folderTree);
                    listFile(folderTree,fileItem,fileList,ids);
                }else {
                    fileList.add(fileItem);
                    folder.getChildren().add(folderTree(fileItem,ids));
                }
            }
        }
        return fileList;
    }

    private static FolderTree folderTree(File fileItem,List<String> ids){
        FolderTree folderTree =new FolderTree();
        folderTree.setName(fileItem.getName());
        folderTree.setRename(fileItem.getName());
        folderTree.setIsDirectory(fileItem.isDirectory());
        folderTree.setAbsolutePath(Base64.encode(fileItem.getAbsolutePath(), StandardCharsets.UTF_8));
        String relativePath = getRelativePath(fileItem.getAbsolutePath());
        folderTree.setRelativePath(relativePath);
        String template = prefix(CmsConf.get(SysConst.cmsTemplate));
        String templateRelativePath = relativePath.startsWith(template)?relativePath.replace(template,""):relativePath;
        folderTree.setTemplateRelativePath(removePrefix(templateRelativePath));
        folderTree.setDisabled(fileItem.isDirectory());
        folderTree.setLastModified(fileItem.lastModified());
        folderTree.setId(IdUtil.getSnowflake(1, 31).nextIdStr());
        folderTree.setRoot(Checker.BeEmpty(ids));
        // folderTree.setId(SecureUtil.md5(fileItem.getAbsolutePath()));
        ids.add(folderTree.getId());
        return folderTree;
    }

    private static List<FolderTree> foldersTree(File[] fileItems,List<String> ids){
       List<FolderTree> folderTrees=new ArrayList<>();
       for(File fileItem:fileItems){
           folderTrees.add(folderTree(fileItem,ids));
       }
       return folderTrees;
    }

    public static boolean saveFileContent(String absolutePath, String content) {
       boolean isExist =  FileUtil.exist(absolutePath);
       if(isExist && !FileUtil.isDirectory(absolutePath)){
           FileUtil.writeString(content,absolutePath,Charset.forName("UTF-8"));
           return true;
       }
       return false;
    }

    public static void main(String[] args) {
        List<String> ids = new ArrayList<>();
        File file = new File("D:\\com\\baomidou");
        FolderTree folder = folderTree(file,ids);
        List<File> fileList = new ArrayList<>();
        listFile(folder,file,fileList,ids);
        System.out.println(fileList);
    }

    public static String fileContent(File file) {
        return FileUtil.readString(file, Charset.forName("UTF-8"));
    }


    public static String addSuffix(String path, String name ,String suffix, boolean appendPoint) {
        if(Checker.BeNotBlank(name)){
            if(appendPoint){
                return path + (name+"." + suffix);
            }else{
                return path + (name+suffix);
            }
        }
        return path;
    }

    public static void createNewFile(String finalFile) throws IOException {
        File file = new File(finalFile);
        if(!file.exists()){
            boolean isMkdir = !FileUtil.exist(file.getParent());
            if(isMkdir){
                FileUtil.mkdir(file.getParent());
            }
            file.createNewFile();
        }
    }

    public static String formatUrl(String url, int index) {
        if(url.contains(SysConst.html) && index>1){
            url=url.substring(0,url.lastIndexOf(SysConst.html))+"_"+index+SysConst.html;
        }
        return url;
    }
}

