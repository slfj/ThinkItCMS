package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum TaskStatusEnum {

    RUNING("RUNING", "运行中"),
    FINSHED("FINSHED", "运行完成"),
    BREAK("BREAK", "任务中断");

    @Getter
    private String name;

    @Getter
    private String val;


    TaskStatusEnum(String val, String name) {
        this.val = val;
        this.name = name;
    }
}
