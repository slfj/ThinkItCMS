
package com.thinkit.cms.annotation;

import java.lang.annotation.*;

/**
 * @author LONG
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ValidGroup3 {

}