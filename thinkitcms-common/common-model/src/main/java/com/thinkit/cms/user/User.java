package com.thinkit.cms.user;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;

/**
 * @author LONG
 */
@Data
public class User implements UserDetail, Serializable {


    private static final String detail = "com.thinkit.cms.api.site.SiteDetail";

    protected String id;
    /**
     * 账户
     */
    protected String account;

    protected SiteConf siteConf;


    /**
     * 昵称
     */
    private String nickName;

    /**
     * 密码
     */
    protected String password;
    /**
     * 状态
     */
    protected String status;

    /**
     * 客户端ID
     */
    protected String clientId;

    /**
     * 用户上下文
     */
    protected UserCtx userCtx;


    /**
     * 任务Id
     */
    protected String taskId;


    public User(){

    }

    public User(String account,String password){
        this.account = account;
        this.password = password;
    }

    public User(String account,String password,String status){
        this.account = account;
        this.password = password;
        this.status = status;
    }

    @Override
    public boolean lock(String status) {
        if(StringUtils.isNotBlank(this.status)){
            return status.equals(this.status);
        }
        return false;
    }

    @Override
    public boolean passCk(String password) {
        return true;
    }

    @Override
    public String getId() {
        return this.id;
    }

    public SiteConf getSite(boolean throwExp) {
        try {
            SysUserDetail sysUserDetail = (SysUserDetail) Class.forName(detail).getConstructor().newInstance();
            return sysUserDetail.getSite(throwExp);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        return null;
    }


    public SiteConf getSite(String siteId) {
        try {
            SysUserDetail sysUserDetail = (SysUserDetail) Class.forName(detail).getConstructor().newInstance();
            return sysUserDetail.getSite(siteId);
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
        }
        return null;
    }

    public SiteConf getSite(boolean throwExp,String userId) {
        try {
            SysUserDetail sysUserDetail = (SysUserDetail) Class.forName(detail).getConstructor().newInstance();
            return sysUserDetail.getSite(throwExp,userId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String getClientId() {
        return this.clientId;
    }

    @Override
    public void setUserCtx(UserCtx userCtx) {
      this.userCtx = userCtx;
    }

    @Override
    public boolean isSuperAdmin() {
        return userCtx.getIsSuperAdmin();
    }

    @Override
    public boolean isSuperRole() {
        return userCtx.getIsSuperRole();
    }

    public void setSiteConf(SiteConf siteConf) {
        this.siteConf = siteConf;
    }

    @Override
    public Integer roles() {
        return 0;
    }
}
