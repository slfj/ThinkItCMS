package com.thinkit.cms.annotation;
import com.thinkit.cms.enums.IdType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface IndexId {

    /**
     * 主键ID
     *
     * @return 默认为未设置
     */
    IdType type() default IdType.NONE;

    String value() default "id";

    String prefix() default "";

    double weight() default 1.0;
}
