package com.thinkit.cms.enums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum LicenseEnum {

    ORG("1", "事业单位商用永久授权"),
    COMPANY("2", "企业商用永久授权"),
    PERSON("3", "个人商用永久授权"),
    FREE("4", "个人非商用永久授权");

    @Getter
    private String name;

    @Getter
    private String val;


    LicenseEnum(String val, String name) {
        this.val = val;
        this.name = name;
    }

    public static LicenseEnum getEnums(String val) {
        for (LicenseEnum licenseEnum : LicenseEnum.values()) {
            if(licenseEnum.getVal().equals(val)){
                return licenseEnum;
            }
        }
        return LicenseEnum.ORG;
    }
}
