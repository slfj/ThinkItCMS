package com.thinkit.cms.annotation;
import com.thinkit.cms.enums.FieldType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字段注解
 * <p>
 * Copyright © 2021 xpc1024 All Rights Reserved
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD, ElementType.ANNOTATION_TYPE})
public @interface IndexField {
    /**
     * 自定义字段在es中的名称
     *
     * @return 字段在 redis 中的名称
     */
    String value() default "";

    /**
     * 是否为数据库表字段 默认 true 存在，false 不存在
     *
     * @return 存在
     */
    boolean exist() default true;

    /**
     * 字段在es索引中的类型,建议根据业务场景指定,若不指定则由本框架自动推断
     *
     * @return 类型
     */
    FieldType fieldType() default FieldType.TEXT;

    /**
     * 设置text、keyword_text 可以进行聚合操作
     *
     * @return 是否设置可聚合
     */
    boolean fieldData() default false;


    /**
     * 是否转换成 map 转换成 map 后 key 将全局显示 不包裹
     * @return
     */
    boolean convertMap() default false;


    /**
     * es索引中的日期格式
     *
     * @return 日期格式 例如yyyy-MM-dd HH:mm:ss
     */
    String dateFormat() default "";

    /**
     * 是否忽略大小写 默认 false 不忽略，为true时则大小写不敏感，都可查
     *
     * @return 是否忽略大小写
     */
    boolean ignoreCase() default false;

    double weight() default 1.0;


    /**
     * 是排序
     * @return
     */
    boolean sortField() default false;


}