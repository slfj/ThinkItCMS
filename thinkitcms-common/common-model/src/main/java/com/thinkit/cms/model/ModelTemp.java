package com.thinkit.cms.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class ModelTemp implements Serializable {

    public ModelTemp(){

    }

    /**
     * 【0】：模板路径 1：目标地址
     */
    private String[] pc = new String[2];

    private String[] mobile = new String[2];

    public void setPcTarget(String target){
        this.getPc()[1] = target;
    }

    private String pcTemp;

    private String mobileTemp;

    /**
     * pc 相对目标地址路径
     */
    private String relativePath;

    /**
     * mobile 相对目标地址路径
     */
    private String relativeMPath;
}
