package com.thinkit.cms.utils;


import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LONG
 */
public class BuildTree {

	public final static String rootNode = "-1";

	public final static String parentNode = "0";

	public static <T> Tree<T> build(List<Tree<T>> nodes) {
		if (nodes == null) {
			return null;
		}
		List<Tree<T>> topNodes = new ArrayList<Tree<T>>();
		for (Tree<T> children : nodes) {
			String pid = children.getPid();
			if (pid == null || parentNode.equals(pid)) {
				topNodes.add(children);
				continue;
			}
			for (Tree<T> parent : nodes) {
				String id = parent.getId();
				if (id != null && id.equals(pid)) {
					parent.getChildren().add(children);
					continue;
				}
			}

		}
		Tree<T> root = new Tree<T>();
		if (topNodes.size() == 1) {
			root = topNodes.get(0);
		} else {
			root.setId(rootNode);
			root.setPid(parentNode);
			root.setChildren(topNodes);
			root.setName("全 部");
		}
		return root;
	}

	public static <T>List<Tree<T>> treeList(Tree tree) {
		if(Checker.BeNotNull(tree)){
			if(rootNode.equals(tree.getId())){
				return tree.getChildren();
			}else{
				List<Tree<T>> trees = new ArrayList<>();
				trees.add(tree);
				return trees;
			}
		}
		return Lists.newArrayList();
	}
}