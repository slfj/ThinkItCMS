package com.thinkit.cms.model;

import cn.hutool.json.JSONUtil;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * @author LONG
 */
@Getter
@Setter
public class ModelData extends BaseDto{

    @IndexField(exist = false)
    private Map<String,Object> extra;


    @IndexField(fieldType = FieldType.TEXT,exist = false)
    private String extraStr;


    @IndexField(fieldType = FieldType.TEXT,exist = false)
    private String table;

    @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
    protected String pk;

    public void extraJson(){
        if(Checker.BeNotEmpty(extra) && Checker.BeBlank(extraStr)){
             this.extraStr = JSONUtil.toJsonStr(extra);
        }
    }
}
