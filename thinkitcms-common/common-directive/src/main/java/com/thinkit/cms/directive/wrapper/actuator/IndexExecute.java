package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.annotation.Executer;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @author LONG
 */
@Slf4j
@Executer(ActuatorEnum.INDEX)
public class IndexExecute extends AbstractActuator{

    @Override
    public void execute(ModelAndView modelAndView) {
         super.execute(modelAndView);
    }

    @Override
    public String rulePath(ModelAndView modelAndView) {
        SiteConf siteConf = getSiteConf(modelAndView);
        String dir = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getDir(), SysConst.index);
        String mobileDir = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getMobileDir(), SysConst.index);
        super.setPcTarget(dir).setMobileTarget(mobileDir);
        return null;
    }

    @Override
    protected void success(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {
        log.error("首页容生成成功:{}",targetPath);
        if(pc){
            String taskId = modelAndView.getTaskId();
            taskService.upProgress(taskId,SysConst.okTimes,1);
        }
    }

    @Override
    protected void fail(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {
        log.error("首页容生成失败:{}",targetPath);
        if(pc){
            String taskId = modelAndView.getTaskId();
            taskService.upProgress(taskId,SysConst.failTimes,1);
        }
    }
}
