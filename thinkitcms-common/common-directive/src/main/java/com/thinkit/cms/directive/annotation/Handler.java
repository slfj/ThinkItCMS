
package com.thinkit.cms.directive.annotation;

import java.lang.annotation.*;

/**
 * @author LONG
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Handler {

    String value() default "";
}