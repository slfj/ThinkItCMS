package com.thinkit.cms.directive.wrapper;

import java.util.HashMap;
import java.util.Map;
/**
 * @author LONG
 */

public class ContextKit {

    public static ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>(16);
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<>(16);
            threadLocal.set(map);
        }
        return map.get(key) == null ? null : map.get(key);
    }


    public static void remove() {
        threadLocal.remove();
    }
}
