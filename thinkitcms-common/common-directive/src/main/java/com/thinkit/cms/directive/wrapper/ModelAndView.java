package com.thinkit.cms.directive.wrapper;

import cn.hutool.extra.spring.SpringUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.wrapper.actuator.Actuator;
import com.thinkit.cms.directive.wrapper.actuator.ActuatorLoader;
import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.model.ModelTempParma;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @author LONG
 */
@Setter
@Getter
public class ModelAndView {

    private Environment environment= SpringUtil.getBean(Environment.class);

    private final static String key="params";

    private  Kv kv;

    private User user;

    private String templateId;

    private String siteId;

    private String rowId;

    private Boolean rewrite  = true;

    private String taskId;

    private List<Actuator> actuators = new ArrayList<>();

    private CategoryMData categoryMData;

    private SiteConf siteConf;


    public ModelAndView(){

    }

    public ModelAndView(Kv kv){
        this.kv = kv;
    }

    public ModelAndView addParam(String key,Object val){
        if(Checker.BeNull(kv)){
            this.kv = Kv.create();
        }
        this.kv.setIfNotNull(key,val);
        return this;
    }

    public ModelAndView addParam(Kv kv){
        if(Checker.BeNull(kv)){
            this.kv = Kv.create();
        }
        this.kv.set(kv);
        return this;
    }

    public ModelAndView category(CategoryMData categoryMData){
        this.categoryMData = categoryMData;
        return this;
    }

    public ModelAndView taskId(String taskId){
        this.taskId = taskId;
        return this;
    }

    public ModelAndView rewrite(Boolean rewrite){
        this.rewrite = rewrite;
        return this;
    }

    public ModelAndView addActuator(ActuatorEnum actuatorEnum,String formModelId,Function<ModelTempParma, ModelTemp> function,Map<String,Object> initParams){
        Actuator actuator =  ActuatorLoader.getActuator(actuatorEnum);
        if(Checker.BeNotNull(actuator)){
            if(Checker.BeNotEmpty(initParams)){
                actuator.initParams(initParams);
            }
            ModelTempParma modelTempParma = new ModelTempParma(this.getSiteId(),this.getTemplateId(),formModelId);
            modelTempParma.setIsHomePage(actuatorEnum.equals(ActuatorEnum.INDEX));
            if(Checker.BeNotNull(function)){
                ModelTemp tmpPaths = function.apply(modelTempParma);
                actuator.sourceTarget(tmpPaths);
            }
            Function<ModelAndView, String> ruleFunc= x ->actuator.rulePath(this);
            ruleFunc.apply(this);
            actuators.add(actuator);
        }
        return this;
    }

    public ModelAndView addActuator(ActuatorEnum actuatorEnum,String formModelId,ModelTemp tmpPaths,Map<String,Object> initParams){
        Actuator actuator =  ActuatorLoader.getActuator(actuatorEnum);
        if(Checker.BeNotNull(actuator)){
            if(Checker.BeNotEmpty(initParams)){
                actuator.initParams(initParams);
            }
            ModelTempParma modelTempParma = new ModelTempParma(this.getSiteId(),this.getTemplateId(),formModelId);
            modelTempParma.setIsHomePage(actuatorEnum.equals(ActuatorEnum.INDEX));
            if(Checker.BeNotNull(tmpPaths)){
                actuator.sourceTarget(tmpPaths);
            }
            Function<ModelAndView, String> ruleFunc= x ->actuator.rulePath(this);
            ruleFunc.apply(this);
            actuators.add(actuator);
        }
        return this;
    }

    public Actuator actuator(int index){
         if(Checker.BeNotEmpty(actuators)){
             return actuators.get(index);
         }
         return null;
    }


    public static ModelAndView create(User user){
        ModelAndView modelAndView = new ModelAndView(Kv.create());
        modelAndView.setUser(user);
        modelAndView.setTemplateId(user.getSite(true).getTemplateId());
        modelAndView.setSiteId(user.getSite(true).getSiteId());
        modelAndView.setSiteConf(user.getSite(true,user.getId()));
        return modelAndView;
    }

    public static ModelAndView create(User user,String siteId,String templateId){
        ModelAndView modelAndView = new ModelAndView(Kv.create());
        modelAndView.setUser(user);
        modelAndView.setTemplateId(templateId);
        modelAndView.setSiteId(siteId);
        modelAndView.setSiteConf(user.getSite(true,user.getId()));
        return modelAndView;
    }

    public static ModelAndView create(User user,SiteConf siteConf,String siteId,String templateId){
        ModelAndView modelAndView = new ModelAndView(Kv.create());
        modelAndView.setUser(user);
        modelAndView.setTemplateId(templateId);
        modelAndView.setSiteId(siteId);
        modelAndView.setSiteConf(siteConf);
        return modelAndView;
    }

    public ModelAndView finished() {
        ContextKit.set(key,this.kv);
        return this;
    }

    public  ModelAndView rowId(String rowId){
        this.setRowId(rowId);
        return this;
    }

    public static void close() {
        ContextKit.remove();
    }


    public static Map<String,Object> getParams(){
        return getParams(key);
    }

    public static void setParams(Map<String,Object> params){
        if(Checker.BeNotEmpty(params)){
            ContextKit.set(key,params);
        }
    }

    public static <T>T getParams(String key){
        Object object =  ContextKit.get(key);
        if(Checker.BeNotNull(object)){
            return (T) object;
        }
        return null;
    }

    public void execute(Boolean async) {
        if(async){
            Map<String,Object> params = getParams();
            ThreadFactory.run(new Executer(actuators,this,initVar(params)));
        }else{
            doIt();
        }
    }

    private void doIt(){
        Executer executer = new Executer(actuators,this, initVar(this.kv));
        executer.run();
    }

    private Map<String,Object> initVar(Map<String,Object> params){
        if(!params.containsKey(SysConst.siteId)){
            params.put(SysConst.siteId,siteId);
        }
        String server = CmsConf.get(SysConst.server);
        String http = CmsConf.getAsBoolean(SysConst.enableSSL)?SysConst.https:SysConst.http;
        String domainRoot = CmsFolderUtil.appendFolder(http,siteConf.getDomain(),SysConst.prefix,siteConf.getDir());
        String domain = siteConf.getDomain();
        if(SysConst.self.equals(server)){
            domain = domain.replace(SysConst.http,"");
            domain = http+SysConst.localhost+":"+environment.getProperty("local.server.port")+SysConst.prefix+domain;
        }
        params.put(SysConst.domain,domain );
        if(SysConst.self.equals(server)){
            domainRoot = domainRoot.replace(SysConst.http,"");
            domainRoot = http+SysConst.localhost+":"+environment.getProperty("local.server.port")+SysConst.prefix+domainRoot;
        }
        params.put(SysConst.domainRoot, domainRoot);
        if(Checker.BeNotBlank(siteConf.getMobileDir())){
            String domainMRoot = CmsFolderUtil.appendFolder(http,siteConf.getDomain(),SysConst.prefix,siteConf.getMobileDir());
            if(SysConst.self.equals(server)){
                domainMRoot = domainMRoot.replace(SysConst.http,"");
                domainMRoot =http+SysConst.localhost+":"+environment.getProperty("local.server.port")+SysConst.prefix+domainMRoot;
            }
            params.put(SysConst.domainMRoot, domainMRoot);
        }
        return params;
    }
}
