package com.thinkit.cms.directive.source;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.exception.TemplateException;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.directive.unit.ParamConfUnit;
import com.thinkit.cms.directive.unit.ParamDefUnit;
import com.thinkit.cms.directive.unit.ParamUnit;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;
import java.util.*;

/**
 * @author LONG
 */
@Slf4j
public class DataResolve extends  Resolve {


    /**
     * ${xx} 获取modelView 的变量  #{xx} 获取指令参数的变量
     * @param render
     * @return
     */
    public static String resolver(BaserRender render) {
        DirectiveUnit unit = render.getUnit();
        ParamConfUnit confUnit = unit.getConfig();
        String sqlTemplate = unit.getSqlTemplate();
        sqlTemplate= sqlFilterTemplate(sqlTemplate);
        DirectiveData directiveData = new DirectiveData(render,sqlTemplate);
        if (Checker.BeNotNull(confUnit)) {
            ParamDefUnit defUnit = confUnit.getParams();
            if (Checker.BeNotNull(defUnit)) {
                List<ParamUnit> inParams = defUnit.getIn();
                if (Checker.BeNotEmpty(inParams)) {
                    for (ParamUnit in : inParams) {
                        resolveParam(in,directiveData);
                    }
                    List<SqlParam> sqlParams = formatSqlTmp(directiveData);
                    return getSql(sqlParams,sqlTemplate);
                }
            }
        }
        return sqlTemplate;
    }

    private String sqlCheck(String sql){
        return sql;
    }

    private static String getSql(List<SqlParam> sqlParams,String sqlTemplate){
        if(Checker.BeNotEmpty(sqlParams)){
            for(SqlParam sqlParam:sqlParams){
                if(sqlTemplate.contains(sqlParam.getExpression())){
                    Object value = sqlParam.getValue();
                    if(Checker.BeNotNull(value)){
                        sqlTemplate = sqlTemplate.replace(sqlParam.getExpression(),sqlParam.getValue().toString());
                    }
                }
            }
        }
        return sqlTemplate;
    }


    private static void resolveParam(ParamUnit unit,DirectiveData directiveData){
        Boolean required= unit.getRequired();
        String directive  = directiveData.getRender().getUnit().getDirective();
        String name = unit.getName();
        String type = unit.getType();
        LongEnum longEnum = LongEnum.getEnum(type);
        Object val =  directiveData.getValue(name,longEnum);
        if(required && Checker.BeNull(val)){
            log.error("表达式:{}的参数:{} 无效",directive,name);
            throw new TemplateException("指令参数无效!");
        }
        directiveData.getData().put(name,val);
    }


    private static List<SqlParam> formatSqlTmp(DirectiveData directiveData){
        String sqlTemplate = directiveData.getSqlTemplate();
        List<SqlParam> sqlParams = sqlParams(sqlTemplate);
        for (SqlParam sqlParam:sqlParams){
            List<String> properties = formatProperty(sqlParam);
            if(Checker.BeNotEmpty(properties)){
                // 循环 属性的 值 contentId, user.xxx.mm 或者 user[0].xx 或者 user[0].hxx[0]
                Object object = null;
                Integer loop =0;
                for(String property:properties){
                    object = getInvokeValue(object,property,directiveData.getData(),loop);
                    loop+=1;
                }
                sqlParam.setValue(object);
            }
        }
        return sqlParams;
    }



    private static List formatProperty(SqlParam sqlParam){
        List<String> propertys=new ArrayList<>();
        String key = getKey(sqlParam);
        if(key.contains(".")){
            String[] property = key.split("\\.");
            sqlParam.setProperty(property);
        }else{
            sqlParam.setProperty(new String[]{key});
        }
        for (String property:sqlParam.getProperty()) {//java string数组 添加到list
            propertys.add(property);
        }
        return propertys;
    }





    private static String getKey(SqlParam sqlParam){
        String key = sqlParam.getExpression().replace("#{","").replace("}","");
        return key;
    }


    private static List<SqlParam> sqlParams(String sqlTemplate){
        List<SqlParam> sqlParams = new ArrayList<>();
        if(Checker.BeNotBlank(sqlTemplate)){
            addExpression(sqlTemplate,sqlParams);
        }
        return  sqlParams;
    }

    private static void addExpression(String sqlTemplate,List<SqlParam> sqlParams){
        int index=0;
        boolean find= true;
        while (find){
            int s = sqlTemplate.indexOf("#{",index);
            if(s!=-1){
                int e = sqlTemplate.indexOf("}",s);
                SqlParam sqlParam = new SqlParam();
                String exps = sqlTemplate.substring(s,e+1).replace(" ","");
                sqlParam.setExpression(exps);
                sqlParam.setFormatExpress(formatParam(exps));
                if(exps.contains(".")){
                    sqlParam.setType(LongEnum.OBJECT);
                }else if(exps.contains("[") && exps.contains("]")){
                    sqlParam.setType(LongEnum.LIST);
                }
                sqlParams.add(sqlParam);
                index = e;
            }else{
                find = false;
            }
        }
    }

    private static String formatParam(String exps){
        if(Checker.BeNotBlank(exps)){
            exps = exps.replace("#{","").replace("}","");
        }
        return exps;
    }

    public static void main(String[] args) {
        String input ="[2]";
        int start = input.indexOf("[");
        int end = input.indexOf("]",start);
        System.out.println(input.substring(start+1,end));
        System.out.println();
       // System.out.println( input.substring(0, 1).toUpperCase() + input.substring(1));
//        String a = "SELECT * FROM tk_content WHERE ID =#{contentId} AND id =#{user.email} or us=1";
//        int index=0;
//        boolean find= true;
//        while (find){
//            int s = a.indexOf("#{",index);
//            if(s!=-1){
//                int e = a.indexOf("}",s);
//                index = e;
//                System.out.println(a.substring(s,e+1));
//            }else{
//                find = false;
//            }
//        }
    }
}