package com.thinkit.cms.directive.unit;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LONG
 */
@Data
public class DirectiveUnit implements Serializable {
    private String  name;
    private String  directive;
    private Boolean  enable ;
    private String sqlTemplate;
    private String sqlType="mysql";
    private ParamConfUnit config;
    private Integer directType;
}
