package com.thinkit.cms.directive.wrapper;

import lombok.Getter;
import lombok.Setter;

/**
 * @author LONG
 */
@Setter
@Getter
public class CategoryMData {

    private String categoryId;

    private String categoryCode;

    private String ruleRule;

    private String contentId;


    /**
     * 如果该属性为 true 时 则强制更新栏目地址
     */
    private Boolean forceGen = false;

    public CategoryMData(String categoryId, String categoryCode, String ruleRule) {
        this.categoryId = categoryId;
        this.categoryCode = categoryCode;
        this.ruleRule = ruleRule;
    }

    public CategoryMData(String categoryId, String categoryCode, String ruleRule,Boolean forceGen) {
        this.categoryId = categoryId;
        this.categoryCode = categoryCode;
        this.ruleRule = ruleRule;
        this.forceGen = forceGen;
    }


    public CategoryMData(String categoryId, String categoryCode, String ruleRule, String contentId) {
        this.categoryId = categoryId;
        this.categoryCode = categoryCode;
        this.ruleRule = ruleRule;
        this.contentId = contentId;
    }

    public CategoryMData(String categoryId, String categoryCode) {
        this.categoryId = categoryId;
        this.categoryCode = categoryCode;
    }

    public CategoryMData(String ruleRule) {
        this.ruleRule = ruleRule;
    }

    public CategoryMData() {

    }
}
