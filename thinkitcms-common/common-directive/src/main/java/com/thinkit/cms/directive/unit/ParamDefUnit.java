package com.thinkit.cms.directive.unit;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LONG
 */
@Data
public class ParamDefUnit {

    private List<ParamUnit> in=new ArrayList<>();

    private List<ParamUnit> out=new ArrayList<>();


}
