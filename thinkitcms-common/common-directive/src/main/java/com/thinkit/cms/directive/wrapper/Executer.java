package com.thinkit.cms.directive.wrapper;

import com.thinkit.cms.directive.wrapper.actuator.Actuator;
import com.thinkit.cms.utils.Checker;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
@Data
public class Executer implements Runnable{

    public Executer(){

    }

    private Map<String,Object> params;

    public Executer(List<Actuator> actuators) {
        this.actuators = actuators;
    }

    public Executer(List<Actuator> actuators, ModelAndView modelAndView,Map<String,Object> params) {
        this.actuators = actuators;
        this.modelAndView = modelAndView;
        this.params = params;
    }

    private List<Actuator> actuators = new ArrayList<>();

    private ModelAndView modelAndView;

    @Override
    public void run() {
        if(Checker.BeNotEmpty(actuators)){
           try {
               ModelAndView.setParams(params);
               for(Actuator actuator:actuators){
                   actuator.execute(modelAndView);
               }
           }catch (Exception e){
               log.error("执行器执行异常:{}",e.getMessage());
           }finally {
               ModelAndView.close();
               this.modelAndView = null;
           }
        }
    }
}
