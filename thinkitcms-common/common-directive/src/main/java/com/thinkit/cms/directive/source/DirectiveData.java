package com.thinkit.cms.directive.source;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.render.BaserRender;
import freemarker.template.SimpleHash;
import freemarker.template.TemplateModelException;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author LONG
 */
@Data
public class DirectiveData {

    private Map<String,Object> data = new HashMap<>();

    public DirectiveData() {
    }
    private BaserRender render;

    private String sqlTemplate;

    public DirectiveData(BaserRender render) {
        this.render = render;
    }

    public DirectiveData(BaserRender render, String sqlTemplate) {
        this.render = render;
        this.sqlTemplate = sqlTemplate;
    }

    public Object getValue(String name,LongEnum longEnum){
        Object value =  render.getData(name,null,longEnum);
        try {
            if(value instanceof SimpleHash){
                return ((SimpleHash) value).toMap();
            }
        }catch (TemplateModelException e) {
            throw new RuntimeException(e);
        }
        return value;
    }
}
