package com.thinkit.cms.directive.emums;

import lombok.Data;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */

public enum LongEnum {

	NUMBER("Number","Number类型",Number.class),

	INTEGER("Integer","Integer类型",Integer.class),

	SHORT("Short","Short类型",Short.class),

	LONG("Long","Long类型",Long.class),

	FLOAT("Float","Float类型",Float.class),

	DOUBLE("Double","Double类型",Double.class),

	BOOLEAN("Boolean","Boolean类型",Boolean.class),

	STRING("String","String类型",String.class),

	DATE("Date","Date类型", Data.class),

	MAP("Map","Map类型", Map.class),

	LONG_ARRAY("LongArray","LongArray类型"),

	STRING_ARRAY("StringArray","LongArray类型"),

	INTEGER_ARRAY("IntegerArray","LongArray类型"),


	OBJECT_ARRAY("ObjectArray","ObjectArray类型"),

	BEAN("Bean","对象类型类型"),

	LIST("List","List类型类型", List.class),

	OBJECT("Object","Object 类型");


	public static LongEnum getEnum(String value) {
		for (LongEnum each : LongEnum.class.getEnumConstants()) {
			if (value.equals(each.value)) {
				return each;
			}
		}
		return null;
	}

	@Getter
	private String value;

	@Getter
	private String name;

	@Getter
	private Class clz;



	LongEnum(String value, String name) {
		this.name = name;
		this.value = value;
	}

	LongEnum(String value, String name,Class clz) {
		this.name = name;
		this.value = value;
		this.clz = clz;
	}

	public static boolean isNumber(LongEnum langEnum){
		return langEnum.equals(INTEGER) || langEnum.equals(SHORT)||langEnum.equals(LONG)||
		langEnum.equals(FLOAT)||langEnum.equals(DOUBLE);
	}

	public static boolean isArray(LongEnum langEnum){
		return langEnum.equals(LONG_ARRAY) || langEnum.equals(STRING_ARRAY)||
		langEnum.equals(INTEGER_ARRAY)||langEnum.equals(OBJECT_ARRAY)||langEnum.equals(LIST);
	}
}
