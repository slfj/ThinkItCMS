package com.thinkit.cms.directive.unit;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ParamConfUnit {

    private ParamDefUnit params=new ParamDefUnit();

    private ReturnUnit result = new ReturnUnit();

    private CacheUnit cacheUnit = new CacheUnit();

}
