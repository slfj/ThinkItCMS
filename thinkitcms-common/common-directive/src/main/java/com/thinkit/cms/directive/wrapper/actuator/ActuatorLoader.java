package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.directive.annotation.Executer;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author LONG
 */
@Slf4j
public class ActuatorLoader {

    private static final String packageLocal="com.thinkit.cms.directive.wrapper.actuator";

    private static final Map<String,Class> stringClassMap = new HashMap<>();

    public static Actuator getActuator(ActuatorEnum actuator)  {
        Map<String,Class> classes = getClasses();
        Class clz = classes.get(actuator.getCode());
        if(Checker.BeNotNull(clz)){
            try {
                Actuator actuatorInstance = (Actuator) clz.getConstructor().newInstance();
                return actuatorInstance;
            }catch (Exception e){
                e.printStackTrace();
                log.error("处理器加载失败!{}",e.getMessage());
                return null;
            }
        }
        return null;
    }

    private static Map<String,Class>  getClasses(){
        if(Checker.BeNotEmpty(stringClassMap)){
            return stringClassMap;
        }
        Reflections reflections = new Reflections(packageLocal);
        Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Executer.class);
        if(Checker.BeNotEmpty(classes)){
            for(Class clas:classes){
                Executer executer = (Executer) clas.getAnnotation(Executer.class);
                stringClassMap.put(executer.value().getCode(),clas);
            }
        }
        return stringClassMap;
    }

}
