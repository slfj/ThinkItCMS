package com.thinkit.cms.directive.wrapper;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author LONG
 */
public class ThreadFactory {

    private static  ExecutorService executorService = new ThreadPoolExecutor(2, 16, 100, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(10000), new ThreadPoolExecutor.CallerRunsPolicy());

    public static void  run(Executer executer){
        executorService.submit(executer) ;
    }
}
