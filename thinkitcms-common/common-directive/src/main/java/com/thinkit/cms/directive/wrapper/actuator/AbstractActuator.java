package com.thinkit.cms.directive.wrapper.actuator;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.constant.AnsiColor;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.mapper.SqlMapper;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;
import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * @author LONG
 */
@Slf4j
@Setter
@Getter
public abstract class AbstractActuator implements Actuator {

    protected SqlMapper sqlMapper= SpringUtil.getBean(SqlMapper.class);
    /**
     * 模板地址
     */
    protected ModelTemp modelTemp;

    private String rulePath;

    protected RedisTemplate redisTemplate  = SpringUtil.getBean("redisTemplate");

    protected Configuration configuration = SpringUtil.getBean(Configuration.class);

    protected TaskService taskService = SpringUtil.getBean(TaskService.class);

    /**
     * 初始化执行器是的参数
     */
    private Map<String,Object> initParams;

    /**
     * pc 相对目标地址路径
     */
    protected String relativePath;

    /**
     * mobile 相对目标地址路径
     */
    protected String relativeMPath;



    protected boolean enable(){
        return CmsConf.getAsBoolean(SysConst.enableES);
    }

    protected boolean hasRelativePath(boolean isPc){
        String url = isPc? SysConst.url:SysConst.murl;
        return Checker.BeNotEmpty(initParams) && initParams.containsKey(url);
    }

    protected String getRelativePath(boolean isPc){
        String url = isPc? SysConst.url:SysConst.murl;
        return initParams.get(url).toString();
    }

    @Override
    public Actuator relativePath(String relativePath) {
       this.relativePath = relativePath;
       return this;
    }

    @Override
    public Actuator relativeMPath(String relativeMPath) {
        this.relativeMPath = relativeMPath;
        return this;
    }

    @Override
    public Actuator sourceTarget(ModelTemp modelTemp){
        this.modelTemp = modelTemp;
        return this;
    }

    @Override
    public ModelTemp getTemplate(){
        return this.modelTemp;
    }


    public AbstractActuator setPcTarget(String target){
        log.info("设置PC 目标路径：{}",target);
        if(Checker.BeNotNull(modelTemp)){
            modelTemp.getPc()[1] = target;
        }else{
            log.info("设置PC 目标路径失败：{}","modelTemp 模板配置不存在");
        }
        return this;
    }

    public AbstractActuator setMobileTarget(String target){
        log.info("设置 Mobile 目标路径：{}",target);
        if(Checker.BeNotNull(modelTemp)){
            modelTemp.getMobile()[1] = target;
        }else{
            log.info("设置 Mobile 目标路径失败：{}","modelTemp 模板配置不存在");
        }
        return this;
    }

//    protected SiteConf getSiteConf(ModelAndView modelAndView){
//        SiteConf siteConf;
//        if(Checker.BeNotNull(modelAndView.getUser())){
//             siteConf = modelAndView.getUser().getSite(true,modelAndView.getUser().getId());
//        }else{
//             siteConf = modelAndView.getUser().getSite(true);
//        }
//        return siteConf;
//    }


    protected SiteConf getSiteConf(ModelAndView modelAndView){
        SiteConf siteConf = null;
        if(Checker.BeNotNull(modelAndView.getUser())){
            siteConf =  modelAndView.getUser().getSiteConf();
            if(Checker.BeNull(siteConf)){
                siteConf = modelAndView.getUser().getSite(true,modelAndView.getUser().getId());
            }
            return siteConf;
        }
        return siteConf;
    }
    
    

    @Override
    public Actuator initParams(Map<String, Object> stringObjectMap) {
        this.initParams = stringObjectMap;
        return this;
    }

    @Override
    public void execute(ModelAndView modelAndView) {
        if(Checker.BeNotNull(modelTemp)){
            String taskId = modelAndView.getUser().getTaskId();
            if(Checker.BeNotBlank(taskId)){
                Boolean hashKey = redisTemplate.hasKey(SysConst.taskStop+taskId);
                if(hashKey){
                    log.info("暂停的任务：{}",taskId);
                    return ;
                }
            }
            toDoIt(modelTemp,true,modelAndView);
            toDoIt(modelTemp,false,modelAndView);
        }
    }

    private void toDoIt(ModelTemp modelTemp,Boolean pc,ModelAndView modelAndView){
        if(pc){
            String pcTemp = modelTemp.getPc()[0];
            String pcTarget = modelTemp.getPc()[1];
            doExecute(pcTemp,pcTarget,modelAndView,true);
        }else{
            String mobileTemp = modelTemp.getMobile()[0];
            String mobileTarget = modelTemp.getMobile()[1];
            doExecute(mobileTemp,mobileTarget,modelAndView,false);
        }
    }

    private Actuator doExecute(String tempPath,String targetPath,ModelAndView modelAndView,Boolean pc){
        if(Checker.BeNotBlank(tempPath) && Checker.BeNotBlank(targetPath)){
            Map<String, Object> params = modelAndView.getKv();
            try {
                if(Checker.BeNotEmpty(params)){
                    log.info(AnsiColor.RED+"注意：{} 模板 最终的渲染模板的变量为：{}",tempPath,JSONUtil.toJsonStr(params));
                }
                boolean rewrite = modelAndView.getRewrite();
                FileOutputStream outputStream = new FileOutputStream(initDir(targetPath,rewrite));
                Template t = configuration.getTemplate(CmsFolderUtil.removeTemplate(tempPath));
                Writer out = new OutputStreamWriter(outputStream, Charset.forName("UTF-8"));
                t.process(params, out);
                out.flush();
                out.close();
                this.success(pc,tempPath,targetPath,modelAndView);
            }catch (Exception e){
                this.fail(pc,tempPath,targetPath,modelAndView);
                log.error("模板生成异常：{}",e.getMessage());
                e.printStackTrace();
            }
        }
        return this;
    }


    private File initDir(String destPath,boolean rewrite) throws IOException {
        File file =  new File(destPath);
        if(!FileUtil.exist(file.getParent())){
            FileUtil.mkdir(file.getParent());
        }
        boolean isOk =file.createNewFile();
        if(!isOk && rewrite){
            file.delete();
            isOk =file.createNewFile();
        }
        log.info("创建文件结果：{}",isOk);
        return file;
    }

    private String taskKey(String categoryId,String siteId){
        StringBuffer stringBuffer = new StringBuffer(SysConst.task);
        stringBuffer.append(siteId);
        stringBuffer.append(SysConst.colon);
        stringBuffer.append(TaskEnum.CATEGORY_TASK.getVal());
        stringBuffer.append(SysConst.colon);
        stringBuffer.append(categoryId);
        String key = stringBuffer.toString();
        return key;
    }

    protected void upStatus(String categoryId, String siteId, TaskStatusEnum taskStatusEnum){
        String key = taskKey(categoryId,siteId);
        taskService.upStatus(key, taskStatusEnum);
    }

    @Override
    public Actuator setRulePath(String rulePath){
       this.rulePath = rulePath ;
       return this;
    }

    @Override
    public abstract String rulePath(ModelAndView modelAndView);

    /**
     * 成功回调
     * @param tempPath 模板地址
     * @param targetPath 目标地址
     * @param pc true:手机 false:mobile
     * @param  modelAndView
     */
    protected abstract void success(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView);

    /**
     * 失败回调
     *  @param tempPath 模板地址
     *      * @param targetPath 目标地址
     * @param pc true:手机 false:mobile
     */
    protected abstract void fail(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView);
}
