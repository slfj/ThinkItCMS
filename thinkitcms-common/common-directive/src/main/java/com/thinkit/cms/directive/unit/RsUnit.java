package com.thinkit.cms.directive.unit;

import lombok.Data;

@Data
public class RsUnit {

    private String table;

    private String sql;

    private Integer[] limit;

    private String[] fields;

    private String[] sort;

}
