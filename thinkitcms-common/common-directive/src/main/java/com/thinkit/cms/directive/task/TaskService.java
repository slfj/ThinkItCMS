package com.thinkit.cms.directive.task;

import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;

import java.math.BigDecimal;
import java.util.Map;

public interface TaskService {


    /**
     * 生成任务ID
     * @param action
     * @param siteId
     * @param rowStr
     * @return
     */
    String genTaskKey(TaskEnum action, String siteId,String rowStr);

    /**
     * 查询任务进度
     * @param taskDto
     * @return
     */
    Map<String, Object> query(TaskDto taskDto);


    /**
     * 设置任务状态
     * @param taskDto
     * @param key
     * @return
     */
    String startTask(String key,TaskDto taskDto);


    /**
     * 获取任务状态
     * @param key
     * @return
     */
    TaskDto getTask(String key);


    /**
     * 判断任务是在运行中
     * @param key
     * @return
     */
    Boolean isRuning(String key);


    /**
     * 判断任务是在运行中
     * @param key
     * @return
     */
    void upStatus(String key, TaskStatusEnum taskStatusEnum);


    void upProgress(String taskId, String mapKey ,BigDecimal progress);

    void upProgress(String taskId, String mapKey ,Integer delta);

    Double taskProgress(String taskId,String mapKey,TaskEnum taskEnum);

    void clear(String key);

    boolean isStop(String takeId);

    boolean isUpdated(String key);

    void setKey(String key, String val , long l);


    String getVerson();
}
