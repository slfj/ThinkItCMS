package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.model.ModelTemp;

import java.util.Map;

/**
 * @author LONG
 */
public interface Actuator {

    /**
     * 执行器
     * @param modelAndView
     */
    void execute(ModelAndView modelAndView);


    /**
     * 设置模板
     * @param modelTemp
     * @return
     */
    Actuator sourceTarget(ModelTemp modelTemp);


    /**
     * 获取模板
     * @return
     */
    ModelTemp getTemplate();


    /**
     * 设置规则路径
     * @param rulePath
     *  @return
     */
    Actuator setRulePath(String rulePath);


    /**
     * 规则路径
     * @param modelAndView
     * @return
     */
    String rulePath(ModelAndView modelAndView);


    /**
     * 设置相对路径
     * @param relativePath
     * @return Actuator
     */
    Actuator  relativePath(String relativePath);


    /**
     * 设置mobile相对路径
     * @param relativeMPath
     *  @return Actuator
     */
    Actuator  relativeMPath(String relativeMPath);


    /**
     * 初始化参数
     * @param stringObjectMap
     *  @return Actuator
     */
    Actuator  initParams(Map<String,Object> stringObjectMap);
}
