package com.thinkit.cms.directive.executor;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.constant.AnsiColor;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.funs.BaseFunInterface;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.render.DirectiveExecutor;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
/**
 * @author LONG
 */
@Slf4j
public class Executor extends DirectiveExecutor {

    @Override
    public void execute(BaserRender render) throws Exception {
        StopWatch stopWatch = StopWatch.createStarted();
        String sql = getSql(render);
        String directiveName = this.getDirectiveMeta().getDirective();
        Integer directType = this.getDirectiveMeta().getDirectType();
        log.info("指令{}参数解析耗时：{}",directiveName,stopWatch.getTime());
        log.info("指令[{}]:获取SQL:{}",directiveName,sql);
        if(directType.equals(2)){
            BaseFunInterface funInterface =  getBaseFunInterface(directiveName);
            if(Checker.BeNotNull(funInterface)){
                funInterface.execute(render);
            }else{
                log.info(AnsiColor.BLUE+"指令[{}]:没有初始化调用失败", directiveName);
            }
        }else{
            Object object = executeSql(sql,render);
            if(Checker.BeNotNull(object)){
                log.info(AnsiColor.BLUE+"指令[{}]:获取结果:{}", directiveName, JSONUtil.toJsonStr(object));
            }
            render.put(CmsConf.get(SysConst.defaultTmpGlobalKey),object).render();
            stopWatch.stop();
            log.info("指令[{}]:生成耗时:{} 毫秒",this.getDirectiveMeta().getDirective(),stopWatch.getTime());
        }
    }
}
