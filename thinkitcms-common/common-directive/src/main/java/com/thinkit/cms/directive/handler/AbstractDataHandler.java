package com.thinkit.cms.directive.handler;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.unit.ParamUnit;
import com.thinkit.cms.utils.Checker;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
public abstract class AbstractDataHandler implements DataHandler {

    protected <T>T getBean(Class clz){
        return (T) SpringUtil.getBean(clz);
    }

    @Override
    public Object handData(Map value, BaserRender render) {
        return getData(value,render);
    }

    @Override
    public Object handData(Long value, BaserRender render){
        return getData(value,render);
    }

    @Override
    public Object handData(Integer value, BaserRender render){
        return getData(value,render);
    }

    @Override
    public Object handData(Short value, BaserRender render){
        return getData(value,render);
    }

    @Override
    public Object handData(Float value, BaserRender render){
        return getData(value,render);
    }

    @Override
    public Object handData(Double value, BaserRender render){
        return getData(value,render);
    }


    @Override
    public Object handData(Boolean value, BaserRender render){
        return getData(value,render);
    }


    @Override
    public Object handData(String value,List<ParamUnit> outs, BaserRender render){
        return getData(value,render);
    }


    @Override
    public Object handData(Date value, BaserRender render){
        return getData(value,render);
    }


    @Override
    public Object handData(List  value, BaserRender render){
        return getData(value,render);
    }

    @Override
    public Object handData(List<Map>  values, List<ParamUnit> outs, BaserRender render){
        return getData(values,render);
    }

    @Override
    public Object handData(Map value, List<ParamUnit> outs, BaserRender render) {
        return getData(value,render);
    }

    /**
     * 获取格式化数据
     * @param value
     *  @param render
     * @return
     */
    @Override
    public abstract Object getData(Object value,BaserRender render);


    protected <T>T toObject(Object obj){
        if(Checker.BeNotNull(obj)){
            return (T) obj;
        }
        return null;
    }

    protected Map toMap(Object Object){
        return toObject(Object);
    }

    protected List toList(Object Object){
        return toObject(Object);
    }

    protected <T> List<T> toList(Object Object,Class clz){
        if(Checker.BeNotNull(Object)){
            return  JSONUtil.toList(JSONUtil.toJsonStr(Object),clz);
        }
        return Lists.newArrayList();
    }

    protected String toString(Object Object){
        return toObject(Object);
    }

    protected Integer toInteger(Object Object){
        return toObject(Object);
    }

    protected Long toLong(Object Object){
        return toObject(Object);
    }

    protected Long[] toLongArray(Object Object){
        return toObject(Object);
    }

    protected String[] toStringArray(Object Object){
        return toObject(Object);
    }

    protected Integer[] toIntegerArray(Object Object){
        return toObject(Object);
    }

    protected <T>T toObjectArray(Object Object){
        return toObject(Object);
    }

    protected <T>T mapToBean(Object Object,Class<T> t){
        if(Checker.BeNotNull(Object)){
            return (T)JSONUtil.toBean(JSONUtil.toJsonStr(Object),t.getClass());
        }
        return null;
    }

    protected <T>T jsonStrToBean(String Object,Class<T> t){
        if(Checker.BeNotNull(Object)){
            return (T)JSONUtil.toBean(Object,t.getClass());
        }
        return null;
    }

    protected boolean isMap(Object value){
        return Checker.BeNotNull(value) && value instanceof Map;
    }

    protected boolean isList(Object value){
        return Checker.BeNotNull(value) && value instanceof List;
    }

    protected boolean isInteger(Object value){
        return Checker.BeNotNull(value) && value instanceof Integer;
    }
    protected boolean isLong(Object value){
        return Checker.BeNotNull(value) && value instanceof Long;
    }

    protected boolean isString(Object value){
        return Checker.BeNotNull(value) && value instanceof String;
    }

    protected boolean isFloat(Object value){
        return Checker.BeNotNull(value) && value instanceof Float;
    }

    protected boolean isDouble(Object value){
        return Checker.BeNotNull(value) && value instanceof Double;
    }


}
