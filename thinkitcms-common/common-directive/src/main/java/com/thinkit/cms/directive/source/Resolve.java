package com.thinkit.cms.directive.source;
import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.SysExpEnum;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.utils.Checker;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author LONG
 */
public class Resolve {

    private static final List<String> sysExp = new ArrayList<>();

    static {
        sysExp.add("toString()");
        sysExp.add("join()");
    }

    protected static String sqlFilterTemplate(String sqlTemplate){
        int index=0;
        boolean find= true;
        Map<String,Object> objectMap = new HashMap<>(16);
        while (find){
            int s = sqlTemplate.indexOf("${",index);
            if(s!=-1){
                int e = sqlTemplate.indexOf("}",s);
                String key = sqlTemplate.substring(s,e+1);
                objectMap.put(key,getValue(key));
                index = e;
            }else{
                find = false;
            }
        }
        if(Checker.BeNotEmpty(objectMap)){
            for(String key:objectMap.keySet()){
                if(sqlTemplate.contains(key)){
                     Object obj = objectMap.get(key);
                     sqlTemplate = sqlTemplate.replace(key,obj==null?"null":obj.toString());
                }
            }
        }
        return sqlTemplate;
    }

    private static Object getValue(String key){
        Map<String,Object> result =  ModelAndView.getParams();
        if(Checker.BeNotEmpty(result)){
            key = key.replace("${","").replace("}","");
            String[] properties = key.split("\\.");
            if(properties.length==1){
                if(result.containsKey(properties[0])){
                    Object object = result.get(properties[0]);
                    return object;
                }
            }else{
                Object object = null;
                Integer loop =0;
                for(String property:properties){
                    object = getInvokeValue(object,property,result,loop);
                    loop+=1;
                }
                return object;
            }
        }
        return null;
    }



    protected static Object getInvokeValue(Object value, String property, Map<String,Object> params,Integer loop){
        // 第一个对象
        if(loop.equals(0)){
            // 变量为ARRAY
            int arrayIndex = isArray(property);
            if(arrayIndex>-1){
                String key = getFormatKey(property);
                return getArrayVal(arrayIndex,key,params);
            }else{
                return params.get(property);
            }
        }else{
            if(Checker.BeNotNull(value)){
                int arrayIndex = isArray(property);
                if(arrayIndex>-1){
                    property = getFormatKey(property);
                    value = invoke(value,property);
                    return getListObj(value,arrayIndex);
                }else {
                    return invoke(value,property);
                }
            }
            return null;
        }
    }
    protected static Object getListObj(Object value,int arrayIndex){
        if(Checker.BeNotNull(value)){
            List list = new ArrayList();
            if(value.getClass().isArray()){
                list = Arrays.asList(value);
            }else if(value instanceof List){
                list = (List) value;
            }else if(value instanceof Collection ){
                list = ((Collection) value).stream().toList();
            }
            return list.get(arrayIndex);
        }
        return null;
    }

    protected static Object invoke(Object value,String property){
        try {
            if(!isSysExp(property)){
                if(value instanceof Map){
                    return ((Map) value).get(property);
                }
                String getMethod = "get"+property.substring(0, 1).toUpperCase() + property.substring(1);
                Method method = value.getClass().getMethod(getMethod);
                Object obj = method.invoke(value);
                return obj;
            }else{
               return execSysExp(value,property);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * 是否是系统表达
     * @param property
     * @return
     */
    private static Object execSysExp(Object value,String property){
        SysExpEnum sysExpEnum =  sysExpEnum(property);
        if(Checker.BeNull(sysExpEnum)){
            return value;
        }
        switch (sysExpEnum){
            case TO_STRING:
                value = Checker.BeNotNull(value)?value.toString(): null;
                return value;
            case JOIN:
                value = Checker.BeNotNull(value)?value.toString():value;
                return value;
            case SPLIT:
                value = Checker.BeNotNull(value)?value.toString():value;
                return value;
            default:
                return value;
        }
    }

    private static SysExpEnum sysExpEnum(String property){
        if(property.equals("toString()")){
            return SysExpEnum.TO_STRING;
        }else if(property.startsWith("join(") && property.endsWith(")")){
            return SysExpEnum.JOIN;
        }
        return null;
    }




    /**
     * 是否是系统表达
     * @param property
     * @return
     */
    private static boolean isSysExp(String property){
        return sysExp.contains(property);
    }

    protected static int isArray(String key){
        int start = key.indexOf("[");
        int end = key.indexOf("]",start);
        Integer index = -1;
        if(start>=0 && end>0){
            index = Integer.valueOf(key.substring(start+1,end));
        }
        return index;
    }

    protected static Object getArrayVal(int arrayIndex,String key ,Map<String,Object> params){
        boolean hasKey = params.containsKey(key);
        if(hasKey){
            Object value =params.get(key);
            if(Checker.BeNotNull(value)){
                return getListObj(value,arrayIndex);
            }
        }
        return null;
    }



    protected static String getFormatKey(String key){
        int start = key.indexOf("[");
        int end = key.indexOf("]",start);
        if(start>=0 && end>0){
            Integer index = Integer.valueOf(key.substring(start+1,end));
            if(index>-1){
                key = key.substring(0,start);
            }
        }
        return key;
    }
}
