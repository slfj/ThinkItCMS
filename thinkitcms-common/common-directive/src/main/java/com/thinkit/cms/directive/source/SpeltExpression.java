package com.thinkit.cms.directive.source;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.exception.TemplateException;
import com.thinkit.cms.directive.render.BaserRender;
import com.thinkit.cms.directive.unit.DirectiveUnit;
import com.thinkit.cms.directive.unit.ParamConfUnit;
import com.thinkit.cms.directive.unit.ParamDefUnit;
import com.thinkit.cms.directive.unit.ParamUnit;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.expression.EnvironmentAccessor;
import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 表达式解析器
 * @author LONG
 *  ${xx} 获取modelView 的变量  #{xx} 获取指令参数的变量
 */
@Slf4j
public class SpeltExpression {

    private static final ConcurrentHashMap<String, Expression> c = new ConcurrentHashMap<>();

    private static final String sc1="#";

    private static final String sc2="$";
    /**
     * 表达式解析
     * @param render
     * @return
     */
    public static String resolver(BaserRender render) {
        DirectiveUnit unit = render.getUnit();
        // 变量获取
        String sqlTemplate = unit.getSqlTemplate();
        List<SqlParam> sc2Params = sqlParams(sqlTemplate,false);
        // ModelAndView var
        scValSet(sc2Params, ModelAndView.getParams());
        // 指令参数处理
        DirectiveData directiveData = new DirectiveData(render,sqlTemplate);
        ParamConfUnit confUnit = unit.getConfig();
        if (Checker.BeNotNull(confUnit)) {
            ParamDefUnit defUnit = confUnit.getParams();
            if (Checker.BeNotNull(defUnit)) {
                List<ParamUnit> inParams = defUnit.getIn();
                if (Checker.BeNotEmpty(inParams)) {
                    for (ParamUnit in : inParams) {
                        resolveParam(in,directiveData);
                    }
                    List<SqlParam> sc1Params = sqlParams(sqlTemplate,true);
                    scValSet(sc1Params,directiveData.getData());
                    sc2Params.addAll(sc1Params);
                }
            }
            return getSql(sc2Params,sqlTemplate);
        }
        return sqlTemplate;
    }

    private static void scValSet(List<SqlParam> sqlParams,Map<String,Object> result){
         for(SqlParam sqlParam:sqlParams){
            Object res = exec(sqlParam.getFormatExpress(),result);
            if(Checker.BeNotNull(res)){
                sqlParam.setValue(res);
            }
         }
    }

    private static void resolveParam(ParamUnit unit, DirectiveData directiveData){
        Boolean required= unit.getRequired();
        String directive  = directiveData.getRender().getUnit().getDirective();
        String name = unit.getName();
        String type = unit.getType();
        LongEnum longEnum = LongEnum.getEnum(type);
        Object val =  directiveData.getValue(name,longEnum);
        if(required && Checker.BeNull(val)){
            log.error("表达式:{}的参数:{} 无效",directive,name);
            throw new TemplateException("指令参数无效!");
        }
        directiveData.getData().put(name,val);
    }

    private static String getSql(List<SqlParam> sqlParams,String sqlTemplate){
        if(Checker.BeNotEmpty(sqlParams)){
            for(SqlParam sqlParam:sqlParams){
                if(sqlTemplate.contains(sqlParam.getExpression())){
                    Object value = sqlParam.getValue();
                    if(Checker.BeNotNull(value)){
                        sqlTemplate = sqlTemplate.replace(sqlParam.getExpression(),sqlParam.getValue().toString());
                    }
                }
            }
        }
        return sqlTemplate;
    }

    public static void main(String[] args) {
        String sql ="SELECT id,title,keywords FROM tk_content WHERE category_id=#{categoryId.id} AND status='1' limit  (${page.current}- 1)* ${page.pageSize},${page.pageSize}";
        List<SqlParam> aa = sqlParams(sql,true);
        List<SqlParam> aa1 = sqlParams(sql,false);
        System.out.println(aa);

    }

    private static List<SqlParam> sqlParams(String sqlTemplate,boolean isConst){
        List<SqlParam> sqlParams = new ArrayList<>();
        if(Checker.BeNotBlank(sqlTemplate)){
            addExpression(sqlTemplate,sqlParams,isConst);
        }
        return  sqlParams;
    }

    private static String formatParam(String exps,boolean isConst){
        if(Checker.BeNotBlank(exps)){
            String sc = isConst?sc1:sc2;
            exps = exps.replace(sc+"{","").replace("}","");
        }
        return exps;
    }

    private static void addExpression(String sqlTemplate,List<SqlParam> sqlParams,boolean isConst){
        int index=0;
        boolean find= true;
        String sc = isConst?sc1:sc2;
        while (find){
            int s = sqlTemplate.indexOf(sc+"{",index);
            if(s!=-1){
                int e = sqlTemplate.indexOf("}",s);
                SqlParam sqlParam = new SqlParam();
                String exps = sqlTemplate.substring(s,e+1).replace(" ","");
                sqlParam.setExpression(exps);
                sqlParam.setFormatExpress(formatParam(exps,isConst));
                if(exps.contains(".")){
                    sqlParam.setType(LongEnum.OBJECT);
                }else if(exps.contains("[") && exps.contains("]")){
                    sqlParam.setType(LongEnum.LIST);
                }
                sqlParams.add(sqlParam);
                index = e;
            }else{
                find = false;
            }
        }
    }

    /**
     * 表达式执行器构建
     * @param exp
     * @param object
     * @param <T>
     * @return
     */
    public static <T> T exec(final String exp, final Object object) {
        final Expression expression = c.computeIfAbsent(exp, k -> new SpelExpressionParser().parseExpression(exp));
        final StandardEvaluationContext context = new StandardEvaluationContext(object);
        context.addPropertyAccessor(new EnvironmentAccessor());
        if (object instanceof Map) {
            context.addPropertyAccessor(new MapAccessor());
        }
        return (T) expression.getValue(context);
    }
}
