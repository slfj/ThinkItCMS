package com.thinkit.cms.directive.emums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum ActuatorEnum {
    INDEX("0", "首页执行器"),
    ARTICLE("1", "内容执行器"),
    CATEGORY("2", "分类执行器"),
    FRAGMENT("4", "页面片段执行器"),
    ALL("3", "全站执行器");

    @Getter
    private String name;

    @Getter
    private String code;


    ActuatorEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static ActuatorEnum getActuatorEnum(String code) {
        for (ActuatorEnum each : ActuatorEnum.class.getEnumConstants()) {
            if (code.equals(each.code)) {
                return each;
            }
        }
        return null;
    }
}
