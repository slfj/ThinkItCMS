package com.thinkit.cms.directive.render;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.utils.Checker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author LONG
 */
public class ConvertExecutor {


    public static void convertList(Object val, String name, Map map){
       if(Checker.BeNotNull(val)){
           if(!(val instanceof List)){
               if(isString(val)){
                   String valStr = val.toString();
                   if(isArrayStr(valStr) && JSONUtil.isJson(valStr)){
                        List<Map> mapList = JSONUtil.toList(valStr,Map.class);
                        map.put(name,mapList);
                   }else if(contains(valStr,",")){
                        String[] valArr = valStr.split(",");
                        map.put(name, Arrays.asList(valArr));
                   }
               }else if(isArray(val)){
                   map.put(name, Arrays.asList(val));
               }else{
                   List list = new ArrayList();
                   list.add(val);
                   map.put(name, list);
               }
           }
       }
    }

    public static void convertLong(Object val, String name, Map map){
        if(Checker.BeNotNull(val)){
            if(!(val instanceof Long)){
                if(isString(val)){
                    String valStr = val.toString();
                    map.put(name,Long.valueOf(valStr));
                }
            }
        }
    }

    public static void convertInteger(Object val, String name, Map map){
        if(Checker.BeNotNull(val)){
            if(!(val instanceof Integer)){
                if(isString(val)){
                    String valStr = val.toString();
                    map.put(name,Integer.valueOf(valStr));
                }
            }
        }
    }

    public static void convertMap(Object val, String name, Map map){
        if(Checker.BeNotNull(val)){
            if(!(val instanceof Map)){
                if(isString(val)){
                    String valStr = val.toString();
                    if(JSONUtil.isJson(valStr)){
                        map.put(name,JSONUtil.toBean(valStr,Map.class));
                    }
                }
            }
        }
    }


    public static void convertStringArray(Object val, String name, Map map){
        if(Checker.BeNotNull(val)){
            if(!(val instanceof List) && !val.getClass().isArray()){
                if(isString(val)){
                    String valStr = val.toString();
                    map.put(name,Long.valueOf(valStr));
                }
            }
        }
    }

    private static boolean isString(Object val){
        return val instanceof String;
    }

    private static boolean isArray(Object val){
        return  Checker.BeNotNull(val) && val.getClass().isArray();
    }

    private static boolean contains(String val,String character ){
        return  Checker.BeNotBlank(val) && val.contains(character);
    }


    private static boolean isArrayStr(String valStr){
        int start = valStr.indexOf("[");
        int end = valStr.indexOf("]",start);
        return start>=0 && end>0;
    }
}
