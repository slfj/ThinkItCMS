package com.thinkit.cms.directive.emums;

import lombok.Getter;

/**
 * @author LONG
 */

public enum SysExpEnum {

	TO_STRING("转字符串","toString()"),

	JOIN("加入字符串","join()"),

	SPLIT("字符串截取","split()"),

	IMPORT("页面片段导入","import"),

	GLOBAL("全局变量","global"),
   ;


	@Getter
	private String name;
	@Getter
	private String value;

	SysExpEnum(String name, String value) {
		this.name = name;
		this.value = value;
	}
}
