package com.thinkit.cms.directive.event;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
public class ContentUpEsEvent extends ApplicationEvent {

    @Setter
    public EventEnum eventEnum;

    public ContentUpEsEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
