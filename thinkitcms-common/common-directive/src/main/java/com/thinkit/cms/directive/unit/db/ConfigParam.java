package com.thinkit.cms.directive.unit.db;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ConfigParam {
    private Long cacheTime = 10L;
    private String timeUnit ="s";
    private String cacheName= "";
    private String location ="";
    private Boolean enable = true;
}
