package com.thinkit.cms.directive.wrapper.actuator;

import com.thinkit.cms.directive.annotation.Executer;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.wrapper.ModelAndView;

/**
 * @author LONG
 */
@Executer(ActuatorEnum.ALL)
public class AllExecute extends AbstractActuator{

    @Override
    public void execute(ModelAndView modelAndView) {

    }

    @Override
    public String rulePath(ModelAndView modelAndView) {
        return null;
    }

    @Override
    protected void success(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {

    }

    @Override
    protected void fail(Boolean pc,String tempPath,String  targetPath,ModelAndView modelAndView) {

    }
}
