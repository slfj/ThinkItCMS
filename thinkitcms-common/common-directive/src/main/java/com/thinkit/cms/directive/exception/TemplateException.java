package com.thinkit.cms.directive.exception;

public class TemplateException extends RuntimeException {

    public TemplateException(String msg) {
        super(msg);
    }
}
