package com.thinkit.cms.api.formmodel;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.model.KeyValueModel;
import com.thinkit.cms.model.PageModel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/


public interface FormModelService extends BaseService<FormModelDto> {


    /**
     * 保存模型
     * @param formModelDto
     */
    void save(FormModelDto formModelDto);


    /**
     * 查看详情
     * @param id
     * @return
     */
    FormModelDto detail(String id);


    /**
     * 更新
     * @param formModelDto
     */
    void update(FormModelDto formModelDto);

    /**
     * 删除模型
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 分页查询
     * @param pageDto
     * @return
     */
    PageModel<FormModelDto> page(PageModel<FormModelDto> pageDto);


    /**
     * 查询模型列表
     * @param formModelDto
     * @return
     */
    List<FormModelDto> listModel(FormModelDto formModelDto);


    /**
     * 查询部件编码
     * @param pageDto
     * @return
     */
    PageModel<FormModelDto> pageFragment(PageModel<FormModelDto> pageDto);


    /**
     * 保持模型部件
     * @param formModelDto
     */
    void saveFragment(FormModelDto formModelDto);


    /**
     * 更新
     * @param formModelDto
     */
    void updateFragment(FormModelDto formModelDto);

    /**
     * 查询部件模板内容
     * @param formCode
     * @return
     */
    String fragmentContent(String formCode);

    /**
     * 保存模板内容
     * @param formCode
     * @param content
     * @return
     */
    String saveFragmentContent(String formCode, String content);

    /**
     * 删除页面片段
     * @param id
     * @param formCode
     */
    void deleteFragment(String id,String formCode);



    /**
     * 生成部件模板
     * @param formModelId
     * @return
     */
    void genFragment(String formModelId);

    /**
     * 查询片段路径地址
     * @param code
     * @return
     */
    String findPath(String code);

    /**
     * 查看是否存在首页模型
     * @param siteId
     * @param formType
     * @return
     */
    String hasIndexModel(String siteId, String formType);

    /**
     * 加载模型部件
     * @param siteId
     * @return
     */
    List<KeyValueModel> loadParmModel(String siteId);

    String queryCode(@Param("formModelId") String formModelId);
}