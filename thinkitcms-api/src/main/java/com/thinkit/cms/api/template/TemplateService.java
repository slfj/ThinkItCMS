package com.thinkit.cms.api.template;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.template.*;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.FolderTree;

import java.io.InputStream;

/**
* @author LG
*/


public interface TemplateService extends BaseService<TemplateDto> {


    /**
     * 模板保存
     * @param templateDto
     */
    void save(TemplateDto templateDto);


    /**
     * 模板更新
     * @param templateDto
     */
    void update(TemplateDto templateDto);


    /**
     * 查询模板文件夹
     * @param tmpDir
     * @return
     */
    CmsResult loopFolder(String tmpDir);

    /**
     * 创建文件夹者文件夹
     * @param addTemplate
     * @return
     */
    CmsResult mkdirFile(AddTemplate addTemplate);

    /**
     * 文件删除
     * @param delTemplateFile
     * @return
     */
    CmsResult delFolder(DelTemplateFile delTemplateFile);


    /**
     * 重命名
     * @param renameFile
     * @return
     */
    CmsResult renameFile(RenameFile renameFile);

    /**
     * 获取文件内容
     * @param getTemplate
     * @return
     */
    CmsResult fileContent(GetTemplate getTemplate);

    /**
     * 存储内容
     * @param content
     * @return
     */
    CmsResult saveContent(SaveContent content);

    /**
     * 加载模板
     * @param siteId
     * @return
     */
    CmsResult loadTemp(String siteId);


    /**
      根据模板id 查找模板文件
      @param templateId
      @return
     */
    FolderTree loadTempFolder(String templateId);


    /**
     * 加载路径
     * @param templateId
     * @return
     */
    String  loadTempPath(String templateId);


    /**
     * 加载路径
     * @param siteId
     * @return
     */
    String  loadTempPathBySiteId(String siteId);


    /**
     * 打开文件目录
     * @param absolutePath
     * @return
     */
    void openDir(String absolutePath);

    /**
     * 模板删除
     * @param id
     * @return
     */
    boolean delete(String id);

    /**
     * 上传模板文件到云端
     * @param uploadTemplate
     * @return
     */
    Boolean uploadCloud(UploadTemplate uploadTemplate);

    /**
     * 拉取云端资源
     * @param pullTemplate
     * @return
     */
    Boolean pullCloud(PullTemplate pullTemplate);

    /**
     * 导入文件
     * @param tmpDir
     * @return
     */
    CmsResult importFolder(String tmpDir, String fileName, InputStream inputStream);

}