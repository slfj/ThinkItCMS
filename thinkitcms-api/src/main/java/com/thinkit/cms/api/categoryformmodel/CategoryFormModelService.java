package com.thinkit.cms.api.categoryformmodel;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.categoryformmodel.CategoryFormModelDto;

import java.util.List;

/**
* @author LG
*/


public interface CategoryFormModelService extends BaseService<CategoryFormModelDto> {


    /**
     * 栏目内容模型
     * @param modelIds
     * @param categoryId
     * @param siteId
     */
    void categoryFormModel(List<String> modelIds, String categoryId,String siteId);


    /**
     * 查询栏目的 可发布内容模型列表
     * @param id
     * @param siteId
     * @return
     */
    List<String>  queryFormModel(String id, String siteId);


    /**
     * 查找栏目模型
     * @param categoryId
     * @return
     */
    List<CategoryFormModelDto> listModel(String categoryId);
}