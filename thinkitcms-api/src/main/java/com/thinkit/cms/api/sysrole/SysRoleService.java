package com.thinkit.cms.api.sysrole;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysrole.SysRoleDto;
import com.thinkit.cms.model.CmsResult;

import java.util.List;

/**
* @author LG
*/


public interface SysRoleService extends BaseService<SysRoleDto> {


    /**
     * 角色保存
     * @param sysRoleDto
     * @return
     */

    CmsResult save(SysRoleDto sysRoleDto);


    /**
     * 角色更新
     * @param sysRoleDto
     * @return
     */
    CmsResult update(SysRoleDto sysRoleDto);


    /**
     * 角色删除
     * @param id
     * @return
     */
    boolean delete(String id);


    /**
     * 查询所有的角色
     * @return
     */
    List<SysRoleDto> listRoles();

    /**
     * 根据 用户ID查询用户是否存在超级用户角色
     * @param userId
     * @return
     */
    boolean isSuperRole(String userId);
}