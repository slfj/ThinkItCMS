package com.thinkit.cms.api.sysrolemenu;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysrolemenu.SysRoleAssignDto;
import com.thinkit.cms.dto.sysrolemenu.SysRoleMenuDto;

import java.util.List;

/**
* @author LG
*/


public interface SysRoleMenuService extends BaseService<SysRoleMenuDto> {


    void saveTreeAssign(SysRoleAssignDto roleAssignDto);

    List<String> queryMenuIdsByRoleId(String roleId);

    boolean isSysRole(String roleId);
}