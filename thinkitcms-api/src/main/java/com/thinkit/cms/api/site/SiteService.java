package com.thinkit.cms.api.site;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/


public interface SiteService extends BaseService<SiteDto> {

    /**
     * 创建站点
     * @param siteDto
     */
    void createSite(SiteDto siteDto);

    /**
     * 详情
     * @param id
     * @return
     */
    SiteDto detail(String id);

    /**
     * 更新站点
     * @param siteDto
     */
    void updateSite(SiteDto siteDto);

    /**
     * 设置默认站点
     * @param id
     * @return
     */
    CmsResult setDefault(String id);


    /**
     * 获取默认站点
     *  @param throwExp
     * @return
     */
    String getDefault(boolean throwExp);



    /**
     * 获取站点的 doamin
     *  @param prefix
     * @return
     */
    String getDoamin(String prefix,String siteId);

    /**
     * 获取默认站点
     *  @param throwExp
     *  @param  userId
     * @return
     */
    String getDefault(boolean throwExp,String userId);


    /**
     * 获取默认站点的默认配置
     * @param throwExp
     * @return
     */
    SiteConf getDefConf(boolean throwExp);



    /**
     * 获取默认站点的默认配置
     * @param siteId
     * @return
     */
    SiteConf getDefConf(String siteId);



    /**
     * 获取默认站点的默认配置
     * @param throwExp
     * @param userId
     * @return
     */
    SiteConf getDefConf(boolean throwExp,String userId);

    /**
     * 获取默认站点 默认末班
     * @param throwExp
     * @param siteId
     * @return
     */
    String getDefaultTemp(boolean throwExp,String siteId);

    /**
     * 查询站点列表
     * @return
     */
    CmsResult listSite();

    /**
     * 设置默认模板
     * @param tempId
     * @param  siteId
     * @return
     */
    CmsResult setDefTemplate(String tempId,String siteId);


    /**
     * 获取默认站点的模板
     * @return
     */
    CmsResult getDefTemplate();

    /**
     * 查询站点列表
     * @param pageDto
     * @return
     */
    PageModel<SiteDto> page(PageModel<SiteDto> pageDto);


    /**
     * 加载站点模板文件
     * @return
     */
    CmsResult loadTempFolder();


    /**
     * 获取站点列表
     * @return
     */
    Set<String> listSiteIds();

    /**
     * 更新站点模板为null
     * @param id
     */
    void updateTmpSetNull(String id);


    /**
     * 全站生成
     * @param siteId
     */
    String createSiteFinish(String siteId);


    /**
     * 同步数据到es
     * @param siteId
     * @return
     */
    String syncToEs(String siteId);


    /**
     * 查询站点域名列表
     * @return
     */
    List<String> listDomain();


    /**
     * 删除
     * @param id
     * @return
     */
    boolean deleteSite(String id);


    /**
     * 定时生成首页
     * @param siteId
     */
    void jonIndex(String siteId);
}