package com.thinkit.cms.api.open;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.thinkit.cms.dto.open.OpenSearchDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;

import java.util.Map;

public interface OpenSearchService {

    /**
     * 查询文档分页查询
     * @param openSearchDto
     * @return
     */
    Page<Map> searchDoc(PageModel<OpenSearchDto> openSearchDto);


    /**
     * 获取版本号升级提醒
     * @return
     */
    CmsResult searchVersion(String domain);
}
