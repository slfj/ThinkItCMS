package com.thinkit.cms.api.modeldesign;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.modeldesign.ModelDesignDto;
import com.thinkit.cms.dto.modeldesign.SaveModelDesign;

import java.util.List;

/**
* @author LG
*/


public interface ModelDesignService extends BaseService<ModelDesignDto> {


    /**
     * 保存模型设计
     * @param saveModelDesign
     */
    void saveMdesign(SaveModelDesign saveModelDesign);

    /**
     * 获取设计详情
     * @param id
     * @return
     */
    List<ModelDesignDto> detail(String id);

    /**
     * 创建索引
     * @param saveModelDesign
     */
    void createIndex(SaveModelDesign saveModelDesign);
}