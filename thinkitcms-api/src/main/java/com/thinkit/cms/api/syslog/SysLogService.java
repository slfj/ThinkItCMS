package com.thinkit.cms.api.syslog;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.syslog.SysLogDto;
import org.aspectj.lang.ProceedingJoinPoint;

/**
* @author lg
*/


public interface SysLogService extends BaseService<SysLogDto> {


    /**
     * 创建操作日志记录
     * @param pjp
     * @param time
     */
    void saveLog(ProceedingJoinPoint pjp, Long time);

}