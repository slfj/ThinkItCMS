package com.thinkit.cms.api.formdata;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.enums.ModelEnum;

import java.util.Map;

/**
* @author LG
*/


public interface FormDataService extends BaseService<FormDataDto> {


    /**
     * 构建模型数据
     * @param siteId
     * @param formModelId
     * @param extra
     * @param modelEnum
     * @param rowId
     */
    void formData(ModelEnum modelEnum,String rowId,String siteId, String formModelId, Map<String, Object> extra);

    /**
     * 查询模型对象数据
     * @param modelEnum
     * @param rowId
     * @param siteId
     * @param formModelId
     * @return
     */
    Map<String, Object> queryFormData(ModelEnum modelEnum, String rowId, String siteId, String formModelId);


    /**
     * 保存扩展数据
     * @param formDataDto
     */
    void saveExtra(FormDataDto formDataDto);


    /**
     * 获取记录详情
     * @param id
     * @param isExtra
     * @return
     */
    FormDataDto detail(String id,Boolean isExtra);


    /**
     * 更新扩展数据
     * @param formDataDto
     */
    void updateExtra(FormDataDto formDataDto);


    /**
     * 删除表单数据
     * @param id
     * @return
     */
    boolean delete(String id);
}