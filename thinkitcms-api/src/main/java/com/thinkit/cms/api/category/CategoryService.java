package com.thinkit.cms.api.category;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.category.*;
import com.thinkit.cms.dto.sysmenu.CategoryTree;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Tree;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/


public interface CategoryService extends BaseService<CategoryDto> {


    /**
     * 查询栏目树
     * @param pid
     * @return
     */
    Tree<CategoryTree> categoryTree(String pid);


    /**
     * 获取table
     * @param pid
     * @return
     */
    List<Tree<CategoryTree>> treeTable(String pid);


    /**
     * 保存栏目
     * @param addEditCategory
     */
    void save(AddEditCategory addEditCategory);


    /**
     * 更新
     * @param addEditCategory
     */
    void update(AddEditCategory addEditCategory);


    /**
     * 查询详情
     * @param id
     * @return
     */
    CategoryDto detail(String id,String siteId);


    /**
     * 查询栏目数据详情
     * @param id
     * @return
     */
    CategoryDto categoryData(String id);


    /**
     * 手动执行 栏目首页模板生成
     * @param publishCategory
     */
    String createTmpHome( PublishCategory publishCategory);



    /**
     *  手动任务执行 栏目首页模板生成
     * @param publishCategory
     */
    void jobCreateTmpHome( PublishCategory publishCategory);


    /**
     * 栏目首页模板批量生成
     * @param publishCategory
     */
    String createTmpHomes( PublishCategory publishCategory);

    /**
     * 生成栏目
     * @param categoryJob
     */
    void genStaticFile(CategoryJob categoryJob);

    /**
     * 获取子栏目id集合
     * @param id
     * @return
     */
    List<String> childs(String id);


    /**
     * 初始化站点首页
     */
    void initIndexTemp(String siteId);


    /**
     * 发布内容时查询发布的栏目
     * @param pid
     * @return
     */
    List<Tree<CategoryTree>> treeTableForContent(String pid);


    /**
     * 删除栏目
     * @param categoryIds
     * @return
     */
    boolean deletes(List<String> categoryIds);


    /**
     * 生成
     * @param user
     * @param siteId
     * @param taskId
     * @param taskEnum
     */
    void executer(User user, String siteId, String taskId, TaskEnum taskEnum);


    /**
     * 清理栏目任务
     */
    void clearTaskId(String categoryId);

    /**
     * 更新栏目静态化进度
     * @param categoryId
     */
    void upStatus(String categoryId, TaskStatusEnum taskStatusEnum);

    /**
     * 查询站点的栏目
     * @param siteId
     * @return
     */
    Set<String> listIds(String siteId);

    /**
     * 获取 code
     * @param categoryId
     * @return
     */
    String queryCode(String categoryId);
}