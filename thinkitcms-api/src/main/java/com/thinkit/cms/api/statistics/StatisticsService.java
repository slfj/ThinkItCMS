package com.thinkit.cms.api.statistics;

import com.thinkit.cms.dto.statistics.*;

import java.util.List;
import java.util.Map;

public interface StatisticsService {


    /**
     * 统计内容数量
     * @param statisticsDto
     */
    Map<String,Object> contentTotal(StatisticsDto statisticsDto);

    /**
     * 查询每天发布的内容数量统计
     * @param statisticsDto
     * @return
     */
    List<DateCountResDto> dateTotal(StatisticsDto statisticsDto);


    /**
     * 统计栏目分类占比
     * @return
     */
    List<TxtCountResDto> categoryTotal(StatisticsCategoryDto statisticsCategoryDto);

    /**
     * 排名
     * @param topContent
     * @return
     */
    List<TopContent> topTotal(TopContent topContent);
}
