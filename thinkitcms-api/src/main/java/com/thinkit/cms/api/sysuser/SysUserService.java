package com.thinkit.cms.api.sysuser;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysuser.LockUserDto;
import com.thinkit.cms.dto.sysuser.SysUserDto;
import com.thinkit.cms.dto.sysuser.UpdateBaseUser;
import com.thinkit.cms.dto.sysuser.UpdatePw;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.user.User;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/


public interface SysUserService extends BaseService<SysUserDto> {


    /**
     * 根据用户名加载用户信息
     * @param userName
     * @return
     */
   User loadUserName(String userName);


    /**
     * 保存用户
     * @param sysUserDto
     * @return
     */
    CmsResult save(SysUserDto sysUserDto);


    /**
     * 更新用户
     * @param sysUserDto
     * @return
     */
    CmsResult update(SysUserDto sysUserDto);


    /**
     * 用户详情
     * @param id
     * @return
     */
    CmsResult detail(String id);


    /**
     * 删除用户胡
     * @param id
     * @return
     */
    boolean delete(String id);


    /**
     * 查询用户有哪些角色 以及选中的哪些角色
     * @param userId
     * @return
     */
    CmsResult tipRoles(String userId);


    /**
     * 锁定/解锁 用户
     * @param lockUserDto
     * @return
     */
    CmsResult lock(LockUserDto lockUserDto);

    /**
     * 获取用户信息
     * @return
     */
    CmsResult info();

    boolean isSuperUser(String userId);

    /**
     * 设置默认站点
     * @param siteId
     * @return
     */
    CmsResult setDefaultSite(String siteId);


    /**
     * 获取站点
     * @param userId
     * @return
     */
    String getDefaultSit(String userId);

     SysUserDto userInfo();

     void updateBaseInfo(UpdateBaseUser updateBaseUser);



      void updatePw(UpdatePw updatePw);

    /**
     * 查询权限标识
     * @param uid
     * @param isSuper
     * @return
     */
    List<String> queryAuthSign(String uid, Boolean isSuper);

    /**
     * 验证码是否合法
     * @param verifyUid
     * @param verifyCode
     * @return
     */
    boolean checkVerifyCode(String verifyUid, String verifyCode);
}