package com.thinkit.cms.api.sysmenu;

import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.dto.sysmenu.SysMenuDto;
import com.thinkit.cms.utils.Tree;
import com.thinkit.cms.model.CmsResult;

import java.io.InputStream;
import java.util.List;

/**
* @author LG
*/


public interface SysMenuService extends BaseService<SysMenuDto> {
    /**
     * 获取后台左侧菜单树
     * @return
     */
    List<Tree> menuSlideTree();

    /**
     * 获取 菜单列表 table 树 菜单管理
     * @return
     */
    List<Tree> menuTableTree();


    /**
     * @param roleId 角色 ID
     * @return
     */
    CmsResult menuTreeAssign(String roleId);

    /**
     * 保存菜单
     * @param sysMenuDto
     */
    void save(SysMenuDto sysMenuDto);

    /**
     * 获取菜单详情
     * @param rowId
     * @return
     */
    SysMenuDto detail(String rowId);

    void update(SysMenuDto sysMenuDto);

    boolean delete(String id);

    /**
     * 获取菜单配置文件（前端使用）
     * @return
     */
    void downMenuTs();
}