package com.thinkit.cms.dto.sysmenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LONG
 */
@Getter
@Setter
@Accessors(chain = true)
public class CategoryTree <T> extends Tree<T> {
    /**
     路由名称
     */
    @JsonProperty("key")
    private String id;
    /**
     路由路径
     */
    private String name;

    /**
     编码
     */
    private String code;

    /**
     封面图片
     */
    private String cover;

    /**
     生成路径规则
     */
    private String pathRule;


    /**
     是否只外链 只是外链栏目直接访问
     */
    private Integer onlyUrl;


    /**
     每页数据条数
     */
    private Integer pageSize;

    /**
     允许投稿
     */
    private Integer allowContribute;

    /**
     顺序
     */
    private Integer sort;

    /**
     是否在首页隐藏
     */
    private Integer hidden;

    /**
     是否是单页
     */
    private Integer singlePage;

    /**
     标题(分类标题用于 SEO 关键字优化)
     */
    private String title;

    /**
     关键词用于 SEO 关键字优化
     */
    private String keywords;

    /**
     描述用于 SEO 关键字优化
     */
    private String description;

    /**
     地址
     */
    private String url;


    /**
     * 手机端地址
     */
    private String mUrl;

    private List<String> expandedKeys = new ArrayList<>();

    private Integer allowCreate;

    private Boolean disabled = false;
}
