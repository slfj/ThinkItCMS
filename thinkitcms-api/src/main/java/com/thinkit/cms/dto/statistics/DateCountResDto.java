package com.thinkit.cms.dto.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Data
public class DateCountResDto implements Serializable {


    // @DateTimeFormat(pattern = "yy-MM-dd")
    @JsonFormat(pattern = "MM/dd")
    private Date date;

    private Long count=0L;
}
