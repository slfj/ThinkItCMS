package com.thinkit.cms.dto.sysuser;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class UpdatePw extends BaseDto {


    @NotBlank(groups = {ValidGroup1.class},message = "旧密码不能为空")
    private String pw;

    /**
     密码
     */
    @Size(min = 5,max =12 ,message = "密码最小5位数字最大12位数字")
    @NotBlank(groups = {ValidGroup1.class},message = "新密码不能为空")
    private String npw;


    /**
     密码
     */
    @Size(min = 5,max =12 ,message = "密码最小5位数字最大12位数字")
    @NotBlank(groups = {ValidGroup1.class},message = "确认新密码不能为空")
    private String anpw;

}