package com.thinkit.cms.dto.sysmenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author LONG
 */
@Getter
@Setter
@Accessors(chain = true)
public class MenuAssign<T> extends Tree<T> {

    @JsonProperty("title")
    private String  menuName;
    /**
     路由名称
     */
    private String name;

    /**
     0:目录 1：菜单 2：按钮
     */
    private Integer type;

    @JsonProperty("key")
    private String id;


    private Boolean isLeaf;
}
