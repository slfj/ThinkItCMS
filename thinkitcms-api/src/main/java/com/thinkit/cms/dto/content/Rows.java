package com.thinkit.cms.dto.content;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author LONG
 */
@Data
public class Rows implements Serializable {
    /**
     内容IDid
     */
    private List<String> keys;

    private Integer status;

    public Rows(List<String> keys, Integer status) {
        this.keys = keys;
        this.status = status;
    }
}
