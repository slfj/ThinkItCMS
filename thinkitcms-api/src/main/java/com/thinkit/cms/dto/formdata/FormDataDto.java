package com.thinkit.cms.dto.formdata;
import com.thinkit.cms.annotation.IndexField;
import com.thinkit.cms.annotation.IndexId;
import com.thinkit.cms.annotation.IndexName;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.enums.IdType;
import com.thinkit.cms.model.ModelData;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@IndexName("tk_form_data")
public class FormDataDto extends ModelData {


       public FormDataDto(){

       }

        public FormDataDto(String formModelId, String formData, String siteId) {
            this.formModelId = formModelId;
            this.formData = formData;
            this.siteId = siteId;
        }


        @IndexId(type = IdType.CUSTOMIZE,prefix = SysConst.formDataPrefix)
        public String id;

    /**
         表单模型ID
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String formModelId;


        /**
         表单模型Code
         */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String formModelCode;

       /**
         表单内容
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true,convertMap = true,exist = false)
        private String formData;

       /**
         是否删除 0：正常 1：删除
       */
        @IndexField(exist = false)
        private Boolean deleted;

       /**
         站点ID
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String siteId;


        @IndexField(exist = false)
        private String rowId;

        @IndexField(exist = false)
        private String modual;
}