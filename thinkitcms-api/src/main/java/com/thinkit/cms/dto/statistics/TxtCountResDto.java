package com.thinkit.cms.dto.statistics;

import lombok.Data;

import java.io.Serializable;

@Data
public class TxtCountResDto implements Serializable {


    private String name;

    private Long value=0L;
}
