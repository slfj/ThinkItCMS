package com.thinkit.cms.dto.template;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class RenameFile extends BaseDto {

       /**
         模板ID
       */
        @NotBlank(message = "模板不能为空")
        private String absolutePath;


        /**
         模板路径
         */
        @NotBlank(message = "文件名称不能为空")
        private String name;

}