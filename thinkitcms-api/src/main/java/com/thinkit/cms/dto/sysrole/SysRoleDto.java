package com.thinkit.cms.dto.sysrole;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysRoleDto extends BaseDto {
       /**
         角色名称
       */
        @NotBlank(message = "角色名称不能为空")
        private String roleName;

       /**
         角色标识
       */
        @NotBlank(message = "角色标识不能为空")
        private String roleSign;

       /**
         备注
       */
        private String remark;

       /**
         是否是系统内置 0：非系统内置 1：系统内置
       */
        private Integer isSystem;


        @Override
       public <T extends BaseModel> QueryWrapper<T> toWrapper() {
         QueryWrapper queryWrapper = new QueryWrapper<>();
         queryWrapper.like(Checker.BeNotBlank(roleName),"role_name",roleName);
         queryWrapper.like(Checker.BeNotBlank(remark),"remark",remark);
         queryWrapper.like(Checker.BeNotBlank(roleSign),"role_sign",roleSign);
         queryWrapper.eq(Checker.BeNotNull(isSystem),"is_system",isSystem);
         queryWrapper.orderByDesc("is_system","gmt_create");
         return queryWrapper;
       }
}