package com.thinkit.cms.dto.statistics;

import com.thinkit.cms.model.BaseDto;
import lombok.Data;

@Data
public class StatisticsDto extends BaseDto {

    private String startTime;

    private String endTime;

    private Integer days=30;
}
