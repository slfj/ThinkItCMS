package com.thinkit.cms.dto.statistics;

import lombok.Data;

import java.io.Serializable;

@Data
public class StatisticsResDto implements Serializable {


    private Long allTotal=0L;

    private Long pubTotal=0L;

    private Long clickTotal=0L;

    private Long commentTotal=0L;

}
