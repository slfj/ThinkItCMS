package com.thinkit.cms.dto.sysmenu;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * @author LONG
 */
@Getter
@Setter
@Accessors(chain = true)
public class MenuTable<T> extends Tree<T> {

    private String  menuName;


    private String api;
    /**
     路由名称
     */
    private String name;

    /**
     路由路径
     */
    private String path;

    /**
     菜单组件地址
     */
    private String component;

    /**
     排序
     */
    private Integer order;

    /**
     路由 Meta 元信息
     */
    private Object meta;

    /**
     0:目录 1：菜单 2：按钮
     */
    private Integer type;

    @JsonProperty("key")
    private String id;

}
