package com.thinkit.cms.dto.syslicense;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkit.cms.annotation.License;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysLicenseDto extends BaseDto {
       /**
         站点域名
       */
        @License
        private String domain;

        @NotEmpty(message = "请输入授权域名")
        private List<String> domains;

       /**
         颁发者
       */
       @NotBlank(message = "请输入颁发者")
       @License
       private String issuer;

       /**
         企业/组织名称
       */
       @NotBlank(message = "请输入要授权的企业/组织名称")
       @License
       private String organization;

       /**
         生效开始时间
       */
       // @NotNull(message = "请输入生效开始时间")
       @JsonFormat(pattern = "yyyy-MM-dd")
       @License
       private Date effectDate;

       /**
         截止时间
       */
       @JsonFormat(pattern = "yyyy-MM-dd")
       @License
       private Date deadline;


       @NotEmpty(message = "请输入生效的起止时间")
       private List<String> times;

       /**
         签名
       */
       // @License
       private String signaturer;

       /**
         版本号
       */
       @NotBlank(message = "请输入版本号")
       @License
       private String version;

       /**
         证书类型
       */
       @NotBlank(message = "请选择证书类型")
       @License
       private String certificateType;

       /**
         0:正常 1：删除
       */
        private Boolean deleted;



        private String licenseUrl;

       public <T extends BaseModel> QueryWrapper<T> toWrapper() {
              QueryWrapper queryWrapper =  new QueryWrapper<>();
              queryWrapper.like(Checker.BeNotBlank(domain),"domain",domain);
              queryWrapper.like(Checker.BeNotBlank(issuer),"issuer",issuer);
              queryWrapper.like(Checker.BeNotBlank(organization),"organization",organization);
              queryWrapper.like(Checker.BeNotBlank(version),"version",version);
              queryWrapper.eq(Checker.BeNotBlank(certificateType),"certificate_type",certificateType);
              return queryWrapper;
       }


}