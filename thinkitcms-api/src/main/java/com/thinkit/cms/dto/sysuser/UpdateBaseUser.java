package com.thinkit.cms.dto.sysuser;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class UpdateBaseUser extends BaseDto {


    @NotBlank(groups = {ValidGroup1.class},message = "用户昵称不能为空")
    private String nickName;

    /**
     邮箱
     */
    @NotBlank(groups = {ValidGroup1.class},message = "用户邮箱不能为空")
    private String email;


    /**
     手机号
     */
    @NotBlank(groups = {ValidGroup1.class},message = "用户手机号不能为空")
    private String mobile;

    /**
     0:男 1：女
     */
    private Integer sex;

    /**
     * 备注
     */
    private String remark;

    @NotBlank(groups = {ValidGroup2.class},message = "头像不能为空")
    private String avatar;
}