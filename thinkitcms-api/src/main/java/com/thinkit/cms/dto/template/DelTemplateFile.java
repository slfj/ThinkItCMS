package com.thinkit.cms.dto.template;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class DelTemplateFile extends BaseDto {

       /**
         模板ID
       */
        @NotBlank(message = "模板不能为空")
        private String absolutePath;


        /**
         模板路径
         */
        @NotBlank(message = "模板路径不能为空")
        private String tempPath;

        /**
         是否是目录
         */
        @NotNull(message = "文件类型不能为空")
        private Boolean isDirectory;
}