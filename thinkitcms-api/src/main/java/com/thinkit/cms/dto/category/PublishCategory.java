package com.thinkit.cms.dto.category;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thinkit.cms.annotation.ValidGroup3;
import com.thinkit.cms.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class PublishCategory implements Serializable {


        @NotBlank(message = "栏目ID不能为空")
        private String categoryId;

        /**
         * 是否包含子栏目
         */
        private Boolean containChild;

        /**
         * 是否包含时间范围
         */
        private Boolean containTimes=false;


        /**
         * 开始结束时间
         */
        private List<Date> startEndTime;


        /**
         * 是否强制更新栏目地址
         */
        private Boolean forceGen = false;


        /**
         * 是否异步
         */
        private Boolean async = false;



        @NotEmpty(message = "栏目ID不能为空",groups = {ValidGroup3.class})
        private List<String> categoryIds = new ArrayList<>();


        @JsonIgnoreProperties(ignoreUnknown = true)
        private User user;

        private String  taskId;

        private Integer count=1;
}