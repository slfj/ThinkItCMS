package com.thinkit.cms.dto.modeldesign;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class ModelDesignDto extends BaseDto {
       /**
         表单模型主键
       */
        private String formModelId;

       /**
         站点ID
       */
        private String siteId;

       /**
         字段名称
       */
        private String fieldName;

       /**
         字段编码
       */
        private String fieldCode;

       /**
         字段类型
       */
        private String fieldType;

       /**
         提示消息：请输入 xxx
       */
        private String fieldPlaceholder;

       /**
         字段默认值（{"val":"[]"}）
       */
        private String fieldDefaultVal;

       /**
         最大字段长度
       */
        private Integer length;

       /**
         是否必填 1：必填 0：非必填
       */
        private Integer isRequire;

       /**
         选项
       */
        private List<OptionsDto> options;



        private String optionStr;

       /**
         排序
       */
        private Integer sort;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


        /**
         * 是否是新系统字段 1：是 2：否
         */
        private String isSysfield;


        /**
         * 是否 索引 是否索引 0：不索引 1：索引
         */
        private String isIndex="1";

        /**
         *  索引类型
         */
        private String indexType="TEXT";
}