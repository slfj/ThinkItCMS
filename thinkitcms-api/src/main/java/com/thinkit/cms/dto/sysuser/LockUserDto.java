package com.thinkit.cms.dto.sysuser;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class LockUserDto extends BaseDto {
       /**
         用户账户名
       */

        @NotEmpty(groups = {ValidGroup1.class},message = "请选择一条记录")
        private List<String> userIds;

       /**
         用户名称(昵称)
       */
        @NotBlank(groups = {ValidGroup1.class},message = "锁定状态不能为空")
        private String status;


}