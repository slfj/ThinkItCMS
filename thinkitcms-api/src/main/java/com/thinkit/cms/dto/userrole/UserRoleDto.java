package com.thinkit.cms.dto.userrole;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class UserRoleDto extends BaseDto {
       /**
         用户ID
       */

       @NotBlank(message = "请输入用户ID",groups = {ValidGroup1.class})
        private String userId;

       /**
         角色ID
       */
        private String roleId;


        @NotEmpty(message = "请选择至少一个角色",groups = {ValidGroup1.class})
        private List<String> roleIds;


        public UserRoleDto(String userId, String roleId) {
            this.userId = userId;
            this.roleId = roleId;
        }

        public UserRoleDto(){

        }
}