package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.thinkit.cms.annotation.*;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.enums.FieldType;
import com.thinkit.cms.enums.IdType;
import com.thinkit.cms.model.ModelData;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@IndexName("tk_content")
public class ContentDto extends ModelData {
       /**
         主键
       */
        @IndexId(type = IdType.CUSTOMIZE, prefix = SysConst.contentPrefix)
        public String id;

        @IndexField(exist = false)
        private List<String> ids;
       /**
         站点ID
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String siteId;

       /**
         标题
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String title;

       /**
         关键字
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String keywords;

       /**
         简介
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String description;

       /**
         副标题
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String subTitle;

       /**
         栏目名称
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String categoryName;


        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String categoryCode;


        /**
         * 模型名称
         */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String  modelName;

       /**
         分类
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        @NotBlank(message = "请选择分类",groups = {ValidGroup1.class, ValidGroup2.class})
        private String categoryId;

       /**
         父分类ID
       */
        private String parentCategoryId;

       /**
         模型表ID
       */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String formModelId;


        /**
         模型表code
         */
        @IndexField(fieldType = FieldType.TAG, ignoreCase = true)
        private String formModelCode;

       /**
         是否转载
       */
        @IndexField(fieldType = FieldType.NUMERIC, ignoreCase = true)
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer copied;

       /**
         作者
       */
        @IndexField(fieldType = FieldType.TEXT)
        private String author;

       /**
         编辑
       */
        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String editor;

       /**
         是否置顶
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer isTop;

       /**
         是否推荐
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer isRecomd;

       /**
         是否头条
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        @JsonSerialize(using = ToStringSerializer.class)
        private Integer isHeadline;

       /**
         外链
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer onlyUrl;

       /**
         拥有附件列表
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer hasFiles;

       /**
         是否有推荐内容
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer hasRelated;

       /**
         是否包标签
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer hasTags;

       /**
         地址
       */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String url;


        /**
         * 手机端地址
         */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String mUrl;

       /**
         置顶标签
       */
        @IndexField(fieldType = FieldType.TAG,exist = false)
        private String topTag;

       /**
         封面
       */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String cover;

       /**
         多图轮播
       */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String imgs;

       /**
         评论数
       */
        @IndexField(fieldType = FieldType.NUMERIC, exist = false)
        private Integer comments;

       /**
         点击数
       */
        @IndexField(fieldType = FieldType.NUMERIC, exist = false)
        private Integer clicks;

       /**
         日期生成规则
       */
        private String pathRule;

       /**
         点赞数
       */
        @IndexField(fieldType = FieldType.NUMERIC, exist = false)
        private Integer likes;

        @IndexField(fieldType = FieldType.TEXT, ignoreCase = true)
        private String content;

       /**
         发布日期
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date publishDate;

        @IndexField(exist = false)
        private Date[] publishDates;

       /**
         发表用户ID
       */
        @IndexField(fieldType = FieldType.TAG, exist = false)
        private String publishUserId;

       /**
         顺序
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Integer sort;

       /**
         状态：0、草稿 1、已发布 2、删除
       */
        @IndexField(fieldType = FieldType.NUMERIC,weight = 3.0)
        private Integer status;

       /**
         创建用户id
       */
        @IndexField(fieldType = FieldType.TEXT, exist = false)
        private String createId;

       /**
         修改人id
       */
        private String modifiedId;


        /**
         * 来源
         */
        @IndexField(fieldType = FieldType.TEXT)
        private String source;

       /**
         创建时间
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Date gmtCreate;

       /**
         修改时间
       */
        @IndexField(fieldType = FieldType.NUMERIC)
        private Date gmtModified;

       /**
         创建人名称
       */
        @IndexField(exist = false)
        private String createName;

       /**
         修改人名称
       */
        @IndexField(exist = false)
        private String modifiedName;


        /**
         * 分类多选
         */
        @IndexField(exist = false)
        private List<String> categoryIds;


        private Integer isSyncEs=0;
}