package com.thinkit.cms.dto.template;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class AddTemplate extends BaseDto {
       /**
         文件名称
       */
        @NotBlank(message = "文件名称不能为空")
        private String name;

       /**
         模板ID
       */
        @NotBlank(message = "模板不能为空")
        private String tempId;


        /**
         模板路径
         */
        @NotBlank(message = "模板路径不能为空")
        private String tempPath;

       /**
          是否是目录
       */
        private Boolean isDirectory;

        /**
         * 后缀
         */
        private String suffix;
}