package com.thinkit.cms.dto.sysrole;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class MyRoleDto   {
    List<SysRoleDto> roles ;
    List<String> roleIds ;

    public MyRoleDto(List<SysRoleDto> roles, List<String> roleIds) {
        this.roles = roles;
        this.roleIds = roleIds;
    }
}