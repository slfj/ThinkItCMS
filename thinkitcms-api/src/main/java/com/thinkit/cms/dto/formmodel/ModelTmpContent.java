package com.thinkit.cms.dto.formmodel;

import com.thinkit.cms.model.BaseDto;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class ModelTmpContent extends BaseDto {

    @NotBlank(message = "部件编码不能为空")
    private String formCode;

    private String content;
}