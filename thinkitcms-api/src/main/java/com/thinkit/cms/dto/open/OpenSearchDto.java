package com.thinkit.cms.dto.open;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.model.BaseDto;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OpenSearchDto extends BaseDto  {

    @NotBlank(message = "关键字不能为空",groups = {ValidGroup1.class})
    private String keywords;

}
