package com.thinkit.cms.dto.directs;

import lombok.Data;

/**
 * @author LONG
 */
@Data
public class ConfigParam {
    private Long cacheTime = 10L;
    private String timeUnit ="s";
    private String cacheName= "";
    private String location ="";
    private Boolean enable = true;
}
