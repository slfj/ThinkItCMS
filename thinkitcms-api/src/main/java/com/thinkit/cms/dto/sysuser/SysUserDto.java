package com.thinkit.cms.dto.sysuser;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysUserDto extends BaseDto {
       /**
         用户账户名
       */

        @NotBlank(groups = {ValidGroup1.class},message = "用户账户不能为空")
        private String userName;

       /**
         用户名称(昵称)
       */
        @NotBlank(groups = {ValidGroup1.class, ValidGroup2.class},message = "用户昵称不能为空")
        private String nickName;

       /**
         密码
       */
        @NotBlank(groups = {ValidGroup1.class},message = "用户密码不能为空")
        private String password;

       /**
         邮箱
       */
        private String email;

       /**
         手机号
       */
        private String mobile;

       /**
         状态 0:正常，1:禁用
       */
        private String status;

       /**
         0:男 1：女
       */
        private Integer sex;

       /**
         组织id
       */
        private String orgId;

       /**
         组织代码
       */
        private String orgCode;

       /**
         组织名
       */
        private String orgName;

       /**
         默认站点ID
       */
        private String defSiteId;

       /**
         0:正常 1：删除
       */
        private Boolean deleted;

        private Boolean isSuperAdmin;

         private Date lastLoginTime;

         private String avatar;

         private String remark;

        @Override
        public <T extends BaseModel> QueryWrapper<T> toWrapper() {
            QueryWrapper queryWrapper = new QueryWrapper<>();
            queryWrapper.like(Checker.BeNotBlank(userName),"user_name",userName);
            queryWrapper.like(Checker.BeNotBlank(email),"email",email);
            queryWrapper.like(Checker.BeNotBlank(mobile),"mobile",mobile);
            queryWrapper.eq((Checker.BeNotBlank(status) && !"-1".equals(status)),"status",status);
            queryWrapper.like(Checker.BeNotBlank(nickName),"nick_name",nickName);
            queryWrapper.eq((Checker.BeNotNull(sex) && sex!=-1) ,"sex",sex);
            return queryWrapper;
        }

}