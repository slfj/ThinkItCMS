package com.thinkit.cms.dto.content;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup3;
import com.thinkit.cms.annotation.ValidGroup4;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.user.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class RowsDto extends BaseDto {
           /**
             发布内容
           */
        @NotEmpty(message = "请选择要操作的记录",groups = {ValidGroup1.class,ValidGroup3.class})
        private List<CkRow> ckRowList;

        private Integer status;

        private Boolean forceGen = false;

        @NotBlank(message = "请选择发布时间",groups = {ValidGroup3.class})
        private String publishTime;

        private Boolean async = false;
        /**
         * 上下文用户
         */
        @JsonIgnoreProperties(ignoreUnknown = true)
        private User user;


        @NotEmpty(message = "请选择要操作的记录",groups = {ValidGroup4.class})
        private List<String> ids;
}