package com.thinkit.cms.dto.syslog;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.utils.Checker;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.thinkit.cms.model.BaseDto;
/**
* @author lg
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysLogDto extends BaseDto {
       /**
         模块类型
       */
        private String type;

       /**
         模块名称
       */
        private String module;

       /**
         用户id
       */
        private String userId;

       /**
         用户账号
       */
        private String username;

       /**
         用户姓名
       */
        private String name;

       /**
         用户操作
       */
        private String operation;

       /**
         响应时间
       */
        private Long time;

       /**
         请求方法
       */
        private String method;

       /**
         请求参数
       */
        private String params;

       /**
         IP地址
       */
        private String ip;

       /**
         站点ID
       */
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


        public <T extends BaseModel> QueryWrapper<T> toWrapper() {
            QueryWrapper queryWrapper =  new QueryWrapper<>();
            queryWrapper.eq(Checker.BeNotBlank(username),"username",username);
            queryWrapper.like(Checker.BeNotBlank(name),"name",name);
            queryWrapper.eq(Checker.BeNotBlank(ip),"ip",ip);
            queryWrapper.like(Checker.BeNotBlank(operation),"operation",operation);
            queryWrapper.eq(Checker.BeNotBlank(type),"type",type);
            queryWrapper.orderByDesc("gmt_create");
            return queryWrapper;
        }

}