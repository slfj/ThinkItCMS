package com.thinkit.cms.dto.sysdict;

import com.thinkit.cms.utils.Tree;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
public class SysDictTree extends Tree {

       /**
         标签名
       */
        private String name;

       /**
         数据值
       */
        private String value;

       /**
         类型
       */
        private String type;

       /**
         描述
       */
        private String description;

       /**
         排序（升序）
       */
        private Integer num;

       /**
         是否删除 0：正常 1：删除
       */
        private Boolean deleted;


        private String key;
}