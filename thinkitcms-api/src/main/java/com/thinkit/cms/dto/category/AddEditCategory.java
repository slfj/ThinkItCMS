package com.thinkit.cms.dto.category;

import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.model.ModelData;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author LONG
 */
@Getter
@Setter
public class AddEditCategory extends ModelData {

    /**
     名称
     */
    private String name;

    /**
     父分类ID
     */
    private String pid;

    /**
     编码
     */
    private String code;

    /**
     封面图片
     */
    private String cover;

    /**
     生成路径规则
     */
    private String pathRule;

    /**
     模板路径
     */
    private String tmpPath;

    /**
     手机端模板路径
     */
    private String mobileTmpPath;

    /**
     外链跳转地址
     */
    private String goUrl;

    /**
     每页数据条数
     */
    private Integer pageSize;

    /**
     允许投稿 1:允许 2:不允许
     */
    private Integer allowCreate;

    /**
     顺序
     */
    private Integer sort;

    /**
     是否在首页隐藏
     */
    private Integer hidden;

    /**
     栏目扩展模型ID
     */
    private String formModelId;

    /**
     是否是单页
     */
    private Integer singlePage;

    /**
     标题(分类标题用于 SEO 关键字优化)
     */
    private String title;

    /**
     关键词用于 SEO 关键字优化
     */
    private String keywords;

    /**
     描述用于 SEO 关键字优化
     */
    private String description;

    /**
     是否包含字内容
     */
    private Integer containChild;


    //@NotEmpty(message = "请选择内容模型",groups = {ValidGroup1.class})
    private List<String> modelIds;



    /**
     地址
     */
    private String url;


    /**
     * 手机端地址
     */
    private String mUrl;


    /**
     * 栏目定时
     */
    private CategoryJob categoryJob;
}
