package com.thinkit.cms.core.base;
import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.base.BaseService;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author LONG
 */
public class BaseController<Service extends BaseService>{

    @Autowired(required = false)
    protected Service service;

    @Autowired
    protected SaTokenConfig saTokenConfig;

    @Autowired
    protected RedisTemplate redisTemplate;


    protected String getUserId(){
        return StpUtil.getLoginIdAsString();
    }
    /**
     * 获取上下文
     * @return
     */
    protected User getUserCtx(){
        Object object = StpUtil.getExtra(UserTypeDef.USER_CLIENT_DETAIL.getType());
        if(Checker.BeNull(object)){
            throw new AuthenException(403,"获取用户信息失败!");
        }
        User user = JSONUtil.toBean(JSONUtil.toJsonStr(object),User.class);
        return user;
    }

    protected String getSiteId(Boolean allow){
        Object siteObj = redisTemplate.opsForHash().get(SysConst.defSites,getUserId());
        if(Checker.BeNotNull(siteObj)){
            return String.valueOf(siteObj);
        }
        if(!allow){
            throw new CustomException(CmsResult.result(5040));
        }
        return null;
    }

    protected void switchTo(String userId){
        StpUtil.switchTo(userId);
    }


}