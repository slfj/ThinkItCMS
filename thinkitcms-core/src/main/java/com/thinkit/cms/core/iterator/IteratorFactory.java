package com.thinkit.cms.core.iterator;

public interface IteratorFactory {

   Iterator iterator(Class cls);
}
