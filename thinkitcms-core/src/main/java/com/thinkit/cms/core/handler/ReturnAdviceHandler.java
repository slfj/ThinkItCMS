package com.thinkit.cms.core.handler;

import com.google.common.collect.Lists;
import com.thinkit.cms.authen.annotation.NoDecorate;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.List;

/**
 * @author LONG
 */
@Slf4j
@RestControllerAdvice
public class ReturnAdviceHandler implements ResponseBodyAdvice<Object> {

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		boolean bePrivate = Modifier.PRIVATE == returnType.getMethod().getModifiers();
		if (bePrivate) {
			return false;
		}
		NoDecorate noDecorate = returnType.getMethod().getAnnotation(NoDecorate.class);
		if (noDecorate != null) {
			return false;
		}
		return true;
	}

	@Override
	public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
                                  ServerHttpResponse response) {
		if(HttpMethod.OPTIONS.equals(request.getMethod())) {
			response.setStatusCode(HttpStatus.OK);
		}
		return wrapRes(body,response);
	}

	private Object wrapRes(Object body, ServerHttpResponse response){
        //封装返回结果
		if (ckNull(body)) {
			return CmsResult.result();
		}
		if (isCmsResult(body)) {
			return  body;
		}
		if (isCollection(body)) {
		   return wrapList(body);
		}
		if(isException(body)){
			return wrapException(body,response);
		}
		if (isModelAndView(body)) {
			ModelAndView mv = (ModelAndView) body;
			return mv;
		}
		return CmsResult.result(body);
	}


	private CmsResult wrapException(Object body, ServerHttpResponse response){
		Exception exception = ((Exception) body);
		response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
		return CmsResult.result(-1,exception.getMessage());
	}

	private CmsResult wrapList(Object body){
		Collection<?> object = (Collection<?>) body;
		List<?> list;
		if (object instanceof List && Checker.BeNotNull(object)) {
			list = (List<?>) object;
		} else {
			list = Lists.newArrayList(object);
		}
		return CmsResult.result(list);
	}


	private boolean ckNull(Object body){
		return Checker.BeNull(body);
	}

	private boolean isException(Object body){
		return Exception.class.isInstance(body);
	}

	private boolean isCollection(Object body){
		return Collection.class.isInstance(body);
	}

	private boolean isCmsResult(Object body){
		return CmsResult.class.isInstance(body);
	}

	private boolean isModelAndView(Object body){
		return ModelAndView.class.isInstance(body);
	}

}
