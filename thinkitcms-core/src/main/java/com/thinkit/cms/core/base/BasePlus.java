package com.thinkit.cms.core.base;

import cn.dev33.satoken.config.SaTokenConfig;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.thinkit.cms.authen.enums.UserTypeDef;
import com.thinkit.cms.authen.exceptions.AuthenException;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.core.utils.TaskUtils;
import com.thinkit.cms.model.BaseDto;
import com.thinkit.cms.model.BaseModel;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author LONG
 */
public class BasePlus<D extends BaseDto,T extends BaseModel,M extends BaseMapper<T>> extends ServiceImpl<M, T> {


    @Autowired
    protected SaTokenConfig saTokenConfig;

    @Autowired
    protected RedisTemplate redisTemplate;

    @Autowired
    protected TaskUtils taskUtils;

    @Autowired
    private ApplicationContext applicationContext;

    protected void publishEvent(ApplicationEvent... applicationEvents){
          if(Checker.BeNotEmpty(applicationEvents)){
              List<ApplicationEvent> list =  Arrays.stream(applicationEvents).collect(Collectors.toList());
              publishEvents(list);
          }
    }

    protected void publishEvents(List<ApplicationEvent> applicationEvents){
        if(Checker.BeNotEmpty(applicationEvents)){
            for(ApplicationEvent applicationEvent:applicationEvents){
                applicationContext.publishEvent(applicationEvent);
            }
        }
    }

    protected String getUserId(){
        return StpUtil.getLoginIdAsString();
    }

    protected void logout(){
         StpUtil.logout();
    }

    protected void logout(String userId){
        StpUtil.logout(userId);
    }

    protected String getSiteId(Boolean allow){
        Object siteObj = redisTemplate.opsForHash().get(SysConst.defSites,getUserId());
        if(Checker.BeNotNull(siteObj)){
            return String.valueOf(siteObj);
        }
        if(!allow){
            throw new CustomException(CmsResult.result(5040));
        }
        return null;
    }

    protected String id(){
        return IdUtil.getSnowflake(1, 31).nextIdStr();
    }


    /**
     * 获取request
     */
    protected HttpServletRequest getRequest(){
        return getRequestAttributes().getRequest();
    }

    /**
     * 获取response
     */
    protected HttpServletResponse getResponse(){
        return getRequestAttributes().getResponse();
    }

    /**
     * 获取上下文
     * @return
     */
    protected User getUserCtx(){
        Object object =  StpUtil.getExtra(UserTypeDef.USER_CLIENT_DETAIL.getType());
        if(object==null){
            throw new AuthenException(403,"用户信息获取失败!");
        }
        User user = JSONUtil.toBean(JSONUtil.toJsonStr(object),User.class);
        return user;
    }


    private static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("X-Real-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }
        if (StringUtils.isBlank(ip) || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
        }
        if (StringUtils.isNotBlank(ip) && !"unknown".equalsIgnoreCase(ip)
                && StringUtils.contains(ip, ",")) {
            // 多次反向代理后会有多个IP值，第一个为真实IP。
            ip = StringUtils.substringBefore(ip, ",");
        }
        // 处理localhost访问
        if (StringUtils.isBlank(ip) || "unkown".equalsIgnoreCase(ip)
                || StringUtils.split(ip, ".").length != 4) {
            try {
                InetAddress inetAddress = InetAddress.getLocalHost();
                ip = inetAddress.getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return ip;
    }


    protected void clearCache(String key){
        Set<String> keys = taskUtils.scan(key);
        if(Checker.BeNotEmpty(keys)){
            redisTemplate.delete(keys);
        }
    }

}
