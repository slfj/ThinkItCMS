package com.thinkit.cms.core.utils;

import cn.hutool.extra.spring.SpringUtil;
import com.thinkit.cms.model.BaseModel;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: DozerUtils
 * @Author: LG
 * @Date: 2019/5/16 10:41
 * @Version: 1.0
 **/
public class DozerUtils {

    private static Mapper mapper= SpringUtil.getBean(Mapper.class);

    /**
     * 封装dozer处理集合的方法：List<D> --> List<T>
     */
    public static <T extends BaseModel, D > List<T> D2TList(List<D> sourceList, Class<T> targetObjectClass) {
        List<T> targetList = new ArrayList<T>();
        for (D d : sourceList) {
            targetList.add(mapper.map(d, targetObjectClass));
        }
        return targetList;
    }


    /**
     * 封装dozer处理集合的方法：List<T> --> List<D>
     */


    public static <D , T extends BaseModel> List<D> T2DList(List<T> sourceList, Class<D> targetObjectClass) {
        List<D> targetList = new ArrayList<>();
        for (T t : sourceList) {
            targetList.add(mapper.map(t, targetObjectClass));
        }
        return targetList;
    }

    public static <D , T extends BaseModel> D T2D(T t, Class<D> targetObjectClass) {
        if(t!=null){
            return mapper.map(t, targetObjectClass);
        }
        return null;
    }

    public static  <T extends BaseModel, D >T D2T(D d,Class<T> targetObjectClass){
        if(d!=null){
            return mapper.map(d, targetObjectClass);
        }
        return null;
    }

    public static void copy(Object dest, Object target) {
        mapper.map(dest, target);
    }
}
