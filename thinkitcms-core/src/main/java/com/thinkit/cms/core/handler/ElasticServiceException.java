package com.thinkit.cms.core.handler;

/**
 * @author LONG
 */
public class ElasticServiceException extends RuntimeException{

    public ElasticServiceException(String msg){
        super(msg);
    }
}
