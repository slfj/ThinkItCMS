export interface UserState {
  userName?: string;
  avatar?: string;
  job?: string;
  organization?: string;
  email?: string;
  introduction?: string;
  jobName?: string;
  organizationName?: string;
  nickName?:string;
  mobile?: string;
  gmtCreate?: string;
  accountId?: string;
  certification?: number;
  authSign: string[];
}
