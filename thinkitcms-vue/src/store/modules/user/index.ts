import { defineStore } from 'pinia';
import {
  login as userLogin,
  logout as userLogout,
  getUserInfo,
  LoginData,
} from '@/api/user';
import { setToken, clearToken } from '@/utils/auth';
import { removeRouteListener } from '@/utils/route-listener';
import { JSEncrypt } from 'jsencrypt';
import { UserState } from './types';
import useAppStore from '../app';

const publicKey =
  'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCWthjviuWiln2z0cmc/PUOwsUH1uMf1bEdW0mfZDwFQLfFyERrnTAr9SkAsF/NvDBG14HG7GT1SyiIvQ8R3wz1EXr60tgZ5JkDh6tw9Q6j3KxzNyhHdFmRi41XbCy7O8P5I6+2jFZuXSlqY3/WwsSTSZ2bYfsXcitvMm12A51yQQIDAQAB';

const useUserStore = defineStore('user', {
  state: (): UserState => ({
    userName: undefined,
    avatar: undefined,
    job: undefined,
    organization: undefined,
    email: undefined,
    introduction: undefined,
    jobName: undefined,
    organizationName: undefined,
    mobile: undefined,
    gmtCreate: undefined,
    accountId: undefined,
    certification: undefined,
    nickName:undefined,
    authSign: [],
  }),

  getters: {
    userInfo(state: UserState): UserState {
      return { ...state };
    },
  },

  actions: {
    switchRoles() {
      return new Promise((resolve) => {
        // this.role = this.role === 'user' ? 'admin' : 'user';
        // resolve(this.role);
      });
    },
    // Set user's information
    setInfo(partial: Partial<UserState>) {
      this.$patch(partial);
    },

    // Reset user's information
    resetInfo() {
      this.$reset();
    },

    // Get user's information
    async info() {
      const res = await getUserInfo();
      this.setInfo(res.data);
    },

    // Login
    async login(loginForm: LoginData) {
      try {
        const encryptor = new JSEncrypt(); // 新建JSEncrypt对象
        encryptor.setPublicKey(publicKey); // 设置公钥
        // loginForm.pw = encryptor.encrypt(loginForm.pw);
        const params = {
          ...loginForm,
          pw: encryptor.encrypt(loginForm.pw),
          userName: encryptor.encrypt(loginForm.userName),
        };
        const res = await userLogin(params as LoginData);
        setToken(res.data.token);
      } catch (err) {
        clearToken();
        throw err;
      }
    },
    logoutCallBack() {
      const appStore = useAppStore();
      this.resetInfo();
      clearToken();
      removeRouteListener();
      appStore.clearServerMenu();
    },
    // Logout
    async logout() {
      try {
        await userLogout();
      } finally {
        this.logoutCallBack();
      }
    },
  },
});

export default useUserStore;
