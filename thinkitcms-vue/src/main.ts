import { createApp } from 'vue';
import ArcoVue from '@arco-design/web-vue';
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import globalComponents from '@/components';
import formCreate from '@form-create/arco-design'
import uploader from 'vue-simple-uploader'
import router from './router';
import store from './store';
import i18n from './locale';
import directive from './directive';
// import './mock';
import App from './App.vue';
import '@arco-design/web-vue/dist/arco.css';
import '@/assets/style/global.less';
import '@/api/interceptor';
import 'vue-simple-uploader/dist/style.css'

const app = createApp(App);
app.use(ArcoVue, {});
app.use(ArcoVueIcon);

app.use(router);
app.use(formCreate);
app.use(store);
app.use(i18n);
app.use(globalComponents);
app.use(directive);
app.use(uploader)
app.mount('#app');
