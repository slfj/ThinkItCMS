import { AppRouteRecordRaw } from '@/router/routes/types';
import { DEFAULT_LAYOUT } from '@/router/routes/base';

const ROUTES: AppRouteRecordRaw[] = [

    {
    path: '/system',
    name: 'system',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'system',
      icon: 'icon-settings',
      hideChildrenInMenu: false,
      locale: 'form.sys',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 3
    },
    children: [
        {
          path: 'menu',
          name: 'menu',
          component: () => import('@/views/menu/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'menu',
            icon: 'icon-ordered-list',
            hideChildrenInMenu: false,
            locale: 'form.menu',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'user',
          name: 'user',
          component: () => import('@/views/sysuser/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'sysuser',
            icon: 'icon-user',
            hideChildrenInMenu: false,
            locale: 'form.user',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'role',
          name: 'role',
          component: () => import('@/views/role/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'role',
            icon: 'icon-user-add',
            hideChildrenInMenu: false,
            locale: 'form.role',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'organize',
          name: 'organize',
          component: () => import('@/views/organize/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'organize',
            icon: 'icon-idcard',
            hideChildrenInMenu: false,
            locale: '组织管理',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'genCode',
          name: 'genCode',
          component: () => import('@/views/aicode/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'genCode',
            icon: 'icon-robot',
            hideChildrenInMenu: false,
            locale: 'form.aicode',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'druid',
          name: 'druid',
          component: () => import('@/views/druid/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'druid',
            icon: 'icon-dashboard',
            hideChildrenInMenu: false,
            locale: '数据监控',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/exception',
    name: 'exception',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'exception',
      icon: 'icon-exclamation-circle',
      hideChildrenInMenu: false,
      locale: '异常路由',
      hideInMenu: true,
      ignoreCache: false,
      activeMenu: '',
      noAffix: true,
      order: 3333
    },
    children: [
        {
          path: '403',
          name: '403',
          component: () => import('@/views/exception/403/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: '',
            hideChildrenInMenu: false,
            locale: '权限不足403',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: '404',
          name: '404',
          component: () => import('@/views/exception/404/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: '',
            hideChildrenInMenu: false,
            locale: '页面不存在404',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: '500',
          name: '500',
          component: () => import('@/views/exception/500/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '',
            icon: '',
            hideChildrenInMenu: false,
            locale: '页面错误500',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/dashboard',
    name: 'dashboard',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: false,
      authSign:'*',
      icon: 'icon-dashboard',
      hideChildrenInMenu: false,
      locale: 'menu.dashboard',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 0
    },
    children: [
        {
          path: 'workplace',
          name: 'Workplace',
          component: () => import('@/views/dashboard/workplace/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: 'icon-common',
            hideChildrenInMenu: false,
            locale: 'menu.dashboard.workplace',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'monitor',
          name: 'Monitor',
          component: () => import('@/views/dashboard/monitor/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: 'icon-desktop',
            hideChildrenInMenu: false,
            locale: 'menu.dashboard.monitor',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/visualization',
    name: 'visualization',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: false,
      authSign:'*',
      icon: 'icon-apps',
      hideChildrenInMenu: false,
      locale: 'menu.visualization',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 1
    },
    children: [
        {
          path: 'dataAnalysis',
          name: 'DataAnalysis',
          component: () => import('@/views/visualization/data-analysis/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: 'icon-desktop',
            hideChildrenInMenu: false,
            locale: 'menu.visualization.dataAnalysis',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'multyDataAnalysis',
          name: 'MultyDataAnalysis',
          component: () => import('@/views/visualization/multi-dimension-data-analysis/index.vue'),
          meta: {
            requiresAuth: false,
            authSign: '*',
            icon: 'icon-desktop',
            hideChildrenInMenu: false,
            locale: 'menu.visualization.multiDimensionDataAnalysis',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/site',
    name: 'site',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'site',
      icon: 'icon-qrcode',
      hideChildrenInMenu: false,
      locale: 'form.site',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 10
    },
    children: [
        {
          path: 'mysite',
          name: 'mysite',
          component: () => import('@/views/site/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'mysite',
            icon: 'icon-cloud',
            hideChildrenInMenu: false,
            locale: 'form.mysite',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'webfiles',
          name: 'webfiles',
          component: () => import('@/views/webfiles/search-table/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'webfiles',
            icon: 'icon-file',
            hideChildrenInMenu: false,
            locale: 'form.webfiles',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'source',
          name: 'source',
          component: () => import('@/views/source/card/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'source:source',
            icon: 'icon-cloud',
            hideChildrenInMenu: false,
            locale: '附件管理',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/directs',
    name: 'directs',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'directs',
      icon: 'icon-dice',
      hideChildrenInMenu: false,
      locale: 'form.directs',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: true,
      order: 30
    },
    children: [
        {
          path: 'direct',
          name: 'direct',
          component: () => import('@/views/directs/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'direct',
            icon: 'icon-sun',
            hideChildrenInMenu: false,
            locale: 'form.directs',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/modelTmp',
    name: 'modelTmp',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'modelTmp',
      icon: 'icon-menu-unfold',
      hideChildrenInMenu: false,
      locale: 'form.model',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: true,
      order: 20
    },
    children: [
        {
          path: 'model',
          name: 'model',
          component: () => import('@/views/sysmodel/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'model',
            icon: 'icon-message-banned',
            hideChildrenInMenu: false,
            locale: 'form.model',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'modelDesign',
          name: 'modelDesign',
          component: () => import('@/views/sysmodel/design.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'model:design',
            icon: '',
            hideChildrenInMenu: false,
            locale: 'form.modelDesign',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'templates',
          name: 'templates',
          component: () => import('@/views/templates/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'templates',
            icon: 'icon-paste',
            hideChildrenInMenu: false,
            locale: 'form.templates',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'fragment',
          name: 'fragment',
          component: () => import('@/views/fragment/card/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'fragment',
            icon: 'icon-branch',
            hideChildrenInMenu: false,
            locale: 'form.fragment',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'templateEdit',
          name: 'templateEdit',
          component: () => import('@/views/templates/edit.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'template:dev',
            icon: 'icon-pen-fill',
            hideChildrenInMenu: false,
            locale: 'form.templates.edit',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/category',
    name: 'category',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'category',
      icon: 'icon-apps',
      hideChildrenInMenu: false,
      locale: 'form.category',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: true,
      order: 40
    },
    children: [
        {
          path: 'categoryHome',
          name: 'categoryHome',
          component: () => import('@/views/category/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'categoryHome',
            icon: 'icon-mind-mapping',
            hideChildrenInMenu: false,
            locale: '栏目管理',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'categoryCreate',
          name: 'categoryCreate',
          component: () => import('@/views/category/step/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'categoryCreate',
            icon: 'category',
            hideChildrenInMenu: false,
            locale: 'form.categoryCreate',
            hideInMenu: true,
            ignoreCache: true,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/content',
    name: 'content',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'content',
      icon: 'icon-bookmark',
      hideChildrenInMenu: false,
      locale: 'form.content',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 50
    },
    children: [
        {
          path: 'contentHome',
          name: 'contentHome',
          component: () => import('@/views/content/search-table/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'content',
            icon: 'icon-book',
            hideChildrenInMenu: false,
            locale: 'form.content',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'reduction',
          name: 'reduction',
          component: () => import('@/views/content/search-table/reduction.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'reduction',
            icon: 'icon-book',
            hideChildrenInMenu: false,
            locale: '回收站',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/settings',
    name: 'settings',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'settings',
      icon: 'icon-tool',
      hideChildrenInMenu: false,
      locale: 'menu.user',
      hideInMenu: true,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 60
    },
    children: [
        {
          path: 'uview',
          name: 'uview',
          component: () => import('@/views/user/setting/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'uview',
            icon: '',
            hideChildrenInMenu: false,
            locale: 'uview',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }
        ,
        {
          path: 'setinfo',
          name: 'setinfo',
          component: () => import('@/views/user/info/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'user:info',
            icon: 'icon-edit',
            hideChildrenInMenu: false,
            locale: 'menu.user.info',
            hideInMenu: true,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/logs',
    name: 'logs',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'logs',
      icon: 'icon-message',
      hideChildrenInMenu: false,
      locale: '日志管理',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 70
    },
    children: [
        {
          path: 'views',
          name: 'views',
          component: () => import('@/views/logs/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'log:view',
            icon: 'icon-eye',
            hideChildrenInMenu: false,
            locale: '日志查看',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/dict',
    name: 'dict',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'dict',
      icon: 'icon-storage',
      hideChildrenInMenu: false,
      locale: '数据字典',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 5
    },
    children: [
        {
          path: 'dictmanager',
          name: 'dictmanager',
          component: () => import('@/views/dict/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'dict:page',
            icon: 'icon-nav',
            hideChildrenInMenu: false,
            locale: '字典管理',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }
  ,
  {
    path: '/license',
    name: 'license',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: true,
      authSign:'license',
      icon: 'icon-layers',
      hideChildrenInMenu: false,
      locale: '证书模块',
      hideInMenu: false,
      ignoreCache: false,
      activeMenu: '',
      noAffix: false,
      order: 65
    },
    children: [
        {
          path: 'manager',
          name: 'licenseManager',
          component: () => import('@/views/license/index.vue'),
          meta: {
            requiresAuth: true,
            authSign: 'license:manager',
            icon: 'icon-idcard',
            hideChildrenInMenu: false,
            locale: '证书管理',
            hideInMenu: false,
            ignoreCache: false,
            activeMenu: '',
            noAffix: false,
            order: 0
          }
        }

    ]
  }

];
export default ROUTES;
