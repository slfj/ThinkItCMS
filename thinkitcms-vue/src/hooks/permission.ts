import { RouteLocationNormalized, RouteRecordRaw } from 'vue-router';
import { useUserStore } from '@/store';

export default function usePermission() {
  const userStore = useUserStore();
  return {
    accessRouter(route: RouteLocationNormalized | RouteRecordRaw) {
      return (
        !route.meta?.requiresAuth ||
        !route.meta?.authSign ||
        route.meta?.authSign === '*' ||
        userStore.authSign.includes(route.meta.authSign)
      );
    },
    findFirstPermissionRoute(_routers: any, authSign: string[]) {
      const cloneRouters = [..._routers];
      while (cloneRouters.length) {
        const firstElement = cloneRouters.shift();
        if (
          authSign.includes(firstElement?.meta?.authSign) ||
          firstElement?.meta?.authSign === '*' ||
          !firstElement.meta?.requiresAuth
        )
          return { name: firstElement.name };
        if (firstElement?.children) {
          cloneRouters.push(...firstElement.children);
        }
      }
      return  null;
    },
    find403Route(_routers: any, authSign: string[]) {
      const cloneRouters = [..._routers];
      cloneRouters.push({name:'403'});
      return  '/exception/403';
    }
    // You can add any rules you want
  };
}
