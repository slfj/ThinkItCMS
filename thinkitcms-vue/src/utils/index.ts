type TargetContext = '_self' | '_parent' | '_blank' | '_top';

export const openWindow = (
  url: string,
  opts?: { target?: TargetContext; [key: string]: any }
) => {
  const { target = '_blank', ...others } = opts || {};
  window.open(
    url,
    target,
    Object.entries(others)
      .reduce((preValue: string[], curValue) => {
        const [key, value] = curValue;
        return [...preValue, `${key}=${value}`];
      }, [])
      .join(',')
  );
};

export const regexUrl = new RegExp(
  '^(?!mailto:)(?:(?:http|https|ftp)://)(?:\\S+(?::\\S*)?@)?(?:(?:(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}(?:\\.(?:[0-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))|(?:(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]+-?)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:[a-z\\u00a1-\\uffff]{2,})))|localhost)(?::\\d{2,5})?(?:(/|\\?|#)[^\\s]*)?$',
  'i'
);



export const subStr = (
  content: string,
  length:number
) => {
   if(content && length){
     content =content.length>length?`${content.substring(0,length)}..`:content;
   }
   return content;
};


export const down = (blob:any, suffix:string,res:any) => {
  const elink = document.createElement("a");   // 创建一个<a>标签
  elink.style.display = "none";                       // 隐藏标签
  elink.href = window.URL.createObjectURL(blob);      // 配置href
  // 获取后端返回的响应头中的名称
  let {filename} = res.headers;
  //  自定义名称
  // let newFilename = "样例文件" + new Date().getTime() + suffix; //自定义名字
  // let newFilename = decodeURIComponent(res.headers["content-disposition"].split(';')[1].split('=')[1])
  filename = decodeURIComponent(filename);
  elink.download = filename;
  document.body.appendChild(elink)
  elink.click();
  document.body.removeChild(elink);  // 移除<a>标签
  URL.revokeObjectURL(elink.href);   // 释放URL 对象(弹出框进行下载)
}

export const systemField = [
  {
    key: 0,
    fieldName: '标题',
    fieldCode:'title',
    fieldType: 'input',
    fieldPlaceholder: '请输入标题',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },
  {
    key: 1,
    fieldName: '关键字',
    fieldCode:'keywords',
    fieldType: 'input',
    fieldPlaceholder: '请输入关键字',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },
  {
    key: 2,
    fieldName: '简介',
    fieldCode:'description',
    fieldType: 'textarea',
    fieldPlaceholder: '请输入简介',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },
  {
    key: 3,
    fieldName: '副标题',
    fieldCode:'subTitle',
    fieldType: 'textarea',
    fieldPlaceholder: '请输入副标题',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },
  {
    key: 4,
    fieldName: '作者',
    fieldCode:'author',
    fieldType: 'input',
    fieldPlaceholder: '请输入作者',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    indexType:'TEXT',
    options:[],
    isIndex:'1',
    isSysfield:'1'
  },
  {
    key: 5,
    fieldName: '来源',
    fieldCode:'source',
    fieldType: 'textarea',
    fieldPlaceholder: '请输入来源',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },
  {
    key: 6,
    fieldName: '是否原创',
    fieldCode:'copied',
    fieldType: 'radio',
    fieldPlaceholder: '请选择是否原创',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[{"label": "是", "value": "1"},{"label": "否", "value": "2"}],
    isIndex:'1',
    indexType:'NUMERIC',
    isSysfield:'1'
  }, {
    key: 7,
    fieldName: '是否置顶',
    fieldCode:'isTop',
    fieldType: 'radio',
    fieldPlaceholder: '请选择是否置顶',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[{"label": "是", "value": "1"},{"label": "否", "value": "2"}],
    isIndex:'1',
    indexType:'NUMERIC',
    isSysfield:'1'
  }, {
    key: 8,
    fieldName: '是否推荐',
    fieldCode:'isRecomd',
    fieldType: 'radio',
    fieldPlaceholder: '请选择是否推荐',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[{"label": "是", "value": "1"},{"label": "否", "value": "2"}],
    isIndex:'1',
    indexType:'NUMERIC',
    isSysfield:'1'
  }, {
    key: 9,
    fieldName: '是否头条',
    fieldCode:'isHeadline',
    fieldType: 'radio',
    indexType:'NUMERIC',
    fieldPlaceholder: '请选择是否头条',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[{"label": "是", "value": "1"},{"label": "否", "value": "2"}],
    isIndex:'1',
    isSysfield:'1'
  },{
    key: 10,
    fieldName: '排序',
    fieldCode:'sort',
    fieldType: 'inputNumber',
    fieldPlaceholder: '请输入排序',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    indexType:'NUMERIC',
    isIndex:'1',
    isSysfield:'1'
  },{
    key: 11,
    fieldName: '上传封面',
    fieldCode:'cover',
    fieldType: 'uploadImgOne',
    fieldPlaceholder: '请选择是否原创',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'0',
    isSysfield:'1',
    indexType:'TEXT'
  },{
    key: 12,
    fieldName: '文章内容',
    fieldCode:'content',
    fieldType: 'edit',
    fieldPlaceholder: '请输入内容',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TEXT',
    isSysfield:'1'
  },{
    key: 13,
    fieldName: '文章标签',
    fieldCode:'topTag',
    fieldType: 'tag',
    fieldPlaceholder: '请输入标签',
    fieldDefaultVal: null,
    isRequire: '0',
    length:null,
    options:[],
    isIndex:'1',
    indexType:'TAG',
    isSysfield:'1'
  }
]

export const imageSuffix = [
  'png','jpg','jpeg','gif','pcx','svg','avif','wmf','tif'
]


export const docSuffix = [
  'txt','doc','docx','html','vue','exe','xlsx','xls','pdf','ios','js','css','java','conf','psd','sql','php','dat',"zip","rar","psd"
]

export const videoSuffix = [
  'ogg','mp4','webm'
]


export const systemParam = [
  {
    name: 'Integer',
    value: 'Integer'
  },
  {
    name: 'Short',
    value: 'Short'
  },
  {
    name: 'Long',
    value: 'Long'
  },
  {
    name: 'Float',
    value: 'Float'
  },
  {
    name: 'Double',
    value: 'Double'
  },
  {
    name: 'Boolean',
    value: 'Boolean'
  },
  {
    name: 'String',
    value: 'String'
  },
  {
    name: 'Date',
    value: 'Date'
  },
  {
    name: 'Map',
    value: 'Map'
  },
  {
    name: 'List<Map>',
    value: 'List'
  },
  {
    name: 'List<Long>',
    value: 'LongArray'
  },
  {
    name: 'List<String>',
    value: 'StringArray'
  },
  {
    name: 'List<Integer>',
    value: 'IntegerArray'
  }
]

export const checkSysFieldName=(fieldCode:string)=>{
  let isSysField = false;
  if(fieldCode){
    systemField.forEach((item:any)=>{
      if(item.fieldCode === fieldCode){
          if(isSysField) {return};
          isSysField = true;
      }
    })
    return isSysField;
  }
  return false
};


export const deleteSys=(obj:object)=>{
  const properties = Object.keys(obj);
  if(properties){
    properties.forEach((propertie,index)=>{
         const  isSys = checkSysFieldName(propertie)
         if(isSys){
           Reflect.deleteProperty(obj, propertie)
         }
    })
  }
  return obj;
};


export const deleteExtra=(obj:object)=>{
  const properties = Object.keys(obj);
  if(properties){
    properties.forEach((propertie,index)=>{
      const  isSys = checkSysFieldName(propertie)
      if(!isSys){
        Reflect.deleteProperty(obj, propertie)
      }
    })
  }
  return obj;
};



export const filter = (rows:[],data:any) => {
  if(data && rows.length){
    const properties = Object.keys(data);
    rows.forEach((row:any,index)=>{
      if(row.isSysfield ==='1' && properties.indexOf(row.fieldCode)!==-1){
         row.fieldDefaultVal = data[row.fieldCode];
      }
    })

    // eslint-disable-next-line no-prototype-builtins
    if(data?.hasOwnProperty("extra")){
          const extraProperties = Object.keys(data.extra);
          rows.forEach((row:any,index)=>{
            if(row.isSysfield !=='1' && extraProperties.indexOf(row.fieldCode)!==-1){
               row.fieldDefaultVal = data.extra[row.fieldCode];
            }
          })
    }
  }
}


export const filterExtra = (rows:[],data:any) => {
  if(data && rows.length){
    // eslint-disable-next-line no-prototype-builtins
    if(data?.hasOwnProperty("extra")){
      const extraProperties = Object.keys(data.extra);
      rows.forEach((row:any,index)=>{
        if(extraProperties.indexOf(row.fieldCode)!==-1){
          row.fieldDefaultVal = data.extra[row.fieldCode];
        }
      })
    }
  }
}
export default null;
