export function isLegal(fileName: string) {
  let isLegalName = true;
  const charts: string[] = ['/', '|', '\\', '@', '%', ':', '*', '&'];
  charts.forEach((value) => {
    if (isLegalName) {
      if (fileName.indexOf(value) >= 0) {
        isLegalName = false;
      }
    }
  });
  return isLegalName;
}

export function isReadFile(fileName: string) {
  const charts: string[] = [
    'jpg',
    'png',
    'psd',
    'gif',
    'svg',
    'xlsx',
    'xls',
    'mp3',
    'mp4',
    'pdf',
  ];
  fileName = fileName.substring(fileName.lastIndexOf('.') + 1);
  return charts.indexOf(fileName) === -1;
}

export function substring(title: string, count: number) {
  if (title) {
    title = title.length > count ? `${title?.substring(0, count)}..` : title;
    return title;
  }
  return title;
}

export function formatFileSize(fileSize: number) {
  if (fileSize < 1024) {
    return `${fileSize  }B`;
  } else if (fileSize < (1024*1024)) {
    const temp = fileSize / 1024;
    return  `${temp.toFixed(2)}KB`;
  } else if (fileSize < (1024*1024*1024)) {
    const temp = fileSize / (1024*1024);
    return  `${temp.toFixed(2)}MB`;
  } else {
    const temp = fileSize / (1024*1024*1024);
     return  `${temp.toFixed(2)}GB`;
  }
}


export function fileSuffix(fileName: string) {
  if (fileName) {
      return fileName.split('.').pop();
  }
  return "";
}

export function arrayBufferToBase64(buffer: any) {
  let binary = '';
  const bytes = new Uint8Array(buffer);
  const len = bytes.byteLength;
  for (let i = 0; i < len; i++) {
    binary += String.fromCharCode(bytes[i])
  }
  return window.btoa(binary)
}

