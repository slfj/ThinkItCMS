import axios from "axios";
import { GeneralChart } from "@/types/global";

export interface ChartDataRecord {
  x: string;
  y: number;
  name: string;
}
export interface DataChainGrowth {
  quota: string;
}

export interface DataChainGrowthRes {
  count: number;
  growth: number;
  chartData: {
    xAxis: string[];
    data: { name: string; value: number[] };
  };
}
export function queryDataChainGrowth(data: DataChainGrowth) {
  return axios.post<DataChainGrowthRes>('/api/data-chain-growth', data);
}


export function queryEnv() {
  return axios.get<any>('/actuator/env');
}

export function queryCpu() {
  return axios.get<any>('/actuator/metrics/system.cpu.usage');
}

export function queryCpuTotal() {
  return axios.get<any>('/actuator/metrics/system.cpu.count');
}

export function queryMemoryUsed() {
  return axios.get<any>('/actuator/metrics/jvm.memory.used');
}

export function queryThreads() {
  return axios.get<any>('/actuator/metrics/jvm.threads.live');
}


export function queryDiskTotal() {
  return axios.get<any>('/actuator/metrics/disk.total');
}

export function queryDiskFree() {
  return axios.get<any>('/actuator/metrics/disk.free');
}



export function queryThreeLive() {
  return axios.get<any>('/actuator/metrics/jvm.threads.live');
}

export function queryThreeDaemon() {
  return axios.get<any>('/actuator/metrics/jvm.threads.daemon');
}

export function queryRequests() {
  return axios.get<any>('/actuator/metrics/http.server.requests');
}
export function queryInfo() {
  return axios.get<any>('/actuator/info');
}


export interface PopularAuthorRes {
  list: {
    ranking: number;
    author: string;
    contentCount: number;
    clickCount: number;
  }[];
}

export function queryPopularAuthor() {
  return axios.get<PopularAuthorRes>('/api/popular-author/list');
}

export interface ContentPublishRecord {
  x: string[];
  y: number[];
  name: string;
}

export function queryContentPublish() {
  return axios.get<ContentPublishRecord[]>('/api/content-publish');
}

export function queryContentPeriodAnalysis() {
  return axios.post<GeneralChart>('/api/content-period-analysis');
}

export interface PublicOpinionAnalysis {
  quota: string;
}
export interface PublicOpinionAnalysisRes {
  count: number;
  growth: number;
  chartData: ChartDataRecord[];
}
export function queryPublicOpinionAnalysis(data: DataChainGrowth) {
  return axios.post<PublicOpinionAnalysisRes>(
    '/api/public-opinion-analysis',
    data
  );
}
export interface DataOverviewRes {
  xAxis: string[];
  data: Array<{ name: string; value: number[]; count: number }>;
}

export function queryDataOverview() {
  return axios.post<DataOverviewRes>('/api/data-overview');
}
