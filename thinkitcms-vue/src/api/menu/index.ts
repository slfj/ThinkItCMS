import axios, { AxiosResponse } from 'axios';
import { Response } from '@/api/aicode';

export interface Record {
  menuName: string | '';
  name: string;
  type: string | '0';
  key: string | '';
  order: number | 0;
  pid: string | '0';
  parentName: string | '';
  path: string;
  api: string | '';
  id: string | '';
  children: [];
  isUp: boolean | false;
  meta: {
    roles: [];
    requiresAuth: boolean;
    icon: string;
    locale: string;
    hideInMenu: boolean;
    hideChildrenInMenu: boolean;
    activeMenu: string;
    order: number;
    noAffix: boolean;
    ignoreCache: boolean;
  };
}
export function menuTree() {
  return axios.get<Record[]>('/api/sysMenu/menuTableTree');
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysMenu/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysMenu/update', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysMenu/detail?id=${rowId}`);
}

export function deleteIt(rowId: string) {
  return axios.delete<Response>(`/api/sysMenu/delete?id=${rowId}`);
}


export function downTs() {
  return axios.post<any>(`/api/sysMenu/downMenuTs`,{},{
      responseType:'blob'
  });
}

