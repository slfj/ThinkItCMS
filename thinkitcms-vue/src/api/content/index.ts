import axios from "axios";

export interface Record {
  id: string;
  title: string | '0';
  keywords: string;
  description:string;
  subTitle: string;
  categoryName:string;
  categoryId: number;
  formModelId: string;
  copied: boolean;
  sort: number;
  author:string;
  editor:string;
  isTop:boolean;
  isRecomd:boolean;
  isHeadline: boolean;
  hasFiles:boolean;
  hasRelated:boolean;
  hasTags:boolean;
  url:string;
  topTag:string;
  cover:string;
  comments:number;
  clicks:number;
  pathRule:string;
  likes:number;
  publishDate:string;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}


export interface Response {
  code: string;
  msg: string;
  data: any;
}


export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}


export function page(params: PageParams) {
  return axios.post<ListRes>(`/api/content/page`,params);
}

export function listRecycle(params: PageParams) {
  return axios.post<ListRes>(`/api/content/listRecycle`,params);
}

export function reduction(params: any) {
  return axios.post<any>(`/api/content/reduction`,params);
}

export function readDels(params: any) {
  return axios.post<any>(`/api/content/readDel`,params);
}


export function detail(rowId: string) {
  return axios.get<Response>(`/api/content/detail?contentId=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/content/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/content/update', params);
}


export function pathRule(){
  return axios.get<Response>(`/api/category/getKeyValueModels`);
}


export function deleteIt(rowId: string) {
  return axios.delete(`/api/site/delete?id=${rowId}`);
}

export function queryTag(){
  return axios.get<string[]>(`/api/content/tags`);
}

export function updateTag(params:any){
  return axios.put<Response>(`/api/content/updateTag`,params);
}


export function publish(params:any){
  return axios.put<Response>(`/api/content/publish`,params);
}

export function jobPublish(params:any){
  return axios.put<Response>(`/api/content/jobPublish`,params);
}

export function generate(params:any){
  return axios.put<Response>(`/api/content/generate`,params);
}


export function upRelateds(params:any){
  return axios.post<Response>(`/api/related/upRelateds`,params);
}


export function listRelateds(contentId:string){
  return axios.get<Response>(`/api/related/listRelateds?contentId=${contentId}`);
}

export function topIt(params:any){
  return axios.put<Response>(`/api/content/topIt`,params);
}


export function headIt(params:any){
  return axios.put<Response>(`/api/content/headIt`,params);
}


export function recomdIt(params:any){
  return axios.put<Response>(`/api/content/recomdIt`,params);
}





