import axios from 'axios';

export interface Record {
  id: string,
  domain: string,
  issuer: string,
  organization: string,
  effectDate: string,
  deadline:string,
  signaturer:string,
  version:string,
  certificateType:string

}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/sysLicense/page', params);
}

export function genMyCode(rowId: string) {
  return axios.put(`/api/sysLicense/genCode?id=${rowId}`);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysLicense/detail?id=${rowId}`);
}

export function update(params: any) {
  return axios.post<Response>('/api/sysLicense/update', params);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/sysLicense/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/sysLicense/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/sysLicense/update', params);
}

export function deployLicenseFile(fileUrl: string) {
  return axios.put<Response>(`/api/sysLicense/deploy?licenseUrl=${fileUrl}`);
}



export function downLicenseFile(fileUrl: string) {
 // return axios.put<Response>(`/api/sysLicense/downLicense?licenseUrl=${fileUrl}`);
  return axios.put<any>(`/api/sysLicense/downLicense?licenseUrl=${fileUrl}`, {},{
    responseType:'blob'
  });

}
