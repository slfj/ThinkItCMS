import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  formName: string;
  tmpPath: string;
  mobileTmpPath: string;
  formType: string;
}


export interface Fragment {
  id: string;
  formName: string;
  tmpPath: string;
  formType: string;
  formCode:string;
  formDesc:string;
  confRows:number;
}


export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response {
  code: string;
  msg: string;
  data: any;
}

export interface ListDesign {
  data: any;
  code: string;
  msg: string;
}

export interface ListFragment {
  rows: Fragment[];
  total: number;
  current: number;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/formModel/page', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/formModel/detail?id=${rowId}`);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/formModel/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/formModel/save', params);
}

export function addFragment(params: any) {
  return axios.post<Response>('/api/formModel/saveFragment', params);
}

export function detailFragment(rowId: string) {
  return axios.get<Fragment>(`/api/formModel/detail?id=${rowId}`);
}

export function editFragment(params: any) {
  return axios.post<Response>('/api/formModel/updateFragment', params);
}

export function genFragTemp(formModelId: string) {
  return axios.put<Response>(`/api/formModel/genFragment?formModelId=${formModelId}`);
}


export function editForm(params: any) {
  return axios.put<Response>('/api/formModel/update', params);
}

export function saveModeDesign(params: any) {
  return axios.post<Response>('/api/modelDesign/saveMdesign', params);
}

export function saveModeIndex(params: any) {
  return axios.post<Response>('/api/modelDesign/createIndex', params);
}

export function designDetail(modeId: string) {
  return axios.get<ListDesign>(`/api/modelDesign/detail?modelId=${modeId}`);
}

export function pageFragment(params: PageParams) {
  return axios.post<ListFragment>('/api/formModel/pageFragment', params);
}

export function pageFormData(params: PageParams) {
  return axios.post<ListFragment>('/api/formData/page', params);
}

export function saveExtra(params: any) {
  return axios.post<Response>('/api/formData/saveExtra', params);
}

export function delFormData(id: string) {
  return axios.delete<Response>(`/api/formData/delete?id=${id}`);
}

export function detailFormData(rowId: string,isExtra:boolean) {
  return axios.get<Response>(`/api/formData/detail?id=${rowId}&isExtra=${isExtra}`);
}

export function updateExtra(params: any) {
  return axios.post<Response>('/api/formData/updateExtra', params);
}

export function fragmentContent(formCode: string) {
  return axios.get<Response>(`/api/formModel/fragmentContent?formCode=${formCode}`);
}

export function saveFragmentContent(params:any) {
  return axios.put<Response>(`/api/formModel/saveFragmentContent`,params);
}

export function deleteFragment(id:string,formCode:string) {
  return axios.delete<Response>(`/api/formModel/deleteFragment?id=${id}&formCode=${formCode}`);
}

