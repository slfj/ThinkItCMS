import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  tableName: string;
  moduleName: string;
  author: string;
  gmtCreate: string;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response extends AxiosResponse {
  code: string;
  msg: string;
  data: any;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/sysAutoCode/page', params);
}

export function genMyCode(rowId: string) {
  return axios.put(`/api/sysAutoCode/genCode?id=${rowId}`);
}

export function genMyPageCode(rowId: string) {
  return axios.put(`/api/sysAutoCode/genMyPageCode?id=${rowId}`);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysAutoCode/detail?id=${rowId}`);
}

export function update(params: any) {
  return axios.post<Response>('/api/sysAutoCode/update', params);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/sysAutoCode/delete?id=${rowId}`);
}
