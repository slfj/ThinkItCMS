import axios from "axios";

export interface Record {
  id: string;
  name: string | '0';
  code: string;
  pid:string;
  allowCreate: string;
  pageSize: number;
  hidden: string;
  singlePage: string;
  title: string;
  description: string;
  keywords: string;
  sort: number;
  cover:string;
  pathRule:string;
  goUrl:string;
  containChild:string;
  formModelId:string;
  url:string;
  murl:string;
  extra: any;
}

export interface ChannelInfoModel {
   modelIds:[]
}

export interface SeoModel {
  title: string;
  description: string;
  keywords: string;
  categoryJob:{
    enable: boolean;
    jobStartTime: Date,
    cycle:number
  }
}

export interface ResetModel {
  clear: boolean;
}

export type FormDataModel = Record & ChannelInfoModel & SeoModel & ResetModel;

export interface FormModel {
  id: string;
  formName: string;
  tmpPath: string;
  mobileTmpPath: string;
  formModelName:string;
  formType: number;
  showTmp:boolean;
}

export interface FormModelConf {
  id: string;
  formModelId: string;
  fieldName: string;
  fieldCode: string;
  fieldType: string;
  fieldPlaceholder: string;
  fieldDefaultVal: string;
  length: number;
  isRequire: number;
  options: [];
  sort: number;
  isSysfield: string;
}




export interface Response {
  code: string;
  msg: string;
  expandedKeys:[];
  data: any;
}


export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}


export function tree(rowId: string) {
  return axios.get<Response>(`/api/category/tree?pid=${rowId}`);
}

export function treeTable(rowId: string) {
  return axios.get<Response>(`/api/category/treeTable?pid=${rowId}`);
}


export function treeTableForContent(rowId: string) {
  return axios.get<Response>(`/api/category/treeTableForContent?pid=${rowId}`);
}


export function listModel(params: FormModel) {
  return axios.get<FormModel[]>(`/api/formModel/listModel`,{params});
}



export function loadModelConf(id: string) {
  return axios.get<FormModelConf[]>(`/api/modelDesign/detail?modelId=${id}`);
}


export function submitData(params: FormDataModel) {
  return axios.post<Response>('/api/category/save', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/category/update', params);
}

export function detail(categoryId: any) {
  return axios.get<FormDataModel>(`/api/category/detail?id=${categoryId}`);
}

export function categoryModel(categoryId: any) {
  return axios.get<FormModel[]>(`/api/categoryFormModel/listModel?categoryId=${categoryId}`);
}


export function clearTaskId(rowId: string) {
  return axios.delete(`/api/category/clearTaskId?categoryId=${rowId}`);
}

export function upStatus(rowId: string) {
  return axios.put(`/api/category/upStatus?categoryId=${rowId}`);
}



export function createTmpHome(params:any){
  if(params.batch){
    return axios.put<Response>(`/api/category/createTmpHomes`,params);
  }
  return axios.put<Response>(`/api/category/createTmpHome`,params);

}


export function createIndexHome(params:any){
  return axios.put<Response>(`/api/category/createIndexHome`,params);
}



export function pathRule(){
  return axios.get<Response>(`/api/category/getKeyValueModels`);
}



export function deleteIt(rowId: string) {
  return axios.delete(`/api/site/delete?id=${rowId}`);
}


export function deletes(params:any) {
  return axios.post(`/api/category/deletes`,params);
}


