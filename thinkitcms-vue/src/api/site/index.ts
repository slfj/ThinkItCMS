import axios from "axios";
import { Tree } from "@/api/templates";

export interface Record {
  id: string;
  name: string | '0';
  domain: string;
  keyword: string | '';
  dir: string;
  mobileDir: string;
  icp: string;
  copyright: string;
  description: '';
  defsite: boolean;
  logoImg: null;
  order: 0;
}

export interface  WebFilesRecord {
  fileName: string,
  directory: boolean,
  lastModifiedTime: string,
  lastAccessTime: string,
  creationTime: string,
  size: string,
}


export interface Response {
  code: string;
  msg: string;
  data: any;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface Source {
  id: string;
  relativePath: string;
  bucket: string;
  objectName: string;
  suffix:string;
  formDesc:string;
  fileSize:number;
  fullPath:string;
  gmtCreate:string;
}

export interface ListSource {
  rows: Source[];
  total: number;
  current: number;
}




export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/site/page', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/site/detail?id=${rowId}`);
}

export function deleteIt(rowId: string) {
  return axios.delete(`/api/site/delete?id=${rowId}`);
}

export function addForm(params: any) {
  return axios.post<Response>('/api/site/createSite', params);
}

export function editForm(params: any) {
  return axios.put<Response>('/api/site/update', params);
}

export function listSite() {
  return axios.get<Record>('/api/site/listSite');
}

export function setDefault(rowId: string) {
  return axios.put<Response>(`/api/site/setDefault?siteId=${rowId}`);
}

export function createSiteFinish(rowId: string) {
  return axios.put<Response>(`/api/site/createSiteFinish?siteId=${rowId}`);
}


export function loopFolder() {
  return axios.get<Tree>(`/api/site/loadDefTemplate`);
}

export function queryProgress(taskId:string) {
  return axios.get<any>(`/api/task/taskProgress?taskId=${taskId}`);
}


export function upStatus(key:string,status:string) {
  return axios.put<any>(`/api/task/upStatus?key=${key}&status=${status}`);
}

export function clear(key:string) {
  return axios.delete<any>(`/api/task/clear?key=${key}`);
}

export function syncToEs(rowId: string) {
  return axios.put<any>(`/api/site/syncToEs?siteId=${rowId}`);
}

export function queryWebFilesList(path: string) {
  return axios.get<any>(`/api/webstatic/page?path=${path}`);
}

export function fileContent(path: string) {
  return axios.get<any>(`/api/webstatic/fileContent?path=${path}`);
}


export function setContent(params:any) {
  return axios.put<any>(`/api/webstatic/setContent`,params);
}

export function deleteFile(path:any) {
  return axios.delete<any>(`/api/webstatic/deleteFile?path=${path}`);
}

export function openDir(url:any) {
  return axios.put<any>(`/api/webstatic/openDir`,{path:url});
}

export function downDir(url: any) {
  return axios.put<any>('/api/webstatic/zipDown', {path:url},{
    responseType:'blob'
  });
}


export function deleteNetIoFile(key:any) {
  return axios.delete<any>(`/api/netio/delete?key=${key}`);
}


export function uploadNetIoFile(file:any) {
  return axios.post<any>(`/api/netio/upload`,file);
}


export function merge(data:any) {
  return axios.post<any>(`/api/netio/merge`,data);
}



export function pageSource(params: PageParams) {
  return axios.post<ListSource>('/api/attach/page', params);
}

export function deleteSource(id: string) {
  return axios.delete<any>(`/api/attach/delete?id=${id}`);
}
