import axios, { AxiosResponse } from 'axios';

export interface Record {
  id: string;
  type: string;
  module: string;
  userId: string;
  username:string;
  name:string;
  operation:string;
  time:number;
  method:string;
  ip:string;
  gmtCreate: string;
}

export interface PageParams extends Partial<Record> {
  current: number;
  pageSize: number;
  dto: object;
}

export interface ListRes {
  rows: Record[];
  total: number;
  current: number;
}

export interface Response extends AxiosResponse {
  code: string;
  msg: string;
  data: any;
}

export function queryList(params: PageParams) {
  return axios.post<ListRes>('/api/sysLog/page', params);
}

export function detail(rowId: string) {
  return axios.get<Record>(`/api/sysLog/detail?id=${rowId}`);
}

export function deleteRow(rowId: string) {
  return axios.delete(`/api/sysLog/delete?id=${rowId}`);
}
