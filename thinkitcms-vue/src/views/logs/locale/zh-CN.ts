export default {
  'form.logs': '代码生成',
  'form.logs.columns.type': '类型',
  'form.logs.columns.module':'模块',
  'form.logs.columns.name': '昵称',
  'form.logs.columns.username': '账户',
  'form.logs.columns.time': '耗时(毫秒)',
  'form.logs.columns.method': '执行方法',
  'form.logs.columns.ip': 'ip地址',
  'form.logs.columns.params':'参数',
  'form.logs.columns.operation':'操作说明'
};
