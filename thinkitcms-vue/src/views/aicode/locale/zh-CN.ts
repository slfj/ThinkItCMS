export default {
  'form.aicode': '代码生成',
  'form.aicode.columns.moduleName': '模块名称',
  'form.aicode.columns.author': '作者名称',
  'form.aicode.columns.tableName': '表名称',
  'form.aicode.columns.tableComment': '表注释',
  'form.aicode.operations.gen': '一键生成',
  'form.aicode.operations.gen.tip': '该操作将会覆盖之前的代码文件确定要生成吗?',

  'form.aicode.moduleName.required.errMsg': '请输入模块名称',
  'form.aicode.moduleName.maxLength.errMsg': '模块名称最大20个字符',
  'form.aicode.moduleName.minLength.errMsg': '模块名称最小2个字符',
  'form.aicode.author.required.errMsg': '请输入作者名称',
  'form.aicode.author.maxLength.errMsg': '作者名称最大10位字符',
  'form.aicode.author.minLength.errMsg': '作者名称最小2位字符',
  'form.aicode.operations.genPage': '生成页面',
};
