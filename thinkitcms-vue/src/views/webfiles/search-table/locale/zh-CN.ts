export default {
  'webfiles.list.searchTable': '文件列表',
  'webfiles.list.fileName': '文件名称',
  'webfiles.list.directory': '文件类型',
  'webfiles.list.lastModifiedTime': '最后修改时间',
  'webfiles.list.lastAccessTime': '最后访问时间',
  'webfiles.list.form.creationTime': '创建时间',
  'webfiles.list.form.size': '文件大小',
  'searchTable.columns.operations':'操作',
   'webfiles.list':'站点文件',
  'searchTable.columns.operations.openfile':'打开目录',
  'form.webfiles':'站点文件',
  'searchTable.columns.operations.downfile':'压缩下载',
  'searchTable.operation.upload':'上传'
};
