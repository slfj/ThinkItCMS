export default {
  'form.tips': '元数据说明',
  'form.copyRoute': '复制路由配置',
  'form.dict.columns.name': '名称',
  'form.dict.columns.type': '类型',
  'form.dict.columns.value': 'value值',
  'form.dict.columns.description': '描述',
  'form.dict.columns.num':'排序',

  'form.dict.columns.meta.ignoreCache': '菜单缓存',

  'form.dict.type.required.errMsg': '请输入类型',
  'form.dict.type.maxLength.errMsg': '类型最大50个字符',
  'form.dict.type.minLength.errMsg': '类型最小0个字符',

  'form.dict.name.required.errMsg': '请输入名称',
  'form.dict.name.maxLength.errMsg': '名称最大20位字符',
  'form.dict.name.minLength.errMsg': '名称最小2位字符',

  'form.dict.value.required.errMsg': '请输值',
  'form.dict.value.minLength.errMsg': '值最小2位字符',

  'form.dict.description.required.errMsg': '请输入描述',
  'form.dict.description.maxLength.errMsg': '描述最大300位字符',
};
