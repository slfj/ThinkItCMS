export default {
  'form.tips': '元数据说明',
  'form.copyRoute': '复制路由配置',
  'form.role.columns.roleName': '角色名称',
  'form.role.columns.roleSign': '角色标识',
  'form.role.columns.remark': '角色备注',
  'form.role.columns.isSystem': '系统角色',
  'form.role.columns.isSystem.yes': '内置角色不可操作',
  'form.role.columns.isSystem.no': '用户角色',
  'form.role.distribute': '分配菜单',

  'form.role.roleName.required.errMsg': '请输入角色名称',
  'form.role.roleName.maxLength.errMsg': '角色名称最大10个字符',
  'form.role.roleName.minLength.errMsg': '角色名称最小2个字符',

  'form.role.roleSign.required.errMsg': '请输入角色标识',
  'form.role.roleSign.maxLength.errMsg': '角色标识最大20位字符',
  'form.role.roleSign.minLength.errMsg': '角色标识最小2位字符',
};
