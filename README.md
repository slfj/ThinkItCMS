# ThinkItCMS

#### 介绍
又一个 JEE CMS,基于 Java 开发 的 cms 系统 java cms，ThinkItCMS 是一款面向模板开发，支持静态生成的CMS 系统,其支持前后端分离部署,是一款好用的 cms 系统

#### 软件架构
软件架构说明：待完善


#### 安装教程

1.  [技术文档](https://www.thinkitcms.com/p/doc/1717451393964314624.html#/)


#### 使用说明

1.  管理端 演示地址：http://m.thinkitcms.com/  账户：manager 密码：111111
2.  官网模板 https://www.thinkitcms.com/
3.  博客模板：https://blog.thinkitcms.com/p/index.html


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
