import { AppRouteRecordRaw } from '@/router/routes/types';
import { DEFAULT_LAYOUT } from '@/router/routes/base';

const ROUTES: AppRouteRecordRaw[] = [
{
path: '/dashboard',
name: 'dashboard',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.dashboard',
requiresAuth: true,
icon: 'icon-dashboard',
order: 0,
},
children: [
{
path: 'workplace',
name: 'Workplace',
component: () => import('@/views/dashboard/workplace/index.vue'),
meta: {
locale: 'menu.dashboard.workplace',
requiresAuth: true,
authSign: 'admin',
},
},
/** simple */
{
path: 'monitor',
name: 'Monitor',
component: () => import('@/views/dashboard/monitor/index.vue'),
meta: {
locale: 'menu.dashboard.monitor',
requiresAuth: true,
authSign: '*',
},
},
/** simple end */
],
},
{
path: '/exception',
name: 'exception',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.exception',
requiresAuth: true,
icon: 'icon-exclamation-circle',
order: 6,
},
children: [
{
path: '403',
name: '403',
component: () => import('@/views/exception/403/index.vue'),
meta: {
locale: 'menu.exception.403',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: '404',
name: '404',
component: () => import('@/views/exception/404/index.vue'),
meta: {
locale: 'menu.exception.404',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: '500',
name: '500',
component: () => import('@/views/exception/500/index.vue'),
meta: {
locale: 'menu.exception.500',
requiresAuth: true,
authSign: 'admin',
},
},
],
},

{
path: '/form',
name: 'form',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.form',
icon: 'icon-settings',
requiresAuth: true,
order: 3,
},
children: [
{
path: 'step',
name: 'Step',
component: () => import('@/views/form/step/index.vue'),
meta: {
locale: 'menu.form.step',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: 'group',
name: 'Group',
component: () => import('@/views/form/group/index.vue'),
meta: {
locale: 'menu.form.group',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
path: '/list',
name: 'list',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.list',
requiresAuth: true,
icon: 'icon-list',
order: 2,
},
children: [
{
path: 'search-table', // The midline path complies with SEO specifications
name: 'SearchTable',
component: () => import('@/views/list/search-table/index.vue'),
meta: {
locale: 'menu.list.searchTable',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: 'card',
name: 'Card',
component: () => import('@/views/list/card/index.vue'),
meta: {
locale: 'menu.list.cardList',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
path: '/profile',
name: 'profile',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.profile',
requiresAuth: true,
icon: 'icon-file',
order: 4,
},
children: [
{
path: 'basic',
name: 'Basic',
component: () => import('@/views/profile/basic/index.vue'),
meta: {
locale: 'menu.profile.basic',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
path: '/result',
name: 'result',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.result',
icon: 'icon-check-circle',
requiresAuth: true,
order: 5,
},
children: [
{
path: 'success',
name: 'Success',
component: () => import('@/views/result/success/index.vue'),
meta: {
locale: 'menu.result.success',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: 'error',
name: 'Error',
component: () => import('@/views/result/error/index.vue'),
meta: {
locale: 'menu.result.error',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
path: '/user',
name: 'user',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.user',
icon: 'icon-user',
requiresAuth: true,
order: 7,
},
children: [
{
path: 'info',
name: 'Info',
component: () => import('@/views/user/info/index.vue'),
meta: {
locale: 'menu.user.info',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: 'setting',
name: 'Setting',
component: () => import('@/views/user/setting/index.vue'),
meta: {
locale: 'menu.user.setting',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
path: '/visualization',
name: 'visualization',
component: DEFAULT_LAYOUT,
meta: {
locale: 'menu.visualization',
requiresAuth: true,
icon: 'icon-apps',
order: 1,
},
children: [
{
path: 'data-analysis',
name: 'DataAnalysis',
component: () =>
import('@/views/visualization/data-analysis/index.vue'),
meta: {
locale: 'menu.visualization.dataAnalysis',
requiresAuth: true,
authSign: 'admin',
},
},
{
path: 'multi-dimension-data-analysis',
name: 'MultiDimensionDataAnalysis',
component: () =>
import(
'@/views/visualization/multi-dimension-data-analysis/index.vue'
),
meta: {
locale: 'menu.visualization.multiDimensionDataAnalysis',
requiresAuth: true,
authSign: 'admin',
},
},
],
},
{
component: DEFAULT_LAYOUT,
children: [
{
name: 'menu',
path: 'menu',
component: () => import('@/views/menu/index.vue'),
meta: {
requiresAuth: true,
order: 1,
locale: 'form.menu',
authSign: 'menu',
},
},
{
name: 'role',
path: 'sysRole',
component: () => import('@/views/role/index.vue'),
meta: {
requiresAuth: true,
order: 1,
locale: 'form.role',
authSign: 'role',
},
},
{
name: 'user',
path: 'user',
component: () => import('@/views/sysuser/index.vue'),
meta: {
requiresAuth: true,
order: 1,
locale: 'form.user',
authSign: 'user',
},
},
{
path: 'ai-code', // The midline path complies with SEO specifications
name: 'aicode',
component: () => import('@/views/aicode/index.vue'),
meta: {
locale: 'form.aicode',
requiresAuth: true,
authSign: 'aicode',
},
},
{
path: 'organize', // The midline path complies with SEO specifications
name: 'organize',
component: () => import('@/views/organize/index.vue'),
meta: {
locale: 'form.organize',
requiresAuth: true,
authSign: 'organize',
},
},
],
meta: {
requiresAuth: true,
authSign: 'system',
order: 1,
locale: 'form.sys',
icon: 'icon-apps',
},
name: 'sys',
path: '/sys',
},
];

export default ROUTES;
