package com.thinkit.cms.dto.${tkModule?lower_case};
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.thinkit.cms.model.BaseDto;
/**
* @author ${author!'System'}
*/
@Getter
@Setter
@Accessors(chain = true)
public class ${tkModule}Dto extends BaseDto {
<#macro fieldType jdbcType="" propertyName="">
    <#if jdbcType?? && jdbcType == "VARCHAR">
        private String ${propertyName};
    <#elseif jdbcType?? && jdbcType == "LONGVARCHAR">
        private String ${propertyName};
    <#elseif jdbcType?? && jdbcType == "INTEGER">
        private Integer ${propertyName};
    <#elseif jdbcType?? && jdbcType == "BIGINT">
        private Long ${propertyName};
    <#elseif jdbcType?? && jdbcType == "BIT">
        private Boolean ${propertyName};
    <#elseif jdbcType?? && jdbcType == "TINYINT">
        private Integer ${propertyName};
    <#elseif jdbcType?? && jdbcType == "TIMESTAMP">
        private Date ${propertyName};
    <#elseif jdbcType?? && jdbcType == "DECIMAL">
        private BigDecimal ${propertyName};
    </#if>
    <#return>
</#macro>
 <#if  table.fields ?? && (table.fields?size > 0) >
     <#list table.fields as field>
       /**
         ${field.comment}
       */
         <@fieldType jdbcType="${field.metaInfo.jdbcType}" propertyName="${field.propertyName}"/>

     </#list>
 </#if>

}