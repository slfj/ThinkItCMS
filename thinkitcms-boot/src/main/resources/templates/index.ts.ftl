import { AppRouteRecordRaw } from '@/router/routes/types';
import { DEFAULT_LAYOUT } from '@/router/routes/base';

const ROUTES: AppRouteRecordRaw[] = [

  <#list trees as tree>
  {
    path: '${tree.path}',
    name: '${tree.name}',
    component: DEFAULT_LAYOUT,
    meta: {
      requiresAuth: ${tree.meta.requiresAuth?string("true","false")},
      authSign:'${tree.meta.roles[0]}',
      icon: '${(tree.meta.icon)}',
      hideChildrenInMenu: ${(tree.meta.hideChildrenInMenu)?string("true","false")},
      locale: '${(tree.meta.locale)!}',
      hideInMenu: ${(tree.meta.hideInMenu)?string("true","false")},
      ignoreCache: ${(tree.meta.ignoreCache)?string("true","false")},
      activeMenu: '${(tree.meta.activeMenu)!}',
      noAffix: ${(tree.meta.noAffix)?string("true","false")},
      order: ${(tree.order)!0}
    },
    children: [
      <#list tree.children as tc>
        {
          path: '${tc.path}',
          name: '${tc.name}',
          component: () => import('${tc.component}'),
          meta: {
            requiresAuth: ${(tc.meta.requiresAuth)?string("true","false")},
            authSign: '${tc.meta.roles[0]}',
            icon: '${tc.meta.icon}',
            hideChildrenInMenu: ${(tc.meta.hideChildrenInMenu)?string("true","false")},
            locale: '${(tc.meta.locale)!}',
            hideInMenu: ${(tc.meta.hideInMenu)?string("true","false")},
            ignoreCache: ${(tc.meta.ignoreCache)?string("true","false")},
            activeMenu: '${(tc.meta.activeMenu)!}',
            noAffix: ${(tc.meta.noAffix)?string("true","false")},
            order: ${(tc.order)!0}
          }
        }
        <#if tc_has_next>,</#if>
      </#list>
    ]
  }
  <#if tree_has_next>,</#if>
  </#list>
];
export default ROUTES;
