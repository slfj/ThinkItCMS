<template>
  <div class="container">
    <Breadcrumb :items="['menu.list', 'menu.list.searchTable']" />
    <a-card class="general-card" :title="$t('menu.list.searchTable')">
      <a-row>
        <a-col :flex="1">
          <a-form
            :model="formModel"
            :label-col-props="{ span: 6 }"
            :wrapper-col-props="{ span: 18 }"
            label-align="left"
          >
            <a-row :gutter="16">
              <a-col :span="8">
                <a-form-item
                  field="userName"
                  :label="$t('form.sysuser.columns.userName')"
                >
                  <a-input
                    v-model="formModel.userName"
                    :placeholder="$t('form.sysuser.columns.userName')"
                  />
                </a-form-item>
              </a-col>
              <a-col :span="8">
                <a-form-item
                  field="nickName"
                  :label="$t('form.sysuser.columns.nickName')"
                >
                  <a-input
                    v-model="formModel.nickName"
                    :placeholder="$t('form.sysuser.columns.nickName')"
                  />
                </a-form-item>
              </a-col>

              <a-col :span="8">
                <a-form-item
                  field="nickName"
                  :label="$t('form.sysuser.columns.mobile')"
                >
                  <a-input
                    v-model="formModel.mobile"
                    :placeholder="$t('form.sysuser.columns.mobile')"
                  />
                </a-form-item>
              </a-col>

              <a-col :span="8">
                <a-form-item
                  field="nickName"
                  :label="$t('form.sysuser.columns.email')"
                >
                  <a-input
                    v-model="formModel.email"
                    :placeholder="$t('form.sysuser.columns.email')"
                  />
                </a-form-item>
              </a-col>

              <a-col :span="8">
                <a-form-item
                  field="sex"
                  :label="$t('form.sysuser.columns.sex')"
                >
                  <a-select
                    v-model="formModel.sex"
                    :placeholder="$t('form.sysuser.columns.sex')"
                  >
                    <a-option value="-1">
                      <icon-select-all />
                      {{ $t('form.sysuser.columns.sex.all') }}
                    </a-option>

                    <a-option value="0">
                      <icon-man />
                      {{ $t('form.sysuser.columns.sex.nan') }}
                    </a-option>
                    <a-option value="1">
                      <icon-woman />
                      {{ $t('form.sysuser.columns.sex.nv') }}
                    </a-option>
                  </a-select>
                </a-form-item>
              </a-col>

              <a-col :span="8">
                <a-form-item
                  field="status"
                  :label="$t('form.sysuser.columns.status')"
                >
                  <a-select
                    v-model="formModel.status"
                    :placeholder="$t('form.sysuser.columns.status')"
                  >
                    <a-option value="-1">
                      <icon-select-all />
                      {{ $t('form.sysuser.columns.status.all') }}
                    </a-option>

                    <a-option value="0">
                      <icon-unlock />
                      {{ $t('form.sysuser.columns.status.normal') }}
                    </a-option>
                    <a-option value="1">
                      <icon-lock />
                      {{ $t('form.sysuser.columns.status.lock') }}
                    </a-option>
                  </a-select>
                </a-form-item>
              </a-col>
            </a-row>
          </a-form>
        </a-col>
        <a-divider style="height: 84px" direction="vertical" />
        <a-col :flex="'86px'" style="text-align: right">
          <a-space direction="vertical" :size="18">
            <a-button
              v-permission="['admin', 'system']"
              type="primary"
              @click="search"
            >
              <template #icon>
                <icon-search />
              </template>
              {{ $t('form.search') }}
            </a-button>
            <a-button @click="reset">
              <template #icon>
                <icon-refresh />
              </template>
              {{ $t('form.reset') }}
            </a-button>
          </a-space>
        </a-col>
      </a-row>
      <a-row>
        <a-col :flex="'86px'" style="margin-right: 10px; text-align: center">
          <a-button type="primary" @click="addForm()">
            <template #icon>
              <icon-plus />
            </template>
            {{ $t('form.create') }}
          </a-button>
        </a-col>

        <a-col :flex="'86px'" style="margin-right: 10px; text-align: center">
          <a-button type="primary" @click="lock(1)">
            <template #icon>
              <icon-lock />
            </template>
            {{ $t('form.sysuser.columns.status.lock') }}
          </a-button>
        </a-col>

        <a-col :flex="'86px'" style="margin-right: 10px; text-align: center">
          <a-button type="primary" @click="lock(0)">
            <template #icon>
              <icon-unlock />
            </template>
            {{ $t('form.sysuser.columns.status.unlock') }}
          </a-button>
        </a-col>
      </a-row>
      <a-divider style="margin-top: 0" />
      <a-table
        v-model:selectedKeys="selectedKeys"
        row-key="id"
        :loading="loading"
        :pagination="pagination"
        :data="renderData"
        :bordered="false"
        :row-selection="rowSelection"
        @page-change="onPageChange"
      >
        <template #columns>
          <a-table-column
            :title="$t('form.sysuser.columns.userName')"
            data-index="userName"
          />
          <a-table-column
            :title="$t('form.sysuser.columns.nickName')"
            data-index="nickName"
          />
          <a-table-column
            :title="$t('form.sysuser.columns.email')"
            data-index="email"
          />

          <a-table-column
            :title="$t('form.sysuser.columns.mobile')"
            data-index="mobile"
          />

          <a-table-column
            :title="$t('form.sysuser.columns.sex')"
            data-index="sex"
          >
            <template #cell="{ record }">
              <a-space>
                <a-tooltip
                  v-if="record.sex === 0"
                  :content="$t('form.sysuser.columns.sex.nan')"
                >
                  <a-button>
                    <template #icon> <icon-man /> </template>
                  </a-button>
                </a-tooltip>

                <a-tooltip
                  v-if="record.sex === 1"
                  :content="$t('form.sysuser.columns.sex.nv')"
                >
                  <a-button>
                    <template #icon> <icon-woman /> </template>
                  </a-button>
                </a-tooltip>
              </a-space>
            </template>
          </a-table-column>

          <a-table-column
            :title="$t('form.sysuser.columns.status')"
            data-index="status"
          >
            <template #cell="{ record }">
              <a-space>
                <a-tooltip
                  v-if="record.status === '0'"
                  :content="$t('form.sysuser.columns.status.normal')"
                >
                  <a-button>
                    <template #icon> <icon-unlock /> </template>
                  </a-button>
                </a-tooltip>

                <a-tooltip
                  v-if="record.status === '1'"
                  :content="$t('form.sysuser.columns.status.lock')"
                >
                  <a-button>
                    <template #icon> <icon-lock /> </template>
                  </a-button>
                </a-tooltip>
              </a-space>
            </template>
          </a-table-column>

          <a-table-column
            :title="$t('columns.operations')"
            data-index="operations"
          >
            <template #cell="{ record }">
              <a-button
                type="dashed"
                size="small"
                status="warning"
                @click="edit(record.id)"
              >
                {{ $t('form.edit') }}
              </a-button>
              <a-divider direction="vertical" />

              <a-button
                type="dashed"
                size="small"
                status="success"
                @click="setRole(record.id)"
              >
                {{ $t('form.sysuser.setrole') }}
              </a-button>

              <a-divider v-if="!record.isSuperAdmin" direction="vertical" />
              <a-popconfirm
                v-if="!record.isSuperAdmin"
                :content="$t('confirm.delete')"
                type="warning"
                @ok="deleteIt(record.id)"
              >
                <a-button type="dashed" size="small" status="warning">
                  {{ $t('form.delete') }}
                </a-button>
              </a-popconfirm>
            </template>
          </a-table-column>
        </template>
      </a-table>
      <keep-alive>
        <Form ref="myForm" @ok="fetchData" />
      </keep-alive>
      <TipRole ref="tipRole" />
    </a-card>
  </div>
</template>

<script lang="ts" setup>
  import { ref, reactive } from 'vue';
  import { useI18n } from 'vue-i18n';
  import { Message } from '@arco-design/web-vue';
  import useLoading from '@/hooks/loading';
  import {
    queryList,
    Record,
    PageParams,
    deleteRow,
    lockData,
  } from '@/api/sysuser';
  import { Pagination } from '@/types/global';
  import Form from './form.vue.ftl';
  import TipRole from './tipsRole.vue';

  const generateFormModel = () => {
    return {
      <#if  table.fields ?? && (table.fields?size > 0) >
      <#list table.fields as field>
       <#if field.propertyName!='modifiedId' && field.propertyName!='gmtCreate'  && field.propertyName!='gmtModified'  && field.propertyName!='createId'>
            ${field.propertyName}:'',
       </#if>
      </#list>
      </#if>
    };
  };
  const myForm = ref<any>();
  const tipRole = ref<any>();
  const { loading, setLoading } = useLoading(true);
  const { t } = useI18n();
  const renderData = ref<Record[]>([]);
  const selectedKeys = ref([]);
  const formModel = ref(generateFormModel());
  const basePagination: Pagination = {
    current: 1,
    pageSize: 10,
  };
  const pagination = reactive({
    ...basePagination,
  });
  const rowSelection = reactive({
    type: 'checkbox',
    showCheckedAll: true,
  });
  const fetchData = async (
    params: PageParams = { current: 1, pageSize: 10, dto: {} }
  ) => {
    setLoading(true);
    try {
      const { data } = await queryList(params);
      renderData.value = data.rows;
      pagination.current = data.current;
      pagination.total = data.total;
    } catch (err) {
      // you can report use errorHandler or other
    } finally {
      setLoading(false);
    }
  };

  const search = () => {
    fetchData({
      ...basePagination,
      dto: { ...formModel.value },
    } as unknown as PageParams);
  };
  const onPageChange = (current: number) => {
    fetchData({ ...basePagination, current, dto: { ...formModel.value } });
  };

  fetchData();
  const reset = () => {
    formModel.value = generateFormModel();
    fetchData();
  };

  const edit = (id: string) => {
    myForm.value.editForm(id);
  };

  const lock = (state: string) => {
    if (!selectedKeys.value.length) {
      Message.error(t('form.sysuser.operations.selectOne'));
      return;
    }
    const params = { userIds: selectedKeys.value, status: state };
    lockData(params).then((response) => {
      Message.success(t('request.ok'));
      fetchData();
    });
  };

  const deleteIt = (id: string) => {
    deleteRow(id).then((response) => {
      Message.success(t('request.ok'));
      fetchData();
    });
  };

  const addForm = async () => {
    myForm.value.addForm();
  };

  const setRole = (id: string) => {
    tipRole.value.show(id);
  };
</script>

<script lang="ts">
  export default {
    name: 'SearchTable',
  };
</script>

<style scoped lang="less">
  .container {
    padding: 0 20px 20px 10px;
  }

  :deep(.arco-table-th) {
    &:last-child {
      .arco-table-th-item-title {
        margin-left: 16px;
      }
    }
  }
</style>
