<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.thinkit.cms.boot.mapper.${tkModule?lower_case}.${tkModule}Mapper">

    <!-- 通用查询结果列 -->
    <sql id="column_list">
        <#if  table.fields ?? && (table.fields?size > 0) ><#list table.fields as field>`${field.columnName}`<#if field_has_next>,</#if></#list></#if>
        <#if  table.commonFields ?? && (table.commonFields?size > 0) ><#list table.commonFields as field>`${field.columnName}`<#if field_has_next>,</#if></#list></#if>
    </sql>

</mapper>
