package com.thinkit.cms.boot.controller.related;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.related.RelatedService;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.related.Relateds;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import static com.thinkit.cms.enums.LogModule.CONTENT;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/related")
@Slf4j
public class RelatedController extends BaseController<RelatedService> {



        @Logs(module = CONTENT,operation = "获取文章相关的推荐")
        @GetMapping(value = "/listRelateds")
        public List<ContentDto> listRelateds(@NotBlank(message = "ID不能为空!")  String contentId) {
             return service.listRelateds(contentId);
        }



        @Logs(module = CONTENT,operation = "更新相关阅读文章")
        @PostMapping(value="upRelateds")
        public void upRelateds(@Validated(value = {ValidGroup1.class}) @RequestBody Relateds relateds) {
              service.upRelateds(relateds);
        }
}