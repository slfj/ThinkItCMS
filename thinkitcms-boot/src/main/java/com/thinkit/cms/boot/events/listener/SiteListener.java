package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.boot.events.SiteEvent;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.model.ModelJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SiteListener extends BaseListener implements ApplicationListener<SiteEvent> {

    @Override
    public void onApplicationEvent(SiteEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()) {
                    case SAVE_SITE,UPDATE_SITE -> {
                        SiteDto siteDto =getObj(event);
                        ModelJson modelJson  = getToMap(siteDto);
                        RedisLuaUtils.setJson(modelJson);
                    }
                }
            }
        }catch (Exception e){
            log.error("FormDataEsListener 执行失败：{}",e.getMessage());
        }
    }
}
