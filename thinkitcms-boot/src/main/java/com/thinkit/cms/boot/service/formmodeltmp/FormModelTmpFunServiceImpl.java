package com.thinkit.cms.boot.service.formmodeltmp;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpFunService;
import com.thinkit.cms.boot.mapper.formmodeltmp.FormModelTmpMapper;
import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.model.ModelTempParma;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @author LONG
 */
@Service
public class FormModelTmpFunServiceImpl implements FormModelTmpFunService {

    @Resource
    private FormModelTmpMapper formModelTmpMapper;


    @Cached(name = "cached::", key = "targetObject.class+'.apply.'+#modelTempParma.formModelId+'.'+#modelTempParma.templateId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public ModelTemp apply(ModelTempParma modelTempParma) {
        ModelTemp modelTemp = formModelTmpMapper.getPaths(modelTempParma);
        if(Checker.BeNotNull(modelTemp)){
            String pcTemp =   modelTemp.getPcTemp();
            String mobileTemp =   modelTemp.getMobileTemp();
            if(Checker.BeNotBlank(pcTemp)){
                modelTemp.getPc()[0]=pcTemp;
            }
            if(Checker.BeNotBlank(mobileTemp)){
                modelTemp.getMobile()[0]=mobileTemp;
            }
            return modelTemp;
        }
        return null;
    }
}
