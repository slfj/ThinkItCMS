package com.thinkit.cms.boot.mapper.categoryformmodel;
import com.thinkit.cms.dto.categoryformmodel.CategoryFormModelDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.categoryformmodel.CategoryFormModelEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface CategoryFormModelMapper extends BaseMapper<CategoryFormModelEntity> {

    /**
     * 删除记录
     * @param categoryId
     * @param siteId
     */
    void deleteRow(@Param("categoryId") String categoryId, @Param("siteId") String siteId);


    /**
     * 查询栏目可以发布的内容模型
     * @param categoryId
     * @param siteId
     * @return
     */
    List<String> queryFormModel(@Param("categoryId") String categoryId, @Param("siteId") String siteId);

    List<CategoryFormModelDto> listModel(@Param("categoryId") String categoryId, @Param("siteId") String siteId);
}