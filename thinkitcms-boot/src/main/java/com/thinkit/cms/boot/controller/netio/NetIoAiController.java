package com.thinkit.cms.boot.controller.netio;
import com.thinkit.cms.api.netio.NetIoService;
import com.thinkit.cms.authen.annotation.NoDecorate;
import com.thinkit.cms.model.CmsResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 该方法为了适配 海哥的 ai 编辑器 （后期等海哥优化后在做处理）
 * @author LONG
 */
@Validated
@RestController
@RequestMapping("/api/netioai")
@Slf4j
public class NetIoAiController {

    @Autowired
    private NetIoService netIoService;


    @NoDecorate
    @RequestMapping(value="uploadAiImage",method= RequestMethod.POST)
    public Map<String,Object> uploadAiImage(@Validated @NotNull(message = "请上传文件") @RequestParam("image") MultipartFile multipartFile) throws IOException {
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> images = new HashMap<>();
        CmsResult cmsResult = netIoService.upload(multipartFile.getOriginalFilename(),multipartFile.getName(),
        multipartFile.getInputStream(),multipartFile.getSize());
        images.put("src",cmsResult.getStr("data"));
        images.put("alt",multipartFile.getOriginalFilename());
        result.put("errorCode",cmsResult.getCode());
        result.put("data",images);
        return result;
    }

    @NoDecorate
    @RequestMapping(value="uploadAiVideo",method= RequestMethod.POST)
    public Map<String,Object> uploadAiVideo(@Validated @NotNull(message = "请上传文件") @RequestParam("video") MultipartFile multipartFile) throws IOException {
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> images = new HashMap<>();
        CmsResult cmsResult = netIoService.upload(multipartFile.getOriginalFilename(),multipartFile.getName(),
                multipartFile.getInputStream(),multipartFile.getSize());
        images.put("src",cmsResult.getStr("data"));
        images.put("alt",multipartFile.getOriginalFilename());
        result.put("errorCode",cmsResult.getCode());
        result.put("data",images);
        return result;
    }

    @NoDecorate
    @RequestMapping(value="uploadAiAttachment",method= RequestMethod.POST)
    public Map<String,Object> uploadAiAttachment(@Validated @NotNull(message = "请上传文件") @RequestParam("attachment") MultipartFile multipartFile) throws IOException {
        Map<String,Object> result = new HashMap<>();
        Map<String,Object> images = new HashMap<>();
        CmsResult cmsResult = netIoService.upload(multipartFile.getOriginalFilename(),multipartFile.getName(),
                multipartFile.getInputStream(),multipartFile.getSize());
        images.put("href",cmsResult.getStr("data"));
        images.put("fileName",multipartFile.getOriginalFilename());
        result.put("errorCode",cmsResult.getCode());
        result.put("data",images);
        return result;
    }
}
