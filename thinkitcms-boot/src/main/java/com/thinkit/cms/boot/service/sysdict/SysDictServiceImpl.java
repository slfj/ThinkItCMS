package com.thinkit.cms.boot.service.sysdict;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysdict.SysDictService;
import com.thinkit.cms.boot.entity.sysdict.SysDictEntity;
import com.thinkit.cms.boot.mapper.sysdict.SysDictMapper;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.sysdict.SysDictDto;
import com.thinkit.cms.dto.sysdict.SysDictTree;
import com.thinkit.cms.dto.sysmenu.MenuTable;
import com.thinkit.cms.utils.BuildTree;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.Tree;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/

@Service
public class SysDictServiceImpl extends BaseServiceImpl<SysDictDto,SysDictEntity,SysDictMapper> implements SysDictService{

    @Override
    public List<Tree> dictTableTree() {
        List<SysDictDto> dicts = baseMapper.dictTableTree();
        if(Checker.BeNotEmpty(dicts)){
            List<Tree> trees = new ArrayList<>(16);
            Tree tree = BuildTree.build(toTree(dicts, MenuTable.class));
            trees.addAll(tree.getChildren());
            return trees;
        }
        return Lists.newArrayList();
    }


    @Transactional
    @Override
    public void save(SysDictDto sysDictDto) {
        if(checkHasRow(null,"value",sysDictDto.getValue())){
            throw new CustomException(5067);
        }
        super.insert(sysDictDto);
    }

    @Transactional
    @Override
    public void update(SysDictDto sysDictDto) {
        if(checkHasRow(sysDictDto.getId(),"value",sysDictDto.getValue())){
            throw new CustomException(5067);
        }
        checkPk(sysDictDto.getId());
        super.updateByPk(sysDictDto);
    }

    @Override
    public boolean delete(String id) {
        int count = baseMapper.checkHasRow(null,"pid",id);
        if(count>0){
            throw new CustomException(5068);
        }
        return super.deleteLogicByPk(id);
    }


    private boolean checkHasRow(String id,String field,String value){
        int count = baseMapper.checkHasRow(id,field,value);
        return count>0;
    }

    private <T>List<Tree<T>> toTree(List<SysDictDto> dicts,Class clz){
        List<Tree<T>> trees = new ArrayList<>();
        for(SysDictDto dict:dicts){
            SysDictTree sysDictTree = new SysDictTree();
            copy(dict,sysDictTree);
            sysDictTree.setKey(dict.getId());
            trees.add(sysDictTree);
        }
        return trees;
    }
}