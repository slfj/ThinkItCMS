package com.thinkit.cms.boot.funs.mehod;
import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.MethodEnum;
import com.thinkit.cms.directive.executor.BaseMethod;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class FormatUrl extends BaseMethod {
    @Override
    public MethodEnum getName() {
        return MethodEnum.FORMAT_URL;
    }

    @Override
    public String exec(List list) throws TemplateModelException {
        String url = getString(0,list, LongEnum.STRING);
        Integer index = getInteger(1,list, LongEnum.INTEGER);
        if(Checker.BeNotBlank(url)){
           return  CmsFolderUtil.formatUrl(url,index);
        }
        return null;
    }
}
