package com.thinkit.cms.boot.entity.userrole;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_user_role")
public class UserRoleEntity extends BaseModel {
       /**
         用户ID
       */
        @TableField("user_id")
        private String userId;

       /**
         角色ID
       */
        @TableField("role_id")
        private String roleId;


}