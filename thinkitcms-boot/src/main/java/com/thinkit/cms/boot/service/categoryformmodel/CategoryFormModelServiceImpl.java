package com.thinkit.cms.boot.service.categoryformmodel;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.categoryformmodel.CategoryFormModelService;
import com.thinkit.cms.boot.entity.categoryformmodel.CategoryFormModelEntity;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.categoryformmodel.CategoryFormModelDto;
import com.thinkit.cms.boot.mapper.categoryformmodel.CategoryFormModelMapper;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* @author LG
*/

@Service
public class CategoryFormModelServiceImpl extends BaseServiceImpl<CategoryFormModelDto,CategoryFormModelEntity,CategoryFormModelMapper> implements CategoryFormModelService{


    @Override
    public void categoryFormModel(List<String> modelIds, String categoryId,String siteId) {
        baseMapper.deleteRow(categoryId,siteId);
        if(Checker.BeNotEmpty(modelIds)){
            List<CategoryFormModelEntity> categoryFormModelEntities = new ArrayList<>();
            for(String modelId:modelIds){
                CategoryFormModelEntity categoryFormModelEntity = new CategoryFormModelEntity(modelId,categoryId,siteId);
                categoryFormModelEntity.setCreateId(getUserId());
                categoryFormModelEntity.setGmtCreate(new Date());
                categoryFormModelEntities.add(categoryFormModelEntity);
            }
            super.saveBatch(categoryFormModelEntities);
        }
    }

    @Override
    public List<String> queryFormModel(String categoryId, String siteId) {
        List<String> formModelIds =  baseMapper.queryFormModel(categoryId,siteId);
        return Checker.BeNotEmpty(formModelIds)?formModelIds: Lists.newArrayList();
    }

    @Override
    public List<CategoryFormModelDto> listModel(String categoryId) {
        List<CategoryFormModelDto> categoryFormModelDtos = baseMapper.listModel(categoryId,getSiteId(true));
        return Checker.BeNotEmpty(categoryFormModelDtos)?categoryFormModelDtos:Lists.newArrayList();
    }
}