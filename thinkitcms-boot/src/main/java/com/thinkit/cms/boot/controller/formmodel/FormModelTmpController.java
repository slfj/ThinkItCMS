package com.thinkit.cms.boot.controller.formmodel;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.formmodeltmp.FormModelTmpDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/formModelTmp")
@Slf4j
public class FormModelTmpController extends BaseController<FormModelTmpService> {


        @GetMapping(value="detail")
        public FormModelTmpDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @PostMapping(value = "/page")
        public PageModel<FormModelTmpDto> list(@RequestBody PageModel<FormModelTmpDto> pageDto) {
             return service.listPage(pageDto);
        }
        @Sentinel
        @PostMapping(value="save")
        public void save(@Validated @RequestBody FormModelTmpDto formModelTmpDto) {
              service.save(formModelTmpDto);
        }

        @Sentinel
        @PutMapping(value="update")
        public void update(@Validated @RequestBody FormModelTmpDto formModelTmpDto) {
              service.updateByPk(formModelTmpDto);
        }


        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }

}