package com.thinkit.cms.boot.entity.attach;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_attach")
public class AttachEntity extends BaseModel  {
       /**
         附件地址 绝对路径
       */
        @TableField("full_path")
        private String fullPath;

       /**
         附件大小
       */
        @TableField("file_size")
        private Integer fileSize;

       /**
         相对路径
       */
        @TableField("relative_path")
        private String relativePath;

       /**
         桶
       */
        @TableField("bucket")
        private String bucket;

       /**
         附件名称
       */
        @TableField("object_name")
        private String objectName;

       /**
         下载数
       */
        @TableField("downs")
        private Integer downs;

       /**
         文件后缀
       */
        @TableField("suffix")
        private String suffix;

       /**
         排序
       */
        @TableField("sort")
        private Integer sort;

       /**
         
       */
        @TableField("site_id")
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}