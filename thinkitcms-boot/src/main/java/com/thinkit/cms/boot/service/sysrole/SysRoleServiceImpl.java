package com.thinkit.cms.boot.service.sysrole;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.sysrole.SysRoleService;
import com.thinkit.cms.api.sysrolemenu.SysRoleMenuService;
import com.thinkit.cms.boot.entity.sysrole.SysRoleEntity;
import com.thinkit.cms.boot.mapper.sysrole.SysRoleMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.sysrole.SysRoleDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
* @author LG
*/

@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRoleDto,SysRoleEntity,SysRoleMapper> implements SysRoleService{

    @Autowired
    private SysRoleMenuService roleMenuService;

    @Override
    public CmsResult save(SysRoleDto sysRoleDto) {
       if(hasRoleSign(null,sysRoleDto.getRoleSign())){
           return CmsResult.result(5025);
       }
       super.insert(sysRoleDto);
       return CmsResult.result();
    }

    @Override
    public CmsResult update(SysRoleDto sysRoleDto) {
        String rowId = sysRoleDto.getId();
        if(Checker.BeBlank(rowId)){
            return CmsResult.result(5026);
        }
        if(hasRoleSign(sysRoleDto.getId(),sysRoleDto.getRoleSign())){
            return CmsResult.result(5025);
        }
        if(isSysRole(rowId)){
            return CmsResult.result(5027);
        }
        super.updateByPk(sysRoleDto);
        return CmsResult.result();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean delete(String id) {
        roleMenuService.deleteByFiled("role_id",id);
        clearCache(SysConst.authCache);
        return super.deleteByPk(id);
    }

    @Override
    public List<SysRoleDto> listRoles() {
        List<SysRoleDto> roles = baseMapper.listRoles();
        return Checker.BeNotEmpty(roles)?roles: Lists.newArrayList();
    }

    @Override
    public boolean isSuperRole(String userId) {
        List<Integer> superRoles = baseMapper.isSuperRole(userId);
        if(Checker.BeNotEmpty(superRoles)){
            return superRoles.contains(1);
        }
        return false;
    }

    private boolean hasRoleSign(String rowId,String roleSign){
       Integer count =  baseMapper.hasRoleSign(rowId,roleSign);
       return count>0;
    }

    private boolean isSysRole(String rowId){
        Integer isSys =  baseMapper.isSysRole(rowId);
        if(Checker.BeNotNull(isSys)){
            return isSys.equals(1);
        }
        return false;
    }
}