package com.thinkit.cms.boot.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
/**
 * @author LONG
 */
@Configuration
public class RedisConfig  {

	  @Bean
	  public RedisTemplate<String,Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
	      RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();
	      redisTemplate.setConnectionFactory(redisConnectionFactory);
	      RedisSerializer<String> redisSerializer = new StringRedisSerializer();
	      redisTemplate.setKeySerializer(redisSerializer);
	      redisTemplate.setHashKeySerializer(redisSerializer);
	      Jackson2JsonRedisSerializer<Object> jacksonRedisSerializer = new Jackson2JsonRedisSerializer<Object>(Object.class);
	      ObjectMapper objectMapper = new ObjectMapper();
	      objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
	      objectMapper.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
		  ObjectMapper.DefaultTyping.NON_FINAL,JsonTypeInfo.As.WRAPPER_ARRAY);
	      jacksonRedisSerializer.setObjectMapper(objectMapper);
	      redisTemplate.setValueSerializer(jacksonRedisSerializer);
	      redisTemplate.setHashValueSerializer(jacksonRedisSerializer);
	      redisTemplate.afterPropertiesSet();
	      return redisTemplate; 
	  }

	@Bean
	public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
		RedisMessageListenerContainer container = new RedisMessageListenerContainer();
		container.setConnectionFactory(connectionFactory);
		return container;
	}
}
