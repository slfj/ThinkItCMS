package com.thinkit.cms.boot.controller.syslicense;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.syslicense.SysLicenseService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.syslicense.SysLicenseDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import static com.thinkit.cms.enums.LogModule.LICENSE;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysLicense")
@Slf4j
public class SysLicenseController extends BaseController<SysLicenseService> {



        @Logs(module = LICENSE,operation = "查看证书详情")
        @GetMapping(value="detail")
        public SysLicenseDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }



        @SaCheckPermission("license:manager")
        @PostMapping(value = "/page")
        public PageModel<SysLicenseDto> list(@RequestBody PageModel<SysLicenseDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Logs(module = LICENSE,operation = "创建证书")
        @SaCheckPermission("license:add")
        @PostMapping(value="save")
        public void save(@Validated @RequestBody SysLicenseDto sysLicenseDto) {
              service.save(sysLicenseDto);
        }


        @Logs(module = LICENSE,operation = "下载证书")
        @Sentinel
        @SaCheckPermission("license:down")
        @PutMapping(value="downLicense")
        public void downLicense(String licenseUrl) {
              service.downLicense(licenseUrl);
        }


        @Logs(module = LICENSE,operation = "删除证书")
        @Sentinel
        @SaCheckPermission("license:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }


        @Logs(module = LICENSE,operation = "部署证书")
        @Sentinel
        @SaCheckPermission("license:deploy")
        @PutMapping(value="deploy")
        public void deploy(@NotBlank String licenseUrl){
                 service.deploy(licenseUrl);
        }

}