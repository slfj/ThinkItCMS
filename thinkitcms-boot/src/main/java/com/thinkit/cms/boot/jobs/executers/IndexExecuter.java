package com.thinkit.cms.boot.jobs.executers;

import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class IndexExecuter extends Executer {

    public IndexExecuter(User user, String siteId, String taskId, TaskEnum taskEnum, CountDownLatch countDownLatch){
         super(user,siteId,taskId,taskEnum,countDownLatch);
    }
    @Override
    public void run() {
        taskService.upProgress(taskId,SysConst.count,1);
        String formModelId =  formModelService.hasIndexModel(siteId,"4");
        if(Checker.BeNotBlank(formModelId)){
            try {
                SiteDto siteDto = siteService.detail(siteId);
                Kv kv = Kv.create().setIfNotNull(SysConst.siteId,siteId);
                String templateId = user.getSite(true,user.getId()).getTemplateId();
                ModelAndView modelAndView = ModelAndView.create(user,siteId,templateId).addParam(SysConst.site,siteDto).
                addActuator(ActuatorEnum.INDEX,formModelId,tmpFunService,kv).taskId(taskId).finished();
                modelAndView.execute(false);
            }catch (Exception e){
                log.error("首页生成异常：{}",e.getMessage());
            }finally {
                countDownLatch.countDown();
            }
        }
    }
}
