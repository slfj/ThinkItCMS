package com.thinkit.cms.boot.events;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

public class SiteEvent extends ApplicationEvent {

    @Getter @Setter
    public EventEnum eventEnum;

    public SiteEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
