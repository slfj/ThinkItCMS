package com.thinkit.cms.boot.service.formmodel;

import cn.hutool.core.io.FileUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.boot.entity.formmodel.FormModelEntity;
import com.thinkit.cms.boot.events.FormModelEvent;
import com.thinkit.cms.boot.mapper.formmodel.FormModelMapper;
import com.thinkit.cms.boot.service.formmodeltmp.FormModelTmpFunServiceImpl;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.enums.ModelEnum;
import com.thinkit.cms.model.KeyValueModel;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* @author LG
*/
@Slf4j
@Service
public class FormModelServiceImpl extends BaseServiceImpl<FormModelDto,FormModelEntity,FormModelMapper> implements FormModelService{


    @Autowired
    private FormModelTmpService formModelTmpService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private  SiteService siteService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(FormModelDto formModelDto) {
        checkHasCode(null,formModelDto.getFormCode(),ModelEnum.ARTICLE.getVal());
        formModelDto.setId(id());
        super.insert(formModelDto);
        formModelTmpService.saveTmp(formModelDto);
        publishEvent(new FormModelEvent(formModelDto, EventEnum.SAVE_FORM_MODEL));
    }

    @Cached(name = "cached::", key = "targetObject.class+'.detail.'+#id" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public FormModelDto detail(String id) {
        FormModelDto formModelDto = super.getByPk(id);
        formModelTmpService.getDetail(formModelDto);
        return formModelDto;
    }

    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.detail.'+#formModelDto.id")
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(FormModelDto formModelDto) {
        String key = "cached::"+ FormModelTmpFunServiceImpl.class +".apply."+formModelDto.getId()+"."+formModelDto.getTemplateId();
        redisTemplate.delete(key);
        formModelTmpService.updateTmp(formModelDto);
        super.updateByPk(formModelDto);
        publishEvent(new FormModelEvent(formModelDto, EventEnum.UPDATE_FORM_MODEL));
    }

    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.queryCode.'+#id")
    @CacheInvalidate(name = "cached::", key = "class com.thinkit.cms.boot.service.modeldesign.ModelDesignServiceImpl+'.detail.'+#id")
    @Override
    public boolean delete(String id) {
        FormModelDto formModelDto = super.getByPk(id);
        formModelTmpService.deleteByFiled("form_model_id",id);
        boolean ok = super.deleteByPk(id);
        ModelJson modelJson = new ModelJson();
        modelJson.setPk(id);
        modelJson.setPrefix(SysConst.formModePrefix);
        String key = modelJson.buildKey(formModelDto.getFormCode(),SysConst.colon);
        modelJson.setKey(key);
        publishEvent(new FormModelEvent(modelJson, EventEnum.DELETE_FORM_MODEL));
        return ok;
    }

    @Override
    public PageModel<FormModelDto> page(PageModel<FormModelDto> pageDto) {
        SiteConf siteConf = getUserCtx().getSite(false);
        if(Checker.BeNull(siteConf)){
            throw new CustomException(5040);
        }
        String templateId = siteConf.getTemplateId();
        pageDto.getDto().setTemplateId(templateId);
        IPage page = new Page(pageDto.getCurrent(),pageDto.getPageSize());
        IPage<FormModelDto> result  = baseMapper.page(page,pageDto.getDto());
        return new PageModel(result);
    }

    @Override
    public List<FormModelDto> listModel(FormModelDto formModelDto) {
        if(Checker.BeBlank(formModelDto.getFormType())){
            return Lists.newArrayList();
        }
        List<FormModelDto> formModels;
        if(formModelDto.getShowTmp()){
            String templateId = getUserCtx().getSite(true).getTemplateId();
            formModelDto.setTemplateId(templateId);
            formModels = baseMapper.listModelByTmpId(formModelDto);
        }else {
            formModels = baseMapper.listModel(formModelDto);
        }
        return Checker.BeNotEmpty(formModels)?formModels:Lists.newArrayList();
    }

    @Override
    public PageModel<FormModelDto> pageFragment(PageModel<FormModelDto> pageDto) {
        SiteConf siteConf = getUserCtx().getSite(false);
        if(Checker.BeNull(siteConf)){
            throw new CustomException(5040);
        }
        String templateId = siteConf.getTemplateId();
        pageDto.getDto().setTemplateId(templateId).setFormType(ModelEnum.FRAGMENT.getVal());
        IPage page = new Page(pageDto.getCurrent(),pageDto.getPageSize());
        IPage<FormModelDto> result  = baseMapper.pageFragment(page,pageDto.getDto());
        return new PageModel(result);
    }

    @Site(verifyTmp = true)
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveFragment(FormModelDto formModelDto) {
        String formCode = formModelDto.getFormCode();
        checkHasCode(null,formCode,ModelEnum.FRAGMENT.getVal());
        formModelDto.setFormType(ModelEnum.FRAGMENT.getVal());
        String htmlFile = fragmentPath(formCode);
        String finalFile = CmsFolderUtil.getAbsolutePath(htmlFile);
        formModelDto.setTmpPath(htmlFile).setMobileTmpPath(htmlFile);
        super.insert(formModelDto);
        formModelTmpService.saveTmp(formModelDto);
        publishEvent(new FormModelEvent(formModelDto, EventEnum.SAVE_FORM_MODEL));
        try {
            CmsFolderUtil.createNewFile(finalFile);
        }catch (IOException e){
            throw new CustomException(5048);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Site(verifyTmp = true)
    @Override
    public void updateFragment(FormModelDto formModelDto) {
        String htmlFile = fragmentPath(formModelDto.getFormCode());
        String finalFile = CmsFolderUtil.getAbsolutePath(htmlFile);
        if(!FileUtil.exist(finalFile)){
            try {
                CmsFolderUtil.createNewFile(finalFile);
            }catch (IOException e){
                throw new CustomException(5048);
            }
        }
        publishEvent(new FormModelEvent(formModelDto, EventEnum.UPDATE_FORM_MODEL));
        super.updateByPk(formModelDto);
    }

    @Override
    public String fragmentContent(String formCode) {
        String content="";
        String htmlFile = fragmentPath(formCode);
        String absolutePath = CmsFolderUtil.getAbsolutePath(htmlFile);
        File file = FileUtil.file(absolutePath);
        if(file.exists() && !file.isDirectory()){
            content= CmsFolderUtil.fileContent(file);
        }else{
            try {
                CmsFolderUtil.createNewFile(absolutePath);
            }catch (IOException e){
                throw new CustomException(5048);
            }
        }
        return content;
    }

    @Override
    public String saveFragmentContent(String formCode, String content) {
        if(Checker.BeNotBlank(content)){
            String htmlFile = fragmentPath(formCode);
            String absolutePath = CmsFolderUtil.getAbsolutePath(htmlFile);
            boolean isOk = CmsFolderUtil.saveFileContent(absolutePath,content);
            if(!isOk){
                log.error("保存内容失败!");
            }
        }
        return content;
    }


    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.queryCode.'+#id")
    @Override
    public void deleteFragment(String id,String formCode) {
        super.deleteByPk(id);
        String htmlFile = fragmentPath(formCode);
        String absolutePath = CmsFolderUtil.getAbsolutePath(htmlFile);
        FileUtil.del(absolutePath);
        publishEvent(new FormModelEvent(id, EventEnum.DELETE_FORM_MODEL));
    }

    private String fragmentPath(String formCode){
        SiteConf siteConf = getUserCtx().getSite(true);
        String path = templateService.loadTempPath(siteConf.getTemplateId());
        String htmlFile = CmsFolderUtil.appendFolder(true,path,SysConst.fragment,siteConf.getCode(),formCode)+SysConst.html;
        return htmlFile;
    }

    @Override
    public void genFragment(String formModelId) {
        FormModelService formModelService = SpringUtil.getBean(FormModelService.class);
        FormModelDto formModelDto = formModelService.detail(formModelId);
        String htmlFile = fragmentPath(formModelDto.getFormCode());
        ModelTemp modelTemp = new ModelTemp();
        modelTemp.getPc()[0]=htmlFile;
        modelTemp.getMobile()[0]=htmlFile;
        SiteConf siteConf = getUserCtx().getSite(true);
        String pcTarget = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getDir(),
        SysConst.fragmentName,siteConf.getCode(),formModelDto.getFormCode());
        String mTarget = CmsFolderUtil.getSitePathHtml(null,siteConf.getDomain(),siteConf.getMobileDir(),
        SysConst.fragmentName,siteConf.getCode(),formModelDto.getFormCode());
        Kv kv = Kv.create().setIfNotBlank(SysConst.url,pcTarget).setIfNotBlank(SysConst.murl,mTarget);
        ModelAndView modelAndView = ModelAndView.create(getUserCtx()).
        addActuator(ActuatorEnum.FRAGMENT,formModelId,modelTemp,kv).finished();
        modelAndView.execute(false);
    }

    @Override
    public String findPath(String code) {
        Map<String,Object> params =  ModelAndView.getParams();
        String siteId = null;
        if(Checker.BeNotEmpty(params) && params.containsKey(SysConst.siteId)){
             siteId = (String) params.get(SysConst.siteId);
             if(Checker.BeBlank(siteId)){
                 siteId = getSiteId(true);
             }
        }else{
            siteId = getSiteId(true);
        }
        SiteDto siteDto = siteService.detail(siteId);
        String temp= templateService.loadTempPathBySiteId(siteId);
        temp= CmsFolderUtil.appendFolder(false,temp,SysConst.fragment,siteDto.getCode(),code)+SysConst.html;
        return temp;
    }

    @Override
    public String hasIndexModel(String siteId, String formType) {
        return baseMapper.hasIndexModel(siteId,formType);
    }

    @Override
    public List<KeyValueModel> loadParmModel(String siteId) {
        List<FormModelDto> formModelDtos = baseMapper.loadParmModel(siteId);
        List<KeyValueModel> keyValueModels = new ArrayList<>();
        if(Checker.BeNotEmpty(formModelDtos)){
            for(FormModelDto formModelDto:formModelDtos){
                String name = formModelDto.getFormName();
                String code = buildCode(formModelDto.getFormCode());
                keyValueModels.add(new KeyValueModel(code,name));
            }
            return keyValueModels;
        }
        return Lists.newArrayList();
    }


    @Cached(name = "cached::", key = "targetObject.class+'.queryCode.'+#formModelId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String queryCode(String formModelId) {
         String formModelCode = baseMapper.queryFormCode(formModelId);
         return Checker.BeNotBlank(formModelCode)?formModelCode:SysConst.none;
    }

    private String buildCode(String code){
        StringBuffer stringBuffer = new StringBuffer("<#include import('");
        stringBuffer.append(code).append("')/>");
        return stringBuffer.toString();
    }

    private void checkHasCode(String id,String code,String formType){
        Integer count = baseMapper.checkHasCode(id,code,formType);
        if(count>0){
            throw new CustomException(5047);
        }
    }
}