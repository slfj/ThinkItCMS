package com.thinkit.cms.boot.service.syslicense;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.http.HttpUtil;
import com.thinkit.cms.api.netio.NetIoService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.syslicense.SysLicenseService;
import com.thinkit.cms.boot.entity.syslicense.SysLicenseEntity;
import com.thinkit.cms.boot.mapper.syslicense.SysLicenseMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.iterator.BeanPropertyBox;
import com.thinkit.cms.core.iterator.Iterator;
import com.thinkit.cms.core.iterator.PropertyIterator;
import com.thinkit.cms.core.utils.Base64Utils;
import com.thinkit.cms.core.utils.RSAUtils;
import com.thinkit.cms.dto.syslicense.SysLicenseDto;
import com.thinkit.cms.enums.LicenseEnum;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
* @author LG
*/
@Slf4j
@Service
public class SysLicenseServiceImpl extends BaseServiceImpl<SysLicenseDto,SysLicenseEntity,SysLicenseMapper> implements SysLicenseService{


    @Autowired
    NetIoService netIoService;

    @Autowired
    SiteService siteService;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(SysLicenseDto sysLicenseDto) {
        String privateKey = CmsConf.get(SysConst.privateKey);
        String publicKey = CmsConf.get(SysConst.publicKey);
        sysLicenseDto.setEffectDate(DateUtil.parse(sysLicenseDto.getTimes().get(0),"yyyy-MM-dd"));
        sysLicenseDto.setDeadline(DateUtil.parse(sysLicenseDto.getTimes().get(1),"yyyy-MM-dd"));
        String domainStr = StringUtils.join(sysLicenseDto.getDomains(),",");
        sysLicenseDto.setDomain(domainStr);
        String certificateTypeName = LicenseEnum.getEnums(sysLicenseDto.getCertificateType()).getName();
        String certificateType = sysLicenseDto.getCertificateType();
        sysLicenseDto.setCertificateType(certificateTypeName);
        TreeMap<String, Object> params = signMap(sysLicenseDto);
        String signStr = getSign(params);
        log.info("licenseSign:{}",signStr);
        try {
            byte[] data = signStr.getBytes();
            byte[] encodedData = RSAUtils.encryptByPrivateKey(data, privateKey);
            System.out.println("加密后：\r\n" + new String(encodedData));
            byte[] decodedData = RSAUtils.decryptByPublicKey(encodedData, publicKey);
            String target = new String(decodedData);
            System.out.println("解密后: \r\n" + target);
            String sign = RSAUtils.sign(data, privateKey).
            replace("\r\n","").
            replace("\n","").
            replace("\r","");
            System.out.println("签名:" + sign);
            boolean status = RSAUtils.verify(data, publicKey, sign);
            System.out.println("签名验证结果:"+status);
            if(status){
                params.put(SysConst.signaturer,sign);
                String url = createLicense(params);
                sysLicenseDto.setLicenseUrl(url);
            }
            sysLicenseDto.setCertificateType(certificateType);
            super.insert(sysLicenseDto);
        }catch (Exception e){
            log.error("签名生成失败哦{}",e.getMessage());
        }

    }

    @Override
    public int checkLicense() {
        // 0:不存在 1:不合法 2:证书未生效 3:证书已过期  4:证书未知问题(查看公钥私钥)  5：证书域名未授权 6：通过
        String licensePath = CmsFolderUtil.getAbsolutePath(SysConst.license);
        if(!FileUtil.exist(licensePath)){
            return 0;
        }
        List<String> properties=  FileUtil.readUtf8Lines(licensePath);
        TreeMap<String, Object> params = new TreeMap<>();
        String signaturer="";
        for(String propertie:properties){
            if(Checker.BeBlank(propertie)){
                return 1;
            }
            String[] parr = propertie.split("\\:");
            if(SysConst.signaturer.equals(parr[0])){
                signaturer = parr[1];
                continue;
            }
            params.put(parr[0],parr[1]);
        }
        try {
            String signStr = getSign(params);
            log.info("licenseStr:{}",signStr);
            String publicKey = CmsConf.get(SysConst.publicKey);
            String privateKey = CmsConf.get(SysConst.privateKey);
            String sign = RSAUtils.sign(signStr.getBytes(), privateKey).
            replace("\r\n","").
            replace("\n","").
            replace("\r","");
            // boolean status = RSAUtils.verify(signStr.getBytes(), publicKey, sign);
            boolean isOk = signaturer.equals(sign);
            if(!isOk){
               return 1;
            }
            String effectDate=(String) params.get(SysConst.effectDate);
            String deadline=(String) params.get(SysConst.deadline);;
            Date now = new Date();
            if(now.before(DateUtil.parse(effectDate,"yyyy-MM-dd"))){
                return 2;
            }
            if(now.after(DateUtil.parse(deadline,"yyyy-MM-dd"))){
                return 3;
            }
            int code = checkDomain(params);
            if(code!=6) {
                return code;
            }
            return 6;
        }catch (Exception e){
            log.error("校验证书未通过:{}",e.getMessage());
            return 4;
        }
    }

    @Override
    public void downLicense(String licenseUrl) {
        byte[] bytes = HttpUtil.downloadBytes(licenseUrl);
        downFile(bytes);
    }

    @Override
    public void deploy(String fileUrl) {
        if(Checker.BeNotBlank(fileUrl)){
            String licensePath = CmsFolderUtil.getAbsolutePath(SysConst.license);
            HttpUtil.downloadFile(fileUrl,licensePath);
        }
    }

    private void downFile(byte[] bytes)  {
        HttpServletResponse response = getResponse();
        String fileName = "license.dat";
        response.setCharacterEncoding("utf-8");
        response.setHeader("FileName", fileName);
        response.setHeader("Access-Control-Expose-Headers", "FileName");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" +fileName);
        response.setContentType("application/octet-stream");
        try {
            response.setContentLengthLong(bytes.length);
            IoUtil.write(response.getOutputStream(),true,bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static TreeMap<String, Object>  signMap(SysLicenseDto sysLicenseDto){
        Iterator iterator =new BeanPropertyBox(sysLicenseDto).iterator(PropertyIterator.class);
        TreeMap<String, Object> params = new TreeMap<>();
        while (iterator.hasNext()){
            params.putAll((Map)iterator.next());
        }
        return params;
    }

    private static String getSign(TreeMap<String, Object> params){
        StringBuffer stringBuffer = new StringBuffer();
        for (String key : params.keySet()) {
            stringBuffer.append(key).append("=").append(params.get(key)).append("&");
        }
        String signStr = stringBuffer.toString();
        signStr = signStr.endsWith("&")?signStr.substring(0, signStr.length() - 1):signStr;
        return signStr;
    }



    private String createLicense(TreeMap<String, Object> params) throws Exception {
        String str="\r\n";
        StringBuffer stringBuffer=new StringBuffer("");
        for (String key : params.keySet()) {
            stringBuffer.append(key).append(SysConst.colon).append(params.get(key)).append(str);
        }
        String licenseStr=stringBuffer.toString();
        log.info("licenseStr:{}",licenseStr);
        String file = CmsFolderUtil.getAbsolutePath(UUID.randomUUID()+SysConst.dat);
        Base64Utils.byteArrayToFile(licenseStr.getBytes(),file);
        CmsResult cmsResult = netIoService.upload(file,file,new File(file));
        String url = cmsResult.getAs("data");
        if(Checker.BeNotBlank(url)){
            FileUtil.del(file);
        }
        return url;
    }

    private int checkDomain(TreeMap<String, Object> params){
        if(params.containsKey(SysConst.domain) && !"*".equals(params.get(SysConst.domain))){
            List<String> domains = siteService.listDomain();
            if(Checker.BeNotEmpty(domains)){
                 String[] authDoamins = params.get(SysConst.domain).toString().split(",");
                 // 默认合法
                 for(String domain:domains){
                      boolean legal = false;
                      for(String authDomain:authDoamins){
                          if(authDomain.startsWith("*")){
                              authDomain = authDomain.replace("*","");
                              if(domain.contains(authDomain)){
                                  legal = true;
                              }
                          }else{
                              if(domain.equals(authDomain)){
                                  legal = true;
                              }
                          }
                      }
                      // 不合法
                      if(!legal){
                          return 5;
                      }
                 }
            }
        }
        return 6;
    }

    public static void main(String[] args) throws Exception {
//       List<String> properties=  FileUtil.readUtf8Lines(CmsFolderUtil.getAbsolutePath(null));
//       TreeMap<String, Object> params = new TreeMap<>();
//       String signaturer="";
//       for(String propertie:properties){
//           String[] parr = propertie.split("\\:");
//           if(SysConst.signaturer.equals(parr[0])){
//               signaturer = parr[1];
//               continue;
//           }
//           params.put(parr[0],parr[1]); params.put(parr[0],parr[1]);
//       }
//
//        String signStr = getSign(params);
//        log.info("licenseStr:{}",signStr);
//        String publicKey = CmsConf.get(SysConst.publicKey);
//        String privateKey = CmsConf.get(SysConst.privateKey);
//        String sign = RSAUtils.sign(signStr.getBytes(), privateKey).
//        replace("\r\n","").
//        replace("\n","").
//        replace("\r","");
//
//        boolean status = RSAUtils.verify(signStr.getBytes(), publicKey, sign);
//        boolean isOk = signaturer.equals(sign);
//        System.out.println(status);
//        System.out.println("isok:"+isOk);
        int oo=3;
        for(int i=0;i<10;i++){
            System.out.println("wwwwww");
            for(int j=0;j<10;j++){
                return ;
            }
        }
        System.out.println(11);
    }
}