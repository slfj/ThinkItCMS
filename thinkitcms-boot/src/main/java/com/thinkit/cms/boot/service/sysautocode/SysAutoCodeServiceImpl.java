package com.thinkit.cms.boot.service.sysautocode;

import com.thinkit.cms.api.aicode.CmsCodeGenrater;
import com.thinkit.cms.api.sysautocode.SysAutoCodeService;
import com.thinkit.cms.boot.entity.sysautocode.SysAutoCodeEntity;
import com.thinkit.cms.boot.mapper.sysautocode.SysAutoCodeMapper;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.beans.Transient;
import java.util.List;

/**
* @author LG
*/

@Service
public class SysAutoCodeServiceImpl extends BaseServiceImpl<SysAutoCodeDto,SysAutoCodeEntity,SysAutoCodeMapper> implements SysAutoCodeService{



    @Resource
    private CmsCodeGenrater cmsCodeGenrater;

    @Override
    @Transient
    public PageModel<SysAutoCodeDto> listPage(PageModel<SysAutoCodeDto> pageModel){
        initDbTable();
        return super.listPage(pageModel);
    }


    /**
     * 初始化数据库表
     */
    private void initDbTable(){
       List<SysAutoCodeDto> tables = baseMapper.queryTables();
       if(Checker.BeNotEmpty(tables)){
          for(SysAutoCodeDto table:tables){
              if(Checker.BeNotBlank(table.getId())){
                  baseMapper.forUpdate(table);
              }else{
                  super.insert(table);
              }
          }
       }
    }

    @Override
    public CmsResult genCode(String id) {
        SysAutoCodeDto sysAutoCode = super.getByPk(id);
        if(Checker.BeNull(sysAutoCode)){
            return CmsResult.result(5022);
        }
        if(Checker.BeBlank(sysAutoCode.getModuleName())){
            return CmsResult.result(5023);
        }
        cmsCodeGenrater.genrateCode(sysAutoCode);
        return CmsResult.result();
    }

    @Override
    public CmsResult genMyPageCode(String id) {
        SysAutoCodeDto sysAutoCode = super.getByPk(id);
        if(Checker.BeNull(sysAutoCode)){
            return CmsResult.result(5022);
        }
        if(Checker.BeBlank(sysAutoCode.getModuleName())){
            return CmsResult.result(5023);
        }
        cmsCodeGenrater.genMyPageCode(sysAutoCode);
        return CmsResult.result();
    }


}