package com.thinkit.cms.boot.controller.sysorganize;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.api.sysorganize.SysOrganizeService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysorganize.SysOrganizeDto;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Tree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import static com.thinkit.cms.enums.LogModule.ORG;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysOrganize")
@Slf4j
public class SysOrganizeController extends BaseController<SysOrganizeService> {



        @Logs(module = ORG,operation = "查看组织详情")
        @SaCheckPermission("organize:edit")
        @GetMapping(value="detail")
        public SysOrganizeDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.detail(id);
        }


        @SaCheckPermission("organize")
        @PostMapping(value = "/page")
        public PageModel<SysOrganizeDto> list(@RequestBody PageModel<SysOrganizeDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Logs(module = ORG,operation = "创建组织")
        @Sentinel
        @SaCheckPermission("organize:add")
        @PostMapping(value="save")
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody SysOrganizeDto sysOrganizeDto) {
              service.save(sysOrganizeDto);
        }


        @Logs(module = ORG,operation = "更新组织")
        @Sentinel
        @SaCheckPermission("organize:edit")
        @PutMapping(value="update")
        public void update(@Validated(value = {ValidGroup2.class}) @RequestBody SysOrganizeDto sysOrganizeDto) {
              service.update(sysOrganizeDto);
        }


        @Logs(module = ORG,operation = "删除组织")
        @Sentinel
        @SaCheckPermission("organize:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }


        @SaCheckPermission("organize")
        @GetMapping("orgTableTree")
        public List<Tree> orgTableTree(){
            return service.orgTableTree();
        }

}