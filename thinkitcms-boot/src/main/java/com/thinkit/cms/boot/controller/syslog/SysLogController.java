package com.thinkit.cms.boot.controller.syslog;
import com.thinkit.cms.api.syslog.SysLogService;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.syslog.SysLogDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
/**
* @author lg
*/

@Validated
@RestController
@RequestMapping("/api/sysLog")
@Slf4j
public class SysLogController extends BaseController<SysLogService> {


        @GetMapping(value="detail")
        public SysLogDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @PostMapping(value = "/page")
        public PageModel<SysLogDto> list(@RequestBody PageModel<SysLogDto> pageDto) {
             return service.listPage(pageDto);
        }

//        @PostMapping(value="save")
//        public void save(@Validated @RequestBody SysLogDto sysLogDto) {
//              service.insert(sysLogDto);
//        }
//
//        @PutMapping(value="update")
//        public void update(@Validated @RequestBody SysLogDto sysLogDto) {
//              service.updateByPk(sysLogDto);
//        }
//
//
//        @DeleteMapping(value="delete")
//        public boolean deleteByPk(@NotBlank @RequestParam String id){
//                return service.deleteByPk(id);
//        }

}