package com.thinkit.cms.boot.entity.sysautocode;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_auto_code")
public class SysAutoCodeEntity extends BaseModel {
       /**
         表名称
       */
        @TableField("table_name")
        private String tableName;


        @TableField("table_comment")
        private String tableComment;

       /**
         模块名称
       */
        @TableField("module_name")
        private String moduleName;

       /**
         输出目录
       */
        @TableField("output_dir")
        private String outputDir;

       /**
         作者名称
       */
        @TableField("author")
        private String author;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}