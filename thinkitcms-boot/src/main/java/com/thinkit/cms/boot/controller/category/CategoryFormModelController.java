package com.thinkit.cms.boot.controller.category;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.api.categoryformmodel.CategoryFormModelService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.categoryformmodel.CategoryFormModelDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import static com.thinkit.cms.enums.LogModule.CATEGORY;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/categoryFormModel")
@Slf4j
public class CategoryFormModelController extends BaseController<CategoryFormModelService> {


        @GetMapping(value="detail")
        public CategoryFormModelDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.getByPk(id);
        }

        @PostMapping(value = "/page")
        public PageModel<CategoryFormModelDto> list(@RequestBody PageModel<CategoryFormModelDto> pageDto) {
             return service.listPage(pageDto);
        }


        @GetMapping(value="listModel")
        public List<CategoryFormModelDto> listModel(@NotBlank(message = "ID不能为空!") @RequestParam String categoryId){
            return service.listModel(categoryId);
        }

        @Logs(module = CATEGORY,operation = "栏目模型保存")
        @Sentinel
        @PostMapping(value="save")
        public void save(@Validated @RequestBody CategoryFormModelDto categoryFormModelDto) {
              service.insert(categoryFormModelDto);
        }


        @Logs(module = CATEGORY,operation = "栏目模型更新")
        @Sentinel
        @PutMapping(value="update")
        public void update(@Validated @RequestBody CategoryFormModelDto categoryFormModelDto) {
              service.updateByPk(categoryFormModelDto);
        }


        @Logs(module = CATEGORY,operation = "栏目模型删除")
        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }

}