package com.thinkit.cms.boot.mapper.sysautocode;
import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysautocode.SysAutoCodeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysAutoCodeMapper extends BaseMapper<SysAutoCodeEntity> {


    /**
     * 初始化表结构
     * @return
     */
    List<SysAutoCodeDto> queryTables();

    /**
     * 删除所有表
     */
    void deleteAll();

    /**
     * 存在更新不存在插入
     * @param table
     */
    void forUpdate(@Param("t") SysAutoCodeDto table);
}