package com.thinkit.cms.boot.mapper.sysrole;
import com.thinkit.cms.dto.sysrole.SysRoleDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.sysrole.SysRoleEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRoleEntity> {

    /**
     * 角色是否存在
     * @param rowId
     * @param roleSign
     * @return
     */
    Integer hasRoleSign(@Param("rowId") String rowId,@Param("roleSign") String roleSign);

    /**
     * 是否是系统内置角色，内置角色不可删除和修改
     * @param rowId
     * @return
     */
    Integer isSysRole(@Param("rowId")  String rowId);


    /**
     * 查询所有角色
     * @return
     */
    List<SysRoleDto> listRoles();


    /**
     * 查看是否是超级角色
     * @param userId
     * @return
     */
    List<Integer> isSuperRole(String userId);
}