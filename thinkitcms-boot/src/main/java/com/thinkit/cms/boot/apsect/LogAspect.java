package com.thinkit.cms.boot.apsect;

import com.thinkit.cms.api.syslog.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author LONG
 */
@Slf4j
@Aspect
@Component
public class LogAspect {

	@Autowired
	protected SysLogService sysLogService;
	  @Around("execution(* com.thinkit.cms.*..*.*(..)) && @annotation(com.thinkit.cms.annotation.Logs)")
      public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
			Long startTime = System.currentTimeMillis();
			Object result = pjp.proceed();
			Long endTime = System.currentTimeMillis();
			try {
				sysLogService.saveLog(pjp,(endTime-startTime));
			}catch (Exception e){
				log.error("日志记录失败{}",e.getMessage());
			}
			return result;
      }
}
