package com.thinkit.cms.boot.mapper.attach;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.attach.AttachEntity;

/**
* @author LG
*/

@Mapper
public interface AttachMapper extends BaseMapper<AttachEntity> {

}