package com.thinkit.cms.boot.mapper.site;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.boot.entity.site.SiteEntity;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.user.SiteConf;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
* @author LG
*/

@Mapper
public interface SiteMapper extends BaseMapper<SiteEntity> {

    /**
     * 查看是否存在该域名的网站
     * @param id
     * @param domain
     * @return
     */
    Integer count(@Param("id") String id,@Param("domain") String domain);

    /**
     * 查询站点列表
     * @return
     */
    List<SiteDto> listSite();

    /**
     * 更新站点模板
     * @param siteId
     * @param tempId
     */
    void updateDefTemplate(@Param("siteId") String siteId,@Param("tempId") String tempId);

    /**
     *
     * @param page
     * @param siteDto
     * @return
     */
    IPage<SiteDto> page(IPage page, @Param("site") SiteDto siteDto);


    /**
     * 获取站点默认模板
     * @param siteId
     * @return
     */
    String getTemplate(@Param("siteId") String siteId);


    /**
     *获取站点的默认配置
     * @param siteId
     * @return
     */
    SiteConf getConf(@Param("siteId") String siteId);


    /**
     * 获取站点列表
     * @return
     */
    Set<String> listSiteIds();

    Integer hasCode(@Param("id") String id, @Param("code") String code);

    void updateTmpSetNull(@Param("templateId") String templateId);


    List<String>  querySiteIdByTmpId(@Param("templateId") String templateId);

    String getDoamin(@Param("prefix") String prefix, @Param("siteId") String siteId);


    /**
     * 查询域名列表
     * @return
     */
    List<String> listDomain();
}