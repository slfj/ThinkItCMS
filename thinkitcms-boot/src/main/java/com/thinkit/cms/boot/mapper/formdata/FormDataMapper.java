package com.thinkit.cms.boot.mapper.formdata;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.boot.entity.formdata.FormDataEntity;
import com.thinkit.cms.dto.formdata.FormDataDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
* @author LG
*/

@Mapper
public interface FormDataMapper extends BaseMapper<FormDataEntity> {

    /**
     * 查询扩展模型记录对应扩展数据
     * @param modual
     * @param rowId
     * @param siteId
     * @param formModelId
     * @return
     */
    String queryFormData(@Param("modual") String modual, @Param("rowId") String rowId,
                         @Param("siteId") String siteId, @Param("formModelId") String formModelId);


    /**
     * 删除数据
     * @param rowId
     * @param modual
     * @param siteId
     * @param formModelId
     */
    void deleteData(@Param("rowId") String rowId, @Param("modual")  String modual,
                    @Param("siteId") String siteId,  @Param("formModelId")String formModelId);


    /**
     * 查找模型对应的数据列表
     * @param page
     * @param dto
     * @return
     */
    IPage<FormDataDto> listPage(IPage page, @Param("dto") FormDataDto dto);
}