package com.thinkit.cms.boot.controller.sysautocode;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.api.sysautocode.SysAutoCodeService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysautocode.SysAutoCodeDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysAutoCode")
@Slf4j
public class SysAutoCodeController extends BaseController<SysAutoCodeService> {


        @SaCheckPermission("genCode")
        @RequestMapping(value = "/page", method = RequestMethod.POST)
        public PageModel<SysAutoCodeDto> list(@RequestBody PageModel<SysAutoCodeDto> pageDto) {
            return service.listPage(pageDto);
        }


        @Sentinel
        @PostMapping(value="update")
        public void update(@Validated @RequestBody SysAutoCodeDto sysAutoCodeDto) {
              service.updateByPk(sysAutoCodeDto);
        }


        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.deleteByPk(id);
        }


        @Sentinel
        @PutMapping(value = "/genCode")
        public CmsResult genCode(@Validated @NotBlank(message = "主键不能为空") String id) {
                return service.genCode(id);
        }


        @Sentinel
        @PutMapping(value = "/genMyPageCode")
        public CmsResult genMyPageCode(@Validated @NotBlank(message = "主键不能为空") String id) {
                return service.genMyPageCode(id);
        }



        @GetMapping(value = "/detail")
        public SysAutoCodeDto detail(@Validated @NotBlank(message = "主键不能为空") String id) {
                return service.getByPk(id);
        }

}