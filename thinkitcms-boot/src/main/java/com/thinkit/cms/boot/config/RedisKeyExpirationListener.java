package com.thinkit.cms.boot.config;

import com.thinkit.cms.boot.jobs.RedisPublishTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

    private List<RedisListener> redisListeners=new ArrayList<>();

    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @Autowired
    public void init(List<RedisListener> redisListeners){
        this.redisListeners = redisListeners;
    }


    @Autowired
    private RedisPublishTask redisPublishTask;

//    @Override
//    protected void doRegister(RedisMessageListenerContainer listenerContainer) {
//        listenerContainer.addMessageListener(this, new PatternTopic("__keyevent@11__:expired"));
//    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        String expiredKey = message.toString();
        log.info("过期消息key:{}.......pattern:{}", expiredKey, new String(pattern, StandardCharsets.UTF_8));
        for(RedisListener redisListener:redisListeners){
            redisListener.filter(expiredKey);
        }
    }
}
