package com.thinkit.cms.boot.service.category;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.categoryformmodel.CategoryFormModelService;
import com.thinkit.cms.api.formdata.FormDataService;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpFunService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.boot.entity.category.CategoryEntity;
import com.thinkit.cms.boot.entity.content.ContentEntity;
import com.thinkit.cms.boot.events.CategoryEvent;
import com.thinkit.cms.boot.mapper.category.CategoryMapper;
import com.thinkit.cms.boot.mapper.content.ContentMapper;
import com.thinkit.cms.boot.mapper.formmodel.FormModelMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.task.TaskDto;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.directive.wrapper.CategoryMData;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.dto.category.AddEditCategory;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.dto.category.CategoryJob;
import com.thinkit.cms.dto.category.PublishCategory;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.dto.sysmenu.CategoryTree;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.enums.ModelEnum;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.enums.TaskStatusEnum;
import com.thinkit.cms.model.PageTemp;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.BuildTree;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import com.thinkit.cms.utils.Tree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
* @author LG
*/
@Slf4j
@Service
public class CategoryServiceImpl extends BaseServiceImpl<CategoryDto,CategoryEntity,CategoryMapper> implements CategoryService{


    @Autowired
    private CategoryFormModelService categoryFormModelService;

    @Autowired
    private FormDataService formDataService;


    @Autowired
    private FormModelTmpFunService tmpFunService;

    @Resource
    private ContentMapper contentMapper;

    @Resource
    private FormModelMapper formModelMapper;

    @Autowired
    private FormModelService formModelService;

    @Autowired
    private SiteService siteService;

    @Autowired
    private TaskService taskService;


    @Override
    public Tree<CategoryTree> categoryTree(String pid) {
        List<String> keys = new ArrayList<>();
        CategoryTree tree = new CategoryTree();
        List<CategoryDto> categories = baseMapper.selectCategorys(getSiteId(true), pid,null);
        tree.setName("全部栏目");
        tree.setId(BuildTree.rootNode);
        tree.setChildren(categoryToTree(categories, CategoryTree.class,false));
        if(Checker.BeNotEmpty(categories)){
            categories.forEach(category->keys.add(category.getId()));
            tree.setExpandedKeys(keys);
        }
        return tree;
    }

    @Override
    public List<Tree<CategoryTree>> treeTable(String pid) {
        Tree tree = new CategoryTree();
        SiteConf siteConf = getUserCtx().getSite(true);
        String domain = CmsFolderUtil.getSiteDomain(siteConf.getDomain());
        List<CategoryDto> categories = baseMapper.selectCategorys(getSiteId(true), null,domain);
        tree = BuildTree.build(categoryToTree(categories, CategoryTree.class,false));
        List<Tree<CategoryTree>> trees = BuildTree.treeList(tree);
        if(!BuildTree.rootNode.equals(pid)){
            trees.removeIf(t->!t.getId().equals(pid));
        }
        return trees;
    }

    @Override
    public List<Tree<CategoryTree>> treeTableForContent(String pid) {
        Tree tree = new CategoryTree();
        SiteConf siteConf = getUserCtx().getSite(true);
        String domain = CmsFolderUtil.getSiteDomain(siteConf.getDomain());
        List<CategoryDto> categories = baseMapper.selectCategorys(getSiteId(true), null,domain);
        tree = BuildTree.build(categoryToTree(categories, CategoryTree.class,true));
        List<Tree<CategoryTree>> trees = BuildTree.treeList(tree);
        if(!BuildTree.rootNode.equals(pid)){
            trees.removeIf(t->!t.getId().equals(pid));
        }
        return trees;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean deletes(List<String> categoryIds) {
        super.deleteLogicByPks(categoryIds);
        initIndexTemp(getSiteId(true));
        return true;
    }

    @Override
    public void executer(User user, String siteId, String taskId, TaskEnum taskEnum) {
          List<CategoryDto> categoryDtos =  baseMapper.queryCategorys(siteId);
          if(Checker.BeNotEmpty(categoryDtos)){
              CategoryService categoryService = SpringUtil.getBean(CategoryService.class);
              List<Date> startEndTime =new ArrayList<>();
              DateTime endTime = new DateTime();
              Date startTime = DateUtil.offset(endTime, DateField.MONTH,-12);
              startEndTime.add(startTime);
              startEndTime.add(endTime);
              for(CategoryDto categoryDto:categoryDtos){
                  if(taskService.isStop(taskId)){
                      break;
                  }
                  PublishCategory publishCategory = new PublishCategory();
                  publishCategory.setCategoryId(categoryDto.getId());
                  publishCategory.setContainChild(false);
                  publishCategory.setContainTimes(true);
                  publishCategory.setStartEndTime(startEndTime);
                  publishCategory.setForceGen(false);
                  publishCategory.setAsync(true);
                  publishCategory.setTaskId(taskId);
                  publishCategory.setUser(user);
                  categoryService.jobCreateTmpHome(publishCategory);
              }
          }
    }

    @Override
    public void clearTaskId(String categoryId) {
        taskService.clear(taskKey(categoryId));
    }

    private String taskKey(String categoryId){
        StringBuffer stringBuffer = new StringBuffer(SysConst.task);
        stringBuffer.append(getSiteId(true));
        stringBuffer.append(SysConst.colon);
        stringBuffer.append(TaskEnum.CATEGORY_TASK.getVal());
        stringBuffer.append(SysConst.colon);
        stringBuffer.append(categoryId);
        String key = stringBuffer.toString();
        return key;
    }

    @Override
    public void upStatus(String categoryId,TaskStatusEnum taskStatusEnum) {
        String key = taskKey(categoryId);
        taskService.upStatus(key, taskStatusEnum);
    }

    @Override
    public Set<String> listIds(String siteId) {
        return baseMapper.listIds(siteId);
    }


    @Cached(name = "cached::", key = "targetObject.class+'.queryCode.'+#categoryId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String queryCode(String categoryId) {
        return baseMapper.queryCode(categoryId);
    }

    @Site
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(AddEditCategory addEditCategory) {
           checkParam(addEditCategory);
           String categoryId = id();
           String formModelId = addEditCategory.getFormModelId();
           List<String> modeIds = addEditCategory.getModelIds();
           String siteId = addEditCategory.getSiteId();
           CategoryDto categoryDto = new CategoryDto();
           copy(addEditCategory,categoryDto);
           categoryDto.setId(categoryId).setPk(categoryId);
           if(Checker.BeBlank(categoryDto.getPid())){
               categoryDto.setPid(BuildTree.parentNode);
           }
           categoryFormModelService.categoryFormModel(modeIds,categoryId,siteId);
           String formModelCode = formModelService.queryCode(formModelId);
           categoryDto.setFormModelCode(formModelCode);
           super.insert(categoryDto);
           formDataService.formData(ModelEnum.CATEGORY,categoryId,siteId,formModelId, addEditCategory.getExtra());
           addEditCategory.getCategoryJob().setCategoryId(categoryId).
           setCategoryName(addEditCategory.getName());
           saveJobInfo(siteId,addEditCategory.getCategoryJob());
           publishCategory(categoryId,-12);
           initIndexTemp(getSiteId(true));
           publishEvent(new CategoryEvent(categoryDto, EventEnum.SAVE_CATEGORY));
    }


    private void publishCategory(String categoryId,Integer befMonth){
        if(Checker.BeNotBlank(categoryId)){
            List<Date> startEndTime =new ArrayList<>();
            DateTime endTime = new DateTime();
            Date startTime = DateUtil.offset(endTime, DateField.MONTH,befMonth);
            startEndTime.add(startTime);
            startEndTime.add(endTime);
            PublishCategory publishCategory = new PublishCategory();
            publishCategory.setCategoryId(categoryId);
            publishCategory.setContainChild(false);
            publishCategory.setContainTimes(true);
            publishCategory.setStartEndTime(startEndTime);
            this.createTmpHome(publishCategory);
        }
    }

    @Site
    @CacheInvalidate(name = "cached::", key="targetObject.class+'.'+#addEditCategory.id")
    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.categoryData.'+#addEditCategory.id")
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(AddEditCategory addEditCategory) {
        checkParam(addEditCategory);
        addEditCategory.setPid(null);
        String categoryId = addEditCategory.getId();
        String formModelId = addEditCategory.getFormModelId();
        List<String> modeIds = addEditCategory.getModelIds();
        String siteId = addEditCategory.getSiteId();
        CategoryDto categoryDto = new CategoryDto();
        copy(addEditCategory,categoryDto);
        categoryDto.setPk(categoryDto.getId());
        categoryFormModelService.categoryFormModel(modeIds,categoryId,siteId);
        String formModelCode = formModelService.queryCode(formModelId);
        categoryDto.setFormModelCode(formModelCode);
        super.updateByPk(categoryDto);
        formDataService.formData(ModelEnum.CATEGORY,categoryId,siteId,formModelId, addEditCategory.getExtra());
        addEditCategory.getCategoryJob().setCategoryId(categoryId).
        setCategoryName(addEditCategory.getName());
        saveJobInfo(siteId,addEditCategory.getCategoryJob());
        publishCategory(categoryId,-12);
        initIndexTemp(getSiteId(true));
        publishEvent(new CategoryEvent(categoryDto, EventEnum.UPDATE_CATEGORY));
    }


    private void saveJobInfo(String siteId, CategoryJob categoryJob){
        boolean isSave = Checker.BeNotNull(categoryJob)  && Checker.BeNotBlank(siteId);
        if(isSave){
            String format = "yyyy-MM-dd HH:mm:ss";
            Date date;
            Date jobStartTime;
            if(categoryJob.getJobStartTime().contains("T")){
                date = DateUtil.parseUTC(categoryJob.getJobStartTime());
                jobStartTime = DateUtil.offset(date, DateField.HOUR_OF_DAY,8);
            }else {
                date = DateUtil.parse(categoryJob.getJobStartTime(),format);
                jobStartTime = date;
            }
            categoryJob.setJobStartTime(DateUtil.format(jobStartTime,format));
            User user= getUserCtx();
            categoryJob.setUser(user);
            categoryJob.setSiteId(siteId);
            categoryJob.setTemplateId(user.getSite(true).getTemplateId());
            String categoryId = categoryJob.getCategoryId();
            String key = SysConst.jobCategory+siteId+SysConst.colon+categoryId;
            redisTemplate.delete(key);
            redisTemplate.opsForList().leftPush(key,categoryJob);
        }
    }

    @Cached(name = "cached::", key = "targetObject.class+'.'+#id" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public CategoryDto detail(String id,String siteId) {
        CategoryDto categoryDto = super.getByPk(id);
        String categoryId = categoryDto.getId();
        List<String> formModelIds =  categoryFormModelService.queryFormModel(categoryId,siteId);
        categoryDto.setModelIds(formModelIds);
        if(Checker.BeNotBlank(categoryDto.getFormModelId())){
            Map<String,Object> extra = formDataService.queryFormData(ModelEnum.CATEGORY,categoryId,siteId,categoryDto.getFormModelId());
            categoryDto.setExtra(extra);
        }
        String key = SysConst.jobCategory+siteId+SysConst.colon+categoryId;
        if(redisTemplate.hasKey(key)){
            CategoryJob categoryJob = (CategoryJob) redisTemplate.opsForList().index(key,0);
            categoryJob.setUser(null);
            categoryDto.setCategoryJob(categoryJob);
        }else{
            CategoryJob categoryJob = new CategoryJob();
            categoryJob.setJobStartTime(DateUtil.format(new Date(),"yyyy-MM-dd HH:mm:ss"));
            categoryJob.setCycle(3);
            categoryDto.setCategoryJob(categoryJob);
        }
        return categoryDto;
    }

    @Cached(name = "cached::", key = "targetObject.class+'.categoryData.'+#id" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public CategoryDto categoryData(String id) {
         CategoryDto categoryDto = baseMapper.categoryData(id);
         if(Checker.BeNotBlank(categoryDto.getExtraStr())){
            categoryDto.setExtra(JSONUtil.toBean(categoryDto.getExtraStr(),Map.class));
         }
         return categoryDto;
    }

    @Override
    public String createTmpHome(PublishCategory publishCategory) {
        Set<String> categoryIds = checkParams(publishCategory);
        User user = publishCategory.getUser();
        user = Checker.BeNotNull(user)?user:getUserCtx();
        String siteId = user.getSite(true,user.getId()).getSiteId();
        String key = taskService.genTaskKey(TaskEnum.CATEGORY_TASK,siteId,publishCategory.getCategoryId());
        if(taskService.isRuning(key)){
            throw new CustomException(5060);
        }
        TaskDto taskDto = new TaskDto();
        taskDto.setAction(TaskEnum.CATEGORY_TASK_BATCH.getVal());
        taskDto.setSiteId(siteId);
        String taskId =  taskService.startTask(key,taskDto);
        publishCategory.setTaskId(taskId);
        publishCategory.setUser(user);
        makeFileCategory(publishCategory,categoryIds);
        return taskId;
    }

    @Override
    public String createTmpHomes(PublishCategory publishCategory) {
        List<String> categoryIds = publishCategory.getCategoryIds();
        String key = taskService.genTaskKey(TaskEnum.CATEGORY_TASK_BATCH,getSiteId(true),  SecureUtil.md5(categoryIds.toString()));
        TaskDto taskDto = new TaskDto();
        taskDto.setAction(TaskEnum.CATEGORY_TASK_BATCH.getVal());
        taskDto.setSiteId(getSiteId(true));
        String taskId =  taskService.startTask(key,taskDto);
        for(String categoryId:categoryIds){
            PublishCategory pCategory = new PublishCategory();
            copy(publishCategory,pCategory);
            pCategory.setCategoryId(categoryId);
            pCategory.setTaskId(taskId);
            pCategory.setCount(categoryIds.size());
            pCategory.setUser(getUserCtx());
            Set<String> childIds = checkParams(pCategory);
            makeFileCategory(pCategory,childIds);
        }
        return taskId;
    }


    @Override
    public void jobCreateTmpHome(PublishCategory publishCategory) {
        Set<String> categoryIds = checkParams(publishCategory);
        makeFileCategory(publishCategory,categoryIds);
    }


    private void makeFileCategory(PublishCategory publishCategory,Set<String> categoryIds){
        CategoryService categoryService = SpringUtil.getBean(CategoryService.class);
        Integer size = categoryIds.size();
        Boolean forceGen = publishCategory.getForceGen();
        String taskId = publishCategory.getTaskId();
        for(String  categoryId:categoryIds){
            PublishCategory pCategory = new PublishCategory();
            copy(publishCategory,pCategory);
            pCategory.setCategoryId(categoryId);
            pCategory.setCount(size);
            CategoryDto categoryDto = categoryService.categoryData(categoryId);
            if(Checker.BeNotBlank(categoryDto.getFormModelId())){
                // 需要分页生成
                categoryDto.setForceGen(forceGen);
                Integer pageSize = categoryDto.getPageSize();
                if(Checker.BeNotNull(pageSize) && !pageSize.equals(0)){
                    pCategory.setAsync(true);
                    CompletableFuture.runAsync(()->{
                        pageParams(pCategory);
                    });
                }else{
                    taskService.upProgress(taskId,SysConst.count,1);
                    Kv url = Kv.create().setIfNotBlank(SysConst.url,categoryDto.getUrl()).setIfNotBlank(SysConst.murl,categoryDto.getMUrl());
                    doExec(categoryDto,null,url,publishCategory);
                }
            }else{
                upStatus(categoryId,TaskStatusEnum.FINSHED);
            }
        }
    }

    private Set<String> checkParams(PublishCategory publishCategory){
        if(publishCategory.getContainTimes() && Checker.BeEmpty(publishCategory.getStartEndTime())){
            throw new CustomException(5049);
        }
        String cid = publishCategory.getCategoryId();
        Set<String> categoryIds = new HashSet<>();
        Boolean containChild = publishCategory.getContainChild();
        if(containChild){
            List<String> categoryIdList = childs(cid);
            if(Checker.BeNotEmpty(categoryIdList)){
                categoryIds.addAll(categoryIdList);
            }
        }else{
            categoryIds.add(cid);
        }
        return categoryIds;
    }


    private void doExec(CategoryDto categoryDto,Kv initialize,Kv urlParam,PublishCategory publishCategory){
        Boolean async = publishCategory.getAsync();
        User user = publishCategory.getUser();
        if(Checker.BeNull(user)){
            user = getUserCtx();
        }
        String categoryId = categoryDto.getId();
        String formModelId = categoryDto.getFormModelId();
        String templateId = categoryDto.getTemplateId();
        String taskId = publishCategory.getTaskId();
        templateId = Checker.BeNotBlank(templateId)?templateId:user.getSite(true,user.getId()).getTemplateId();
        // 存在 分类首页模型才会执行
        if(Checker.BeNotBlank(formModelId)){
            ModelAndView modelAndView = ModelAndView.create(user,categoryDto.getSiteId(),templateId).
            addParam(SysConst.categoryId,categoryId).addParam(SysConst.category,categoryDto).
            category(new CategoryMData(categoryId,categoryDto.getCode(),categoryDto.getPathRule(),categoryDto.getForceGen())).
            addActuator(ActuatorEnum.CATEGORY,formModelId,tmpFunService,urlParam).taskId(taskId).finished();
            if(Checker.BeNotEmpty(initialize)){
                modelAndView.addParam(initialize);
            }
            log.info("栏目模板变量为：{}",JSONUtil.toJsonStr(modelAndView.getParams()));
            modelAndView.execute(async);
        }
    }

    private void pageParams(PublishCategory publishCategory){
           String categoryId = publishCategory.getCategoryId();
           List<Date> dates = publishCategory.getStartEndTime();
           if(Checker.BeEmpty(dates)){
               dates = new ArrayList<>(16);
               DateTime endTime = new DateTime();
               Date startTime = DateUtil.offset(endTime, DateField.MONTH,-12);
               dates.add(startTime);
               dates.add(endTime);
           }
           genBatch(categoryId,dates,publishCategory);
    }

        private void genBatch(String categoryId, List<Date> dates,PublishCategory publishCategory){
            Boolean forceGen = publishCategory.getForceGen();
            String taskId = publishCategory.getTaskId();
            CategoryService categoryService = SpringUtil.getBean(CategoryService.class);
            CategoryDto categoryDto = categoryService.categoryData(categoryId);
            Integer pageSize = categoryDto.getPageSize();
            if(Checker.BeNotNull(pageSize) && !pageSize.equals(0)){
                Long totalCount = count(categoryId,dates,categoryDto.getSiteId());
                log.info("栏目获取内容条数为:{}",totalCount);
                Long pageCount = (totalCount+ pageSize-1) / pageSize;
                pageCount = pageCount==0?1:pageCount;
                log.info("栏目分页条数为:{}",pageCount);
                taskService.upProgress(taskId,SysConst.count,pageCount.intValue());
                for(int i=1;i<=pageCount;i++){
                    PageTemp pageModel = new PageTemp(totalCount,pageCount,i,pageSize);
                    pageModel.setCurrent(i);
                    String url = categoryDto.getUrl(),
                    mUrl =categoryDto.getMUrl();
                    pageModel.setBaseUrl(url).setBaseMUrl(mUrl);
                    Kv urlParam = Kv.create();
                    url = CmsFolderUtil.formatUrl(url,i);
                    mUrl = CmsFolderUtil.formatUrl(mUrl,i);
                    urlParam.setIfNotBlank(SysConst.url,url).
                    setIfNotBlank(SysConst.murl,mUrl);
                    Kv pageParams = Kv.create().setIfNotNull(SysConst.page,pageModel).
                    setIfNotNull(SysConst.startEndTime,dates);
                    if(i==1){
                        categoryDto.setForceGen(forceGen);
                    }else{
                        categoryDto.setForceGen(false);
                    }
                    if(taskService.isStop(taskId)){
                        break;
                    }
                    doExec(categoryDto,pageParams,urlParam,publishCategory);
                    log.info("栏目循环生成:{}",i);
                }
            }else{
                taskService.upProgress(taskId,SysConst.count,1);
                categoryDto.setForceGen(forceGen);
                Kv urlParam = Kv.create().setIfNotBlank(SysConst.url,categoryDto.getUrl()).
                setIfNotBlank(SysConst.murl,categoryDto.getMUrl());
                Kv initialize = Kv.create().setIfNotNull(SysConst.startEndTime,dates);
                doExec(categoryDto,initialize,urlParam,publishCategory);
            }
    }

    private Long count(String categoryId,List<Date> dates,String siteId){
        QueryWrapper<ContentEntity> queryWrapper = new QueryWrapper();
        queryWrapper.eq("site_id",Checker.BeBlank(siteId)?getSiteId(true):siteId);
        queryWrapper.eq("category_id",categoryId);
        queryWrapper.eq("status","1");
        if(Checker.BeNotEmpty(dates)){
            // queryWrapper.between("publish_date",dates.get(0),dates.get(1));
            queryWrapper.gt("publish_date",dates.get(0));
        }
        return contentMapper.selectCount(queryWrapper);
    }



    @Override
    public void genStaticFile(CategoryJob categoryJob) {
        String categoryId = categoryJob.getCategoryId();
        CategoryService categoryService = SpringUtil.getBean(CategoryService.class);
        CategoryDto categoryDto = categoryService.categoryData(categoryId);
        categoryDto.setTemplateId(categoryJob.getTemplateId());
        Integer pageSize = categoryDto.getPageSize();
        List<Date> startEndTime=new ArrayList<>();
        DateTime endTime = new DateTime();
        Date startTime = DateUtil.offset(endTime, DateField.MONTH,-12);
        startEndTime.add(startTime);
        startEndTime.add(endTime);
        if(Checker.BeNotNull(pageSize) && !pageSize.equals(0)){
            Long totalCount = count(categoryId,null,categoryDto.getSiteId());
            Long pageCount = (totalCount+ pageSize-1) / pageSize;
            pageCount = pageCount==0?1:pageCount;
            for(int i=1;i<=pageCount;i++){
                PageTemp pageModel = new PageTemp(totalCount,pageCount,i,pageSize);
                pageModel.setCurrent(i);
                String url = categoryDto.getUrl(),
                mUrl =categoryDto.getMUrl();
                pageModel.setBaseUrl(url).setBaseMUrl(mUrl);
                url = CmsFolderUtil.formatUrl(url,i);
                mUrl = CmsFolderUtil.formatUrl(mUrl,i);
                Kv urlParam = Kv.create();
                urlParam.setIfNotBlank(SysConst.url,url).
                setIfNotBlank(SysConst.murl,mUrl);
                pageModel.setUrl(url).setMurl(mUrl);
                Kv pageParams = Kv.create().setIfNotNull(SysConst.page,pageModel).
                setIfNotNull(SysConst.startEndTime,startEndTime);
               // doExec(categoryDto,pageParams,urlParam,true,user);
            }
        }else{
            Kv urlParam = Kv.create().setIfNotBlank(SysConst.url,categoryDto.getUrl()).setIfNotBlank(SysConst.murl,categoryDto.getMUrl());
            Kv initialize = Kv.create().setIfNotNull(SysConst.startEndTime,startEndTime);
           // doExec(categoryDto,initialize,urlParam,true,user);
        }
    }

    @Override
    public List<String> childs(String id) {
        List<String> ids = new ArrayList<>();
        ids.add(id);
        loopFind(id,ids);
        return ids;
    }

    private void loopFind(String id,List<String> ids){
        List<String> pids = baseMapper.getId(id);
        if(Checker.BeNotEmpty(pids)){
            if(!ids.contains(pids)){
                ids.addAll(pids);
            }
            for(String pid:pids){
                loopFind(pid,ids);
            }
        }
    }

    private void checkParam(AddEditCategory addEditCategory){
        Integer count =  baseMapper.hasExist(getSiteId(true), addEditCategory.getCode(), addEditCategory.getId());
        if(count>0){
            throw new CustomException(5045);
        }
        if(addEditCategory.getAllowCreate().equals(1) &&
            Checker.BeEmpty(addEditCategory.getModelIds())){
            throw new CustomException(5058);
        }
    }

    private <T>List<Tree<T>> categoryToTree(List<CategoryDto> categories , Class clz,Boolean setDisable){
        List<Tree<T>> trees = new ArrayList<>();
        categories = Checker.BeNotEmpty(categories)?categories: Lists.newArrayList();
        // Collections.reverse(categories);
        for(CategoryDto category:categories){
            try {
                CategoryTree tree = (CategoryTree) clz.getConstructor().newInstance();
                copy(category,tree);
                if(setDisable){
                    tree.setDisabled(category.getAllowCreate().equals(2));
                }
                trees.add(tree);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return trees;
    }


    @Override
    public void initIndexTemp(String siteId) {
        if(Checker.BeBlank(siteId)){
            siteId = getSiteId(true);
        }
        String formModelId =  formModelMapper.hasIndexModel(siteId,"4");
        if(Checker.BeBlank(formModelId)){
            throw new CustomException(5055);
        }
        SiteDto siteDto = siteService.detail(siteId);
        Kv kv = Kv.create().setIfNotNull(SysConst.siteId,siteId);
        ModelAndView modelAndView = ModelAndView.create(getUserCtx()).addParam(SysConst.site,siteDto).
        addActuator(ActuatorEnum.INDEX,formModelId,tmpFunService,kv).finished();
        modelAndView.execute(true);
    }


}