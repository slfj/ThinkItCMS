package com.thinkit.cms.boot.entity.categoryformmodel;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_category_form_model")
public class CategoryFormModelEntity extends BaseModel  {


    public CategoryFormModelEntity(){

    }

    public CategoryFormModelEntity(String formModelId, String categoryId, String siteId) {
        this.formModelId = formModelId;
        this.categoryId = categoryId;
        this.siteId = siteId;
    }

    /**
         分类ID
       */
        @TableField("category_id")
        private String categoryId;

       /**
         表单模型ID
       */
        @TableField("form_model_id")
        private String formModelId;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}