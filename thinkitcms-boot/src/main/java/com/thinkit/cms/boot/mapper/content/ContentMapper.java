package com.thinkit.cms.boot.mapper.content;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.boot.entity.content.ContentEntity;
import com.thinkit.cms.dto.content.CkRow;
import com.thinkit.cms.dto.content.ContentDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Collection;
import java.util.List;

/**
* @author LG
*/

@Mapper
public interface ContentMapper extends BaseMapper<ContentEntity> {


    /**
     * 分页查询
     * @param page
     * @param dto
     * @param domain
     * @return
     */
    IPage<ContentDto> page(IPage page, @Param("dto") ContentDto dto,@Param("domain") String domain);


    /**
     * 获取标签
     * @param siteId
     * @return
     */
    List<String> queryTag(@Param("siteId") String siteId);


    /**
     * 更新标签
     * @param topTag
     * @param id
     * @param siteId
     */
    void updateTag(@Param("topTag") String topTag, @Param("id") String id,@Param("siteId") String siteId);


    /**
     * 内容发布
     * @param ids
     * @param status
     * @param userId
     */
    void publish(@Param("ids") List<String> ids, @Param("status") Integer status,@Param("userId") String userId);

    List<CkRow> getContents(@Param("siteId") String siteId,@Param("status") Integer status,@Param("categoryId") String categoryId);

    List<ContentDto> getDetails(@Param("contentIds") Collection<String> contentIds, @Param("siteId") String siteId);

    IPage<ContentDto> listRecycle(IPage page, @Param("dto") ContentDto dto,  @Param("domain") String domain);

    void reduction(@Param("ids") List<String> ckRowList, @Param("status") Integer status, @Param("siteId") String siteId);

    void deletes(@Param("ids") List<String> ids, @Param("siteId") String siteId);

    void updateField(@Param("id") String id, @Param("field") String field, @Param("val") Object val);

    String queryFormCode(@Param("id") String id);

    Long queryViews(@Param("contentId") String contentId);

    Long queryClicks(@Param("contentId")  String contentId);

    void topIt(@Param("ids") List<String> ids, @Param("status") Integer status);

    void recomdIt(@Param("ids") List<String> ids, @Param("status") Integer status);

    void headIt(@Param("ids") List<String> ids, @Param("status") Integer status);

    List<String> queryKeys(@Param("prefix") String contentPrefix, @Param("def") String none,
                           @Param("colon") String colon,
                           @Param("ids") List<String> ids);

    void upViews(@Param("contentId") String contentId, @Param("views") Long views);

    void upLikes(@Param("contentId") String contentId, @Param("likes") Long likes);

}