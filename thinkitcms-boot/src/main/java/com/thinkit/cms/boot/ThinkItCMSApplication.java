package com.thinkit.cms.boot;

import com.alicp.jetcache.anno.config.EnableMethodCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author LONG
 */
@SpringBootApplication(scanBasePackages = {"com.thinkit.cms","com.thinkit.cms.authen"})
@MapperScan(basePackages ={"com.thinkit.cms.*.mapper"})
@EnableMethodCache(basePackages = "com.thinkit.cms")
@EnableScheduling
public class ThinkItCMSApplication {
    public static void main(String[] args) {
        SpringApplicationBuilder springApplicationBuilder =new SpringApplicationBuilder(ThinkItCMSApplication.class);
        springApplicationBuilder.headless(true).run(args);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Success~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
