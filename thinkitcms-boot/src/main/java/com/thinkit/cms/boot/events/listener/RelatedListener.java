package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.boot.events.RelatedEvent;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.related.RelatedDto;
import com.thinkit.cms.model.ModelJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class RelatedListener extends BaseListener  implements ApplicationListener<RelatedEvent> {

    @Override
    public void onApplicationEvent(RelatedEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()) {
                    case SAVE_BATCH_RELATED -> {
                        List<RelatedDto> relateds = (List<RelatedDto>)event.getSource();
                        relateds.forEach(related->{
                            ModelJson modelJson  = getToMap(related);
                            RedisLuaUtils.setJson(modelJson);
                        });
                    }
                    case DELETE_RELATED -> {
                        RelatedDto relatedDto = (RelatedDto)event.getSource();
                        ModelJson modelJson  = getToMap(relatedDto);
                        RedisLuaUtils.setJson(modelJson);
                    }
                }
            }
        }catch (Exception e){
            log.error("RelatedEsListener 执行失败：{}",e.getMessage());
        }
    }
}
