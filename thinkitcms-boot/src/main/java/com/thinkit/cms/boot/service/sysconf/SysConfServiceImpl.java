package com.thinkit.cms.boot.service.sysconf;
import com.thinkit.cms.api.sysconf.SysConfService;
import com.thinkit.cms.boot.entity.sysconf.SysConfEntity;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.sysconf.SysConfDto;
import com.thinkit.cms.boot.mapper.sysconf.SysConfMapper;
import org.springframework.stereotype.Service;
/**
* @author System
*/

@Service
public class SysConfServiceImpl extends BaseServiceImpl<SysConfDto,SysConfEntity,SysConfMapper> implements SysConfService{




}