package com.thinkit.cms.boot.funs.mehod;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.MethodEnum;
import com.thinkit.cms.directive.executor.BaseMethod;
import com.thinkit.cms.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 时间格式化
 * @author LONG
 */
@Component
public class TagSplit extends BaseMethod {

    @Override
    public MethodEnum getName() {
        return MethodEnum.TAG_SPLIT;
    }

    @Override
    public Object exec(List list) throws TemplateModelException {
        List<String> tags = new ArrayList<>();
        String str = getString(0,list, LongEnum.STRING);
        if(Checker.BeNotBlank(str)){
            Collections.addAll(tags, str.split(","));
        }
        return tags;
    }
}
