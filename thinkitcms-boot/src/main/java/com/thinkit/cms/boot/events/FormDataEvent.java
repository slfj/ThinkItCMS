package com.thinkit.cms.boot.events;

import com.thinkit.cms.enums.EventEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

public class FormDataEvent extends ApplicationEvent {

    @Getter @Setter
    public EventEnum eventEnum;

    public FormDataEvent(Object source, EventEnum eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}
