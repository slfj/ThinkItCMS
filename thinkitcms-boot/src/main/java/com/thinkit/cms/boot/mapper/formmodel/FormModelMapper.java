package com.thinkit.cms.boot.mapper.formmodel;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.formmodel.FormModelEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface FormModelMapper extends BaseMapper<FormModelEntity> {

    IPage<FormModelDto> page(IPage page, @Param("dto") FormModelDto dto);

    /**
     * 查询模型列表
     * @param formModel
     * @return
     */
    List<FormModelDto> listModel(@Param("formModel") FormModelDto formModel);


    /**
     * 查询模型列表 listModelByTmpId
     * @param formModel
     * @return
     */
    List<FormModelDto> listModelByTmpId(@Param("formModel") FormModelDto formModel);


    /**
     * 查看是否存在编码
     * @param id
     * @param code
     * @return
     */
    Integer checkHasCode(@Param("id") String id, @Param("code") String code, @Param("formType") String formType);


    /**
     * 查询模板部件
     * @param page
     * @param dto
     * @return
     */
    IPage<FormModelDto> pageFragment(IPage page, @Param("dto") FormModelDto dto);

    String findPath(@Param("code") String code, @Param("siteId") String siteId,@Param("formType") String formType);

    String hasIndexModel(@Param("siteId") String siteId, @Param("formType") String formType);

    List<FormModelDto> loadParmModel(@Param("siteId") String siteId);

    String queryFormCode(@Param("id") String id);
}