package com.thinkit.cms.boot.controller.sysuser;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.sysuser.LockUserDto;
import com.thinkit.cms.dto.sysuser.SysUserDto;
import com.thinkit.cms.dto.sysuser.UpdateBaseUser;
import com.thinkit.cms.dto.sysuser.UpdatePw;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

import static com.thinkit.cms.enums.LogModule.SYSUSER;
import static com.thinkit.cms.enums.LogOperation.*;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/sysUser")
@Slf4j
public class SysUserController extends BaseController<SysUserService> {


        @Logs(module = SYSUSER,operation = "查看用户详情")
        @SaCheckPermission("suser:edit")
        @GetMapping(value="/detail")
        public CmsResult detail(@Validated @NotBlank(message = "ID不能为空") String id){
               return service.detail(id);
        }


        @SaCheckPermission("suser:setrole")
        @GetMapping(value="/tipRoles")
        public CmsResult tipRoles(@Validated @NotBlank(message = "用户ID不能为空") String userId){
                return service.tipRoles(userId);
        }


        @SaCheckPermission("sysuser")
        @RequestMapping(value = "/page", method = RequestMethod.POST)
        public PageModel<SysUserDto> list(@RequestBody PageModel<SysUserDto> pageDto) {
             return service.listPage(pageDto);
        }


        @Sentinel
        @Logs(module = SYSUSER,operaEnum =SAVE )
        @SaCheckPermission("suser:add")
        @RequestMapping(value="save",method= RequestMethod.POST)
        public CmsResult save(@Validated(value = {ValidGroup1.class}) @RequestBody SysUserDto sysUserDto) {
             return service.save(sysUserDto);
        }


        @Sentinel
        @Logs(module = SYSUSER,operaEnum =UPDATE)
        @SaCheckPermission("suser:edit")
        @RequestMapping(value="update",method= RequestMethod.PUT)
        public CmsResult update(@Validated(value = {ValidGroup2.class}) @RequestBody SysUserDto sysUserDto) {
              return service.update(sysUserDto);
        }


        @Sentinel
        @Logs(module = SYSUSER,operaEnum =DELETE)
        @SaCheckPermission("suser:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

        @Sentinel
        @Logs(module = SYSUSER,operaEnum =LOCK)
        @SaCheckPermission("suser:lock")
        @PutMapping(value="/lock")
        public CmsResult lock(@Validated(value = {ValidGroup1.class}) @RequestBody LockUserDto lockUserDto ){
                return service.lock(lockUserDto);
        }


        @Logs(module = SYSUSER,operation = "更新个人信息")
        @PutMapping(value="/updateBaseInfo")
        public void updateBaseInfo(@Validated(value = {ValidGroup1.class}) @RequestBody UpdateBaseUser updateBaseUser ){
                 service.updateBaseInfo(updateBaseUser);
        }


        @Logs(module = SYSUSER,operation = "更换头像")
        @PutMapping(value="/updateAvatar")
        public void updateAvatar(@Validated(value = {ValidGroup2.class}) @RequestBody UpdateBaseUser updateBaseUser ){
                service.updateBaseInfo(updateBaseUser);
        }

        @Sentinel
        @Logs(module = SYSUSER,operation = "更新密码")
        @PutMapping(value="/updatePw")
        public void updatePw(@Validated(value = {ValidGroup1.class}) @RequestBody UpdatePw updatePw ){
                service.updatePw(updatePw);
        }



        @GetMapping(value="/info")
        public CmsResult userInfo(){
              return service.info();
        }
}