package com.thinkit.cms.boot.jobs.executers;

import cn.hutool.extra.spring.SpringUtil;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpFunService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.directive.task.TaskService;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;

import java.util.concurrent.CountDownLatch;

public abstract class Executer implements Runnable{

    protected User user;

    protected String  siteId;

    protected String taskId;

    protected TaskEnum taskEnum;

    protected CountDownLatch countDownLatch;

    protected FormModelService formModelService = SpringUtil.getBean(FormModelService.class);

    protected SiteService siteService = SpringUtil.getBean(SiteService.class);


    protected FormModelTmpFunService tmpFunService = SpringUtil.getBean(FormModelTmpFunService.class);

    protected ContentService contentService = SpringUtil.getBean(ContentService.class);

    protected TaskService taskService = SpringUtil.getBean(TaskService.class);

    protected CategoryService categoryService = SpringUtil.getBean(CategoryService.class);



    public Executer(User user, String siteId, String taskId, TaskEnum taskEnum, CountDownLatch countDownLatch) {
        this.user = user;
        this.siteId = siteId;
        this.taskId = taskId;
        this.taskEnum = taskEnum;
        this.countDownLatch =  countDownLatch;
    }


    public Executer(User user, String siteId, String taskId, TaskEnum taskEnum) {
        this.user = user;
        this.siteId = siteId;
        this.taskId = taskId;
        this.taskEnum = taskEnum;
    }

    public Executer( ) {
    }
}
