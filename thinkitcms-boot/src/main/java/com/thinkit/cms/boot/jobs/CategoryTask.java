package com.thinkit.cms.boot.jobs;
import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.utils.TaskUtils;
import com.thinkit.cms.dto.category.CategoryJob;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Set;

/**
 * 栏目定时
 * @author LONG
 */
@Slf4j
@Component
public class CategoryTask {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SiteService siteService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private TaskUtils taskUtils;

    /**
     *  每1分钟 执行一次扫描
     */
    @Scheduled(cron = "0 0/1 * * * ?")
    public void executeTask() {
         Set<String> siteIds = siteService.listSiteIds();
         if(Checker.BeNotEmpty(siteIds)){
           log.info("==========开始扫描栏目定时查询==========");
           for(String siteId:siteIds){
               String key=SysConst.jobCategory+siteId+SysConst.colon+"*";
               Set<String> categoryIds = taskUtils.scan(key);
               doIt(categoryIds);
           }
         }
    }

    private void doIt(Set<String> categoryIds){
        if(Checker.BeNotEmpty(categoryIds)){
            for(String key:categoryIds){
                CategoryJob categoryJob = (CategoryJob) redisTemplate.opsForList().index(key,0);
                boolean isExc = Checker.BeNotNull(categoryJob) && !categoryJob.getIsRuing() && categoryJob.getEnable();
                if(isExc){
                    genFile(key,categoryJob);
                }
            }
        }
    }

    private void genFile(String key,CategoryJob categoryJob){
        if(Checker.BeNotNull(categoryJob) && categoryJob.getEnable()){
            try {
                if(Checker.BeBlank(categoryJob.getLastExecTime())){
                    Date startTime = DateUtil.parse(categoryJob.getJobStartTime(),"yyyy-MM-dd HH:mm:ss");
                    checkDoIt(key,startTime,categoryJob);
                }else{
                    Date lastTime = DateUtil.parse(categoryJob.getLastExecTime(),"yyyy-MM-dd HH:mm:ss");
                    Date nextTime = DateUtil.offset(lastTime, DateField.HOUR_OF_DAY,categoryJob.getCycle());
                    checkDoIt(key,nextTime,categoryJob);
                }
            }catch (Exception e){
                log.error("定时执行异常：{}",e.getMessage());
            }finally {
                update(key,false,categoryJob);
            }
        }
    }

    private void checkDoIt(String key,Date execDate,CategoryJob categoryJob){
        Date nowTime = new Date();
        if(nowTime.after(execDate)){
            categoryService.genStaticFile(categoryJob);
            categoryJob.setLastExecTime(DateUtil.format(nowTime,"yyyy-MM-dd HH:mm:ss"));
            update(key,true,categoryJob);
        }else{
            log.info("==========未扫描到需要执行的任务==========");
        }
    }


    private void update(String key,boolean isRuning ,CategoryJob categoryJob){
        categoryJob.setIsRuing(isRuning);
        redisTemplate.delete(key);
        redisTemplate.opsForList().leftPush(key,categoryJob);
    }
}
