package com.thinkit.cms.boot.controller.userrole;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.userrole.UserRoleService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.userrole.UserRoleDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.thinkit.cms.enums.LogModule.ROLE;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/userRole")
@Slf4j
public class UserRoleController extends BaseController<UserRoleService> {



        @Logs(module = ROLE,operation = "用户分配角色")
        @Sentinel
        @SaCheckPermission("suser:setrole")
        @PutMapping(value="save")
        public void save(@Validated(value = {ValidGroup1.class}) @RequestBody UserRoleDto userRoleDto) {
              service.save(userRoleDto);
        }

}