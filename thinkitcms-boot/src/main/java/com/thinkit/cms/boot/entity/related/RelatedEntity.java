package com.thinkit.cms.boot.entity.related;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_content_related")
public class RelatedEntity extends BaseModel  {
       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         当前内容id
       */
        @TableField("content_id")
        private String contentId;

       /**
         关联的内容ID
       */
        @TableField("related_id")
        private String relatedId;

       /**
         创建人名称
       */
        @TableField("create_name")
        private String createName;

       /**
         修改人名称
       */
        @TableField("modified_name")
        private String modifiedName;


}