package com.thinkit.cms.boot.mapper.related;
import com.thinkit.cms.dto.content.ContentDto;
import com.thinkit.cms.dto.related.RelatedDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.related.RelatedEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface RelatedMapper extends BaseMapper<RelatedEntity> {


    /**
     * 查询推荐的内容
     * @param contentId
     * @return
     */
    List<ContentDto> listRelateds(@Param("contentId") String contentId,
                                  @Param("siteId") String siteId,
                                  @Param("domain") String domain);

    /**
     * 查询当前站点的所有数据
     * @param siteId
     * @return
     */
    List<RelatedDto> getDatas( @Param("siteId") String siteId);
}