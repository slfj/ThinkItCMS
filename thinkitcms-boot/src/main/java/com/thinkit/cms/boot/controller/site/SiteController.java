package com.thinkit.cms.boot.controller.site;
import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.dto.template.DefTemplate;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import static com.thinkit.cms.enums.LogModule.SITE;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/site")
@Slf4j
public class SiteController extends BaseController<SiteService> {


        @SaCheckPermission("site")
        @RequestMapping(value = "/page", method = RequestMethod.POST)
        public PageModel<SiteDto> list(@RequestBody PageModel<SiteDto> pageDto) {
             return service.page(pageDto);
        }



        @Logs(module = SITE,operation = "保存站点")
        @Sentinel
        @SaCheckPermission("site:add")
        @RequestMapping(value="createSite",method= RequestMethod.POST)
        public void createSite(@Validated(value = {ValidGroup1.class}) @RequestBody SiteDto siteDto) {
              service.createSite(siteDto);
        }


        @Logs(module = SITE,operation = "更新站点")
        @Sentinel
        @SaCheckPermission("site:edit")
        @RequestMapping(value="update",method= RequestMethod.PUT)
        public void updateSite(@Validated @RequestBody SiteDto siteDto) {
              service.updateSite(siteDto);
        }


        @Logs(module = SITE,operation = "站点删除")
        @SaCheckPermission("site:delete")
        @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@Validated @NotBlank(message = "请选择要删除的记录") String id){
                return service.deleteSite(id);
        }


        @Logs(module = SITE,operation = "站点详情查看")
        @SaCheckPermission("site:edit")
        @GetMapping(value="detail")
        public SiteDto detail(@NotBlank(message = "主键不能为空") String id) {
               return service.detail(id);
        }

        @GetMapping(value="listSite")
        public CmsResult listSite() {
            return service.listSite();
        }


        // @Sentinel
        @Logs(module = SITE,operation = "设置默认站点")
        @SaCheckPermission("site:setdef")
        @PutMapping(value="setDefault")
        public CmsResult setDefault(@Validated @NotBlank(message = "站点不能为空") @RequestParam("siteId") String siteId) {
            return service.setDefault(siteId);
        }



        @Logs(module = SITE,operation = "站点全部生成")
        @SaCheckPermission("site:genallsite")
        @PutMapping(value="createSiteFinish")
        public String createSiteFinish(@Validated @NotBlank(message = "站点不能为空") @RequestParam("siteId") String siteId) {
              return service.createSiteFinish(siteId);
        }



        @Logs(module = SITE,operation = "同步到 es ")
        @SaCheckPermission("site:sync:toes")
        @PutMapping(value="syncToEs")
        public String syncToEs(@Validated @NotBlank(message = "站点不能为空") @RequestParam("siteId") String siteId) {
            return service.syncToEs(siteId);
        }


        @Logs(module = SITE,operation = "设置站点的默认模板")
        @Sentinel
        @SaCheckPermission("site:setdef:temp")
        @PutMapping(value="setDefTemplate")
        public CmsResult setDefTemp(@Validated @RequestBody DefTemplate defTemplate) {
            return service.setDefTemplate(defTemplate.getTempId(),defTemplate.getSiteId());
        }

        @GetMapping(value="loadDefTemplate")
        public CmsResult loadDefTemplate() {
            return service.loadTempFolder();
        }

}