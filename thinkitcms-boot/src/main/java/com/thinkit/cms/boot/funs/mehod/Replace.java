package com.thinkit.cms.boot.funs.mehod;

import com.thinkit.cms.directive.emums.LongEnum;
import com.thinkit.cms.directive.emums.MethodEnum;
import com.thinkit.cms.directive.executor.BaseMethod;
import com.thinkit.cms.utils.Checker;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 时间格式化
 * @author LONG
 */
@Component
public class Replace extends BaseMethod {

    @Override
    public MethodEnum getName() {
        return MethodEnum.REPLACE;
    }

    @Override
    public Object exec(List list) throws TemplateModelException {
        String str = getString(0,list, LongEnum.STRING);
        String sourceSeparator = getString(1,list, LongEnum.STRING);
        String targetSeparator = getString(2,list, LongEnum.STRING);
        if(Checker.BeNotBlank(str) && Checker.BeNotBlank(sourceSeparator) && Checker.BeNotBlank(targetSeparator)){
            return  str.replace(sourceSeparator,targetSeparator);
        }
        return "";
    }
}
