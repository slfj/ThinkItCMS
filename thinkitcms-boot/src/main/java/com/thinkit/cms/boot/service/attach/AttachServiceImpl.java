package com.thinkit.cms.boot.service.attach;
import com.thinkit.cms.api.attach.AttachService;
import com.thinkit.cms.boot.entity.attach.AttachEntity;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.attach.AttachDto;
import com.thinkit.cms.boot.mapper.attach.AttachMapper;
import org.springframework.stereotype.Service;
/**
* @author LG
*/

@Service
public class AttachServiceImpl extends BaseServiceImpl<AttachDto,AttachEntity,AttachMapper> implements AttachService{




}