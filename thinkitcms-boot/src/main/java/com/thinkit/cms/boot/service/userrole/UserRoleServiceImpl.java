package com.thinkit.cms.boot.service.userrole;

import com.google.common.collect.Lists;
import com.thinkit.cms.api.userrole.UserRoleService;
import com.thinkit.cms.boot.entity.userrole.UserRoleEntity;
import com.thinkit.cms.boot.mapper.userrole.UserRoleMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.dto.userrole.UserRoleDto;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author LG
*/

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleDto, UserRoleEntity, UserRoleMapper> implements UserRoleService {


    @Override
    public List<String> roleIdsByUserId(String userId) {
        List<String> roleIds = baseMapper.roleIdsByUserId(userId);
        return Checker.BeNotEmpty(roleIds)?roleIds: Lists.newArrayList();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(UserRoleDto userRoleDto) {
        String userId = userRoleDto.getUserId();
        List<String> roleIds = userRoleDto.getRoleIds();
        deleteByFiled("user_id",userId);
        List<UserRoleDto> userRoles = new ArrayList<>();
        for(String roleId:roleIds){
            userRoles.add(new UserRoleDto(userId,roleId));
        }
        super.insertBatch(userRoles);
        clearCache(SysConst.authCache);
        logout(userId);
    }
}