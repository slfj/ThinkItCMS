package com.thinkit.cms.boot.controller.site;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.ValidGroup1;
import com.thinkit.cms.annotation.ValidGroup2;
import com.thinkit.cms.api.site.WebStaticFileService;
import com.thinkit.cms.authen.annotation.NoDecorate;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.dto.site.WebFileDto;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.FileViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import static com.thinkit.cms.enums.LogModule.RESOURCE;

@Validated
@RestController
@RequestMapping("/api/webstatic")
public class WebStaticFileController {

    @Autowired
    WebStaticFileService webStaticFileService;

    @GetMapping("page")
    public List<FileViewModel> listFile(@RequestParam String path) {
        return webStaticFileService.listFile(path);
    }


    @Logs(module = RESOURCE,operation = "静态资源删除 ")
    @Sentinel
    @SaCheckPermission("webfile:delete")
    @DeleteMapping(value = "/deleteFile")
    public void deleteFile(@RequestParam("path") String filePath){
          webStaticFileService.deleteFile(filePath);
    }


    @Logs(module = RESOURCE,operation = "创建文件夹 ")
    @Sentinel
    @PutMapping("mkdirFolder")
    public CmsResult createFile(@RequestParam(required = false,value = "filePath",defaultValue = "") String filePath,
                                @RequestParam(required = false,value = "folderName",defaultValue = "") String folderName){
        return null;
    }



    @Logs(module = RESOURCE,operation = "查看文件内容 ")
    @SaCheckPermission("webfile:view:content")
    @GetMapping("fileContent")
    public CmsResult fileContent(String path){
        return webStaticFileService.fileContent(path);
    }



    @Logs(module = RESOURCE,operation = "更新文件内容 ")
    @Sentinel
    @SaCheckPermission("webfile:save:content")
    @PutMapping("setContent")
    public CmsResult setContent(@Validated(value = {ValidGroup1.class, ValidGroup2.class}) @RequestBody WebFileDto webFileDto){
        return webStaticFileService.setContent(webFileDto);
    }

    @PutMapping(value="openDir")
    public void openDir(@Validated(value = {ValidGroup2.class}) @RequestBody WebFileDto webFileDto){
        webStaticFileService.openDir(webFileDto.getPath());
    }


    @NoDecorate
    @PutMapping(value="zipDown")
    public void zipDown(@Validated(value = {ValidGroup2.class}) @RequestBody WebFileDto webFileDto){
        webStaticFileService.zipDown(webFileDto.getPath());
    }
}
