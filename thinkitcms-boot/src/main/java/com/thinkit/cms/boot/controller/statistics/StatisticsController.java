package com.thinkit.cms.boot.controller.statistics;
import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.api.statistics.StatisticsService;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.statistics.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/statistics")
@Slf4j
public class StatisticsController extends BaseController<ContentService> {

        @Autowired
        private StatisticsService statisticsService;

        @RequestMapping(value = "/contentTotal", method = RequestMethod.GET)
        public Map<String,Object> contentTotal(StatisticsDto statisticsDto) {
                return  statisticsService.contentTotal(statisticsDto);
        }

        @RequestMapping(value = "/dateTotal", method = RequestMethod.GET)
        public List<DateCountResDto> dateTotal(StatisticsDto statisticsDto) {
                return  statisticsService.dateTotal(statisticsDto);
        }

        @RequestMapping(value = "/categoryTotal", method = RequestMethod.GET)
        public List<TxtCountResDto> categoryTotal(StatisticsCategoryDto statisticsCategoryDto) {
                return  statisticsService.categoryTotal(statisticsCategoryDto);
        }

        @RequestMapping(value = "/topTotal", method = RequestMethod.GET)
        public List<TopContent> topTotal(TopContent topContent) {
                return  statisticsService.topTotal(topContent);
        }
}