package com.thinkit.cms.boot.jobs.executers;

import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;

import java.util.concurrent.CountDownLatch;

public class CategoryExecuter extends Executer {

    public CategoryExecuter(User user, String siteId, String taskId, TaskEnum taskEnum, CountDownLatch countDownLatch){
         super(user,siteId,taskId,taskEnum,countDownLatch);
    }
    @Override
    public void run() {
        categoryService.executer(user,siteId,taskId,taskEnum);
    }
}
