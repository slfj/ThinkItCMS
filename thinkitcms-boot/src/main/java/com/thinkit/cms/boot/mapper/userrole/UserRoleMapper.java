package com.thinkit.cms.boot.mapper.userrole;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.userrole.UserRoleEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {

    List<String> roleIdsByUserId(@Param("userId") String userId);
}