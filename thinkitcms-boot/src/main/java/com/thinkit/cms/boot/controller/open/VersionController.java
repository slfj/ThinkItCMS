package com.thinkit.cms.boot.controller.open;

import com.thinkit.cms.api.open.OpenSearchService;
import com.thinkit.cms.model.CmsResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/openapi")
@Slf4j
public class VersionController {


    @Autowired
    private OpenSearchService openSearchService;

    @RequestMapping(value = "/searchVersion", method = RequestMethod.GET)
    public CmsResult searchVersion(@RequestParam(required = false,defaultValue = "") String domain) {
       return openSearchService.searchVersion(domain);
    }
}
