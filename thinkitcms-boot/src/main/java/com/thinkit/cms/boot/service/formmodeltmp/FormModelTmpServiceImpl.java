package com.thinkit.cms.boot.service.formmodeltmp;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpService;
import com.thinkit.cms.boot.entity.formmodeltmp.FormModelTmpEntity;
import com.thinkit.cms.boot.mapper.formmodeltmp.FormModelTmpMapper;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.dto.formmodeltmp.FormModelTmpDto;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import org.springframework.stereotype.Service;

/**
* @author LG
*/

@Service
public class FormModelTmpServiceImpl extends BaseServiceImpl<FormModelTmpDto,FormModelTmpEntity,FormModelTmpMapper> implements FormModelTmpService{


    @Override
    public void save(FormModelTmpDto formModelTmpDto) {


    }

    @Override
    public void saveTmp(FormModelDto formModelDto) {
        String modelId  =  formModelDto.getId();
        SiteConf siteConf = getUserCtx().getSite(true);
        String templateId = siteConf.getTemplateId();
        FormModelTmpDto formModelTmpDto = new FormModelTmpDto();
        formModelTmpDto.setTemplateId(templateId);
        formModelTmpDto.setFormModelId(modelId);
        formModelTmpDto.setTmpPath(formModelDto.getTmpPath());
        formModelTmpDto.setMobileTmpPath(formModelDto.getMobileTmpPath());
        formModelTmpDto.setSiteId(siteConf.getSiteId());
        super.insert(formModelTmpDto);
    }

    @Override
    public void getDetail(FormModelDto formModelDto) {
        String modelId  =  formModelDto.getId();
        SiteConf siteConf = getUserCtx().getSite(false);
        if(Checker.BeNull(siteConf)){
            throw new CustomException(5040);
        }
        String templateId = siteConf.getTemplateId();
        if(Checker.BeNotBlank(templateId)){
            FormModelTmpDto formModelTmpDto =  baseMapper.getDetail(modelId,templateId,getSiteId(true));
            if(Checker.BeNotNull(formModelTmpDto)){
                formModelDto.setTmpPath(formModelTmpDto.getTmpPath());
                formModelDto.setMobileTmpPath(formModelTmpDto.getMobileTmpPath());
            }
        }
    }

    @Override
    public void updateTmp(FormModelDto formModelDto) {
        String modelId  =  formModelDto.getId();
        SiteConf siteConf = getUserCtx().getSite(false);
        if(Checker.BeNull(siteConf)){
            throw new CustomException(5040);
        }
        String templateId = siteConf.getTemplateId();
        String tempPath = formModelDto.getTmpPath();
        String mobileTempPath = formModelDto.getMobileTmpPath();
        if(Checker.BeNotBlank(modelId) && Checker.BeNotBlank(templateId)){
            String siteId = getSiteId(true);
            Integer count = baseMapper.getHasTemp(modelId,templateId,siteId);
            if(count>0){
                baseMapper.updateTmp(modelId,templateId,tempPath,mobileTempPath,siteId);
            }else{
                saveTmp(formModelDto);
            }
        }
    }
}