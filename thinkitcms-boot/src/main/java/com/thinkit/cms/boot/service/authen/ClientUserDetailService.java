package com.thinkit.cms.boot.service.authen;

import com.thinkit.cms.authen.annotation.UserSvr;
import com.thinkit.cms.authen.enums.GrantTypes;
import com.thinkit.cms.authen.enums.LoginTypes;
import com.thinkit.cms.user.User;
import com.thinkit.cms.user.UserDetail;
import com.thinkit.cms.user.UserDetailService;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author LONG
 */
@Service
@UserSvr(plat = LoginTypes.MEM_USER,grant = GrantTypes.PASSWORD)
public class ClientUserDetailService implements UserDetailService {

    @Override
    public UserDetail loadUserName(String userName) {
        User user = new User();
        user.setId("1");
        user.setAccount("test");
        user.setPassword("todo");
        return user;//sysUserService.loadUserName(userName);
    }



    @Override
    public List<String> getPermissionList(String loginId, String loginType) {
        System.out.println("=============开始查询 权限列表===============");
        return Arrays.asList("user:add","user:edit");
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        System.out.println("=============开始查询 角色列表===============");
        return Arrays.asList("manager","sysuser");
    }

    @Override
    public boolean checkVerifyCode(String verifyUid, String verifyCode) {
        return false;
    }

    @Override
    public void destroyVerifyCode(String verifyUid, String verifyCode) {

    }
}
