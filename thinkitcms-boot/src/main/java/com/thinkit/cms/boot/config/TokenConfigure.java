package com.thinkit.cms.boot.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.hutool.core.io.FileUtil;
import com.thinkit.cms.boot.apsect.MyInterceptor;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LONG
 */
@Slf4j
@Configuration
public class TokenConfigure implements WebMvcConfigurer {

    // 注册 Sa-Token 拦截器，打开注解式鉴权功能
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册 Sa-Token 拦截器，打开注解式鉴权功能
        registry.addInterceptor(new SaInterceptor()).excludePathPatterns("/druid/*").addPathPatterns("/**");
        registry.addInterceptor(new MyInterceptor()).addPathPatterns("/api/**");
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        // 第一种方式是将 json 处理的转换器放到第一位，使得先让 json 转换器处理返回值，这样 String转换器就处理不了了。
        // converters.add(0, new MappingJackson2HttpMessageConverter());
        // 第二种就是把String类型的转换器去掉，不使用String类型的转换器
        converters.removeIf(httpMessageConverter -> httpMessageConverter.getClass() == StringHttpMessageConverter.class);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        try {
            String locations = CmsFolderUtil.getSitePath(null);
            locations = CmsFolderUtil.suffix(locations);
            File[] files = FileUtil.ls(locations);
            if(Checker.BeNotEmpty(files)){
                List<String> fileNames = new ArrayList<>();
                for(File file:files){
                    if(file.exists() && file.isDirectory()){
                        fileNames.add(file.getName()+"/**");
                    }
                }
                // registry.addResourceHandler(fileNames.toArray(new String[fileNames.size()])).addResourceLocations("file:///"+locations);
                registry.addResourceHandler("/**").addResourceLocations("file:///"+locations).
                addResourceLocations("file:///"+CmsFolderUtil.getUpload());
            }
        }catch (Exception e){
            log.error("异常：{}",e.getMessage());
        }
    }
}
