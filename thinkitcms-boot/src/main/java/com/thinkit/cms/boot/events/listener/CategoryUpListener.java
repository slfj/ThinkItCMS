package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.api.category.CategoryService;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.event.CategoryUpEsEvent;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.model.ModelJson;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class CategoryUpListener extends BaseListener  implements ApplicationListener<CategoryUpEsEvent> {

    @Autowired
    private CategoryService categoryService;

    @Override
    public void onApplicationEvent(CategoryUpEsEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()){
                    case UPDATE_CATEGORY_FIELD -> {
                        ModelJson modelJson = ( ModelJson)event.getSource();
                        String pk = modelJson.getPk();
                        if(Checker.BeNotBlank(pk)){
                            String formModelCode = categoryService.queryCode(pk);
                            String key = modelJson.buildKey(formModelCode, SysConst.colon);
                            Map<String,Object> data = RedisLuaUtils.getJson(key);
                            if(Checker.BeNotEmpty(data)){
                                data.putAll(modelJson.getObjectMap());
                                modelJson.setObjectMap(data);
                                modelJson.setKey(key);
                                RedisLuaUtils.setJson(modelJson);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            log.error("CategoryUpEsListener 执行失败：{}",e.getMessage());
        }
    }
}
