package com.thinkit.cms.boot.mapper.statistics;

import com.thinkit.cms.dto.statistics.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface StatisticsMapper   {


    StatisticsResDto contentTotal(@Param("dto") StatisticsDto statisticsDto);

    List<DateCountResDto> dateTotal(@Param("dto") StatisticsDto statisticsDto);

    List<TxtCountResDto> categoryTotal(@Param("dto") StatisticsCategoryDto statisticsCategoryDto);

    List<TopContent> topTotal(@Param("dto") TopContent topContent,@Param("num") Integer num,@Param("domain") String domain);
}