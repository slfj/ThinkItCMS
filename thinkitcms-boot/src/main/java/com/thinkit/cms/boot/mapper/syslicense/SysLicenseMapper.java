package com.thinkit.cms.boot.mapper.syslicense;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.syslicense.SysLicenseEntity;

/**
* @author LG
*/

@Mapper
public interface SysLicenseMapper extends BaseMapper<SysLicenseEntity> {

}