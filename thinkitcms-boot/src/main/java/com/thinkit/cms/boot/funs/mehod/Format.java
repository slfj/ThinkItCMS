package com.thinkit.cms.boot.funs.mehod;

import com.thinkit.cms.directive.emums.MethodEnum;
import com.thinkit.cms.directive.executor.BaseMethod;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 时间格式化
 * @author LONG
 */
@Component
public class Format extends BaseMethod {

    @Override
    public MethodEnum getName() {
        return MethodEnum.FORMAT;
    }

    @Override
    public Object exec(List list) {
        return null;
    }
}
