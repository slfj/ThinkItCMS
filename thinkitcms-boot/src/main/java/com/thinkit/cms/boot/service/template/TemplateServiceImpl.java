package com.thinkit.cms.boot.service.template;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.netio.NetIoService;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.boot.entity.template.TemplateEntity;
import com.thinkit.cms.boot.mapper.site.SiteMapper;
import com.thinkit.cms.boot.mapper.template.TemplateMapper;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.template.*;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsFolderUtil;
import com.thinkit.cms.utils.FolderTree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.beans.Transient;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* @author LG
*/
@Slf4j
@Service
public class TemplateServiceImpl extends BaseServiceImpl<TemplateDto,TemplateEntity,TemplateMapper> implements TemplateService{


    @Autowired
    private SiteMapper siteMapper;

    @Autowired
    private NetIoService netIoService;




    @Override
    @Transient
    public PageModel<TemplateDto> listPage(PageModel<TemplateDto> pageModel){
        IPage page = new Page(pageModel.getCurrent(),pageModel.getPageSize());
        pageModel.getDto().setCreateId(getUserId());
        IPage<TemplateDto> result  = baseMapper.listPage(page,pageModel.getDto());
        return new PageModel(result);
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(TemplateDto templateDto) {
        try {
            String code = templateDto.getCode(),dir=templateDto.getDir();
            if(isExist(null,code)){
                throw new CustomException(CmsResult.result(5041));
            }
            File file = CmsFolderUtil.initTemplateFolder(code,dir);
            String tmpDir = CmsFolderUtil.getRelativePath(file.getParent());
            templateDto.setTmpDir(tmpDir);
        }catch (IOException ioException){
            log.error("初始化模板失败！");
        }
        super.insert(templateDto);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(TemplateDto templateDto) {
        String code = templateDto.getCode(),id=templateDto.getId(),dir=templateDto.getDir();
        if(isExist(id,code)){
            throw new CustomException(CmsResult.result(5041));
        }
        try {
            File file = CmsFolderUtil.initTemplateFolder(code,dir);
            String tmpDir = CmsFolderUtil.getRelativePath(file.getParent());
            templateDto.setTmpDir(tmpDir);
        }catch (IOException ioException){
            log.error("初始化模板失败!");
        }
        super.updateByPk(templateDto);
    }

    @Override
    public CmsResult loopFolder(String tmpDir) {
        FolderTree folderTree = CmsFolderUtil.loopFile(true,tmpDir);
        return CmsResult.result(folderTree);
    }

    @Override
    public CmsResult mkdirFile(AddTemplate addTemplate) {
        String path = addTemplate.getTempPath();
        path = Base64.decodeStr(path, StandardCharsets.UTF_8);
        if(addTemplate.getIsDirectory()){
            String name = addTemplate.getName();
            if(Checker.BeNotBlank(path)){
                name = CmsFolderUtil.getName(path,name);
                CmsFolderUtil.create(false,path,name);
            }
        }else{
            path = CmsFolderUtil.suffix(path);
            String name = addTemplate.getName();
            String suffix = addTemplate.getSuffix();
            path = CmsFolderUtil.addSuffix(path,name,suffix,true);
            path = CmsFolderUtil.getFileName(path,name,suffix);
            try {
                FileUtil.file(path).createNewFile();
            } catch (IOException e) {
                log.error("文件创建失败!：{}",e.getMessage());
                return CmsResult.result(5042);
            }
        }
        return CmsResult.result();
    }

    public static void main(String[] args) {
        String path ="e:\\tk\\templates\\feel\\新建文件夹(5)\\新建文件夹1";
        path = CmsFolderUtil.suffix(path);
        path = CmsFolderUtil.addSuffix(path,"新建文件","html",true);
        path = CmsFolderUtil.getFileName(path,"新建文件","html");
        System.out.println(path);
    }

    @Override
    public CmsResult delFolder(DelTemplateFile delTemplateFile) {
        //删除文件夹
        String absolutePath = Base64.decodeStr(delTemplateFile.getAbsolutePath(), StandardCharsets.UTF_8);
        boolean res = FileUtil.del(absolutePath);
        if(res){
            return CmsResult.result();
        }
        return CmsResult.result(5043);
    }

    @Override
    public CmsResult renameFile(RenameFile renameFile) {
        String absolutePath = renameFile.getAbsolutePath();
        absolutePath = Base64.decodeStr(absolutePath, StandardCharsets.UTF_8);
        String fileName = renameFile.getName();
        File file = FileUtil.file(absolutePath);
        if(file.exists()){
            boolean isNoChange = file.getName().equals(fileName);
            if(!isNoChange){
                FileUtil.rename(file,CmsFolderUtil.clearBlank(fileName),true);
            }
        }
        return CmsResult.result();
    }

    @Override
    public CmsResult fileContent(GetTemplate getTemplate) {
        String content ="";
        String absolutePath = getTemplate.getAbsolutePath();
        absolutePath = Base64.decodeStr(absolutePath, StandardCharsets.UTF_8);
        boolean isDirectory = getTemplate.getIsDirectory();
        File file = FileUtil.file(absolutePath);
        if(file.exists() && !isDirectory){
            content= CmsFolderUtil.fileContent(file);
        }
        return CmsResult.result(content);
    }

    @Override
    public CmsResult saveContent(SaveContent content) {
        String path = Base64.decodeStr(content.getAbsolutePath(), StandardCharsets.UTF_8);
        boolean isOk = CmsFolderUtil.saveFileContent(path,content.getContent());
        if(isOk){
            return CmsResult.result();
        }
        return CmsResult.result(-1);
    }

    @Override
    public CmsResult loadTemp(String siteId) {
        Map<String,Object> res = new HashMap<>(16);
        String defTempId =baseMapper.getDefTemp(siteId);
        QueryWrapper queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("deleted",0);
        queryWrapper.select("name","id");
        List<TemplateDto> templates = baseMapper.selectList(queryWrapper);
        templates = Checker.BeNotEmpty(templates)?templates: Lists.newArrayList();
        res.put("rows",templates);
        res.put("defTempId",defTempId);
        return CmsResult.result(res);
    }

    @Override
    public FolderTree loadTempFolder(String templateId) {
        String tmpPath = baseMapper.loadTempFolder(templateId);
        if(Checker.BeNotEmpty(tmpPath)){
            tmpPath = CmsFolderUtil.getAbsolutePath(tmpPath);
            if(FileUtil.exist(tmpPath)){
                FolderTree folderTree = CmsFolderUtil.loopFile(false,tmpPath);
                return folderTree;
            }
        }
        return null;
    }

    @Cached(name = "cached::", key = "targetObject.class+'.loadTempPath.'+#templateId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String loadTempPath(String templateId) {
        String tmpPath = baseMapper.loadTempFolder(templateId);
        return  tmpPath;
    }

    @Cached(name = "cached::", key = "targetObject.class+'.loadTempPathBySiteId.'+#siteId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String loadTempPathBySiteId(String siteId) {
        return baseMapper.loadTempPathBySiteId(siteId);
    }

    @Override
    public void openDir(String absolutePath) {
        try {
            absolutePath = Base64.decodeStr(absolutePath, StandardCharsets.UTF_8);
            if(!FileUtil.isDirectory(absolutePath)){
                absolutePath = FileUtil.file(absolutePath).getParent();
            }
            Desktop.getDesktop().open(new File(absolutePath));
        }catch (Exception e){
            log.error("打开文件夹失败："+e.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean delete(String id) {
        List<String> siteIds = siteMapper.querySiteIdByTmpId(id);
        if(Checker.BeNotEmpty(siteIds)){
            siteIds.forEach(siteId->{
                String redisKey = SysConst.defSiteConf+siteId;
                redisTemplate.delete(redisKey);
                redisTemplate.delete(SysConst.siteDefConf+siteId);
            });
        }
        super.deleteLogicByPk(id);
        siteMapper.updateTmpSetNull(id);
        return true;
    }

    @Override
    public Boolean uploadCloud(UploadTemplate uploadTemplate) {
        String tmpDir = uploadTemplate.getTmpDir();
        if(Checker.BeNotBlank(tmpDir)){
            String  root  = CmsFolderUtil.getAbsolutePath(null);
            String srcFile=CmsFolderUtil.appendFolder(root,tmpDir);
            String fileName = new Date().getTime()+SysConst.zip;
            String targetFile=CmsFolderUtil.appendFolder(root,tmpDir,SysConst.blank_,fileName);
            File file = ZipUtil.zip(srcFile,targetFile);
            CmsResult cmsResult = netIoService.upload(fileName,fileName,file);
            if(cmsResult.success()){
                String url = cmsResult.getStr("data");
                baseMapper.updateFileUrl(uploadTemplate.getTemplateId(),url);
                FileUtil.del(file);
            }
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean pullCloud(PullTemplate pullTemplate) {
        TemplateService templateService = SpringUtil.getBean(TemplateService.class);
        String templateId = pullTemplate.getTemplateId();
        TemplateDto templateDto = templateService.getByPk(templateId);
        String fileUrl = templateDto.getTmpFileUrl();
        if(Checker.BeNotBlank(fileUrl)){
            String[] urlArr= fileUrl.split("\\|");
            fileUrl = urlArr[urlArr.length-1];
            String  root  = CmsFolderUtil.getAbsolutePath(null);
            String fileName = new Date().getTime()+SysConst.zip;
            String targetFile=CmsFolderUtil.appendFolder(root,templateDto.getTmpDir(),SysConst.blank_,fileName);
            Boolean oK = netIoService.down(fileUrl,new File(targetFile));
            if(oK){
                  //之前的文件夹文件  // 重命名之前的
                  String orginPath = CmsFolderUtil.appendFolder(root,templateDto.getTmpDir());
                  File orginfile = FileUtil.file(orginPath);
                  // 新的文件夹名称
                  String newName = CmsFolderUtil.appendFolder(orginfile.getName(),SysConst.blank_,String.valueOf(System.currentTimeMillis()));
                  if(orginfile.exists()){
                      FileUtil.rename(orginfile,newName,true);
                  }
                  // 解压
                  ZipUtil.unzip(targetFile,orginPath);
                  // 删除
                  FileUtil.del(targetFile);

            }
        }
        return Boolean.FALSE;
    }

    private boolean isExist(String id,String code){
        int count = baseMapper.isExist(id,code);
        return count >0;
    }

    @Override
    public CmsResult importFolder(String tmpDir, String fileName, InputStream inputStream) {
        if(!FileUtil.getSuffix(fileName).equals(SysConst.zipFile)){
            getResponse().setStatus(405);
            throw new CustomException(CmsResult.result(5110));
        }
        tmpDir = CmsFolderUtil.getAbsolutePath(tmpDir);
        if(FileUtil.exist(tmpDir)){
            String filePath = tmpDir+SysConst.prefix+fileName;
            File file = new File(filePath);
            FileUtil.writeFromStream(inputStream,file);
            ZipUtil.unzip(filePath);
            FileUtil.del(filePath);
        }
        return CmsResult.result();
    }
}