package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.boot.events.CategoryEvent;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.category.CategoryDto;
import com.thinkit.cms.model.ModelJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class CategoryListener extends BaseListener  implements ApplicationListener<CategoryEvent> {


    @Override
    public void onApplicationEvent(CategoryEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()){
                    case SAVE_BATCH_CATEGORY -> {
                        List<CategoryDto> categorys = getList(event);
                        categorys.forEach(category->{
                            ModelJson modelJson  = getToMap(category);
                            RedisLuaUtils.setJson(modelJson,category.getFormModelCode(), SysConst.colon);
                        });
                    }
                    case SAVE_CATEGORY,UPDATE_CATEGORY->{
                        CategoryDto category = getObj(event);
                        ModelJson modelJson  = getToMap(category);
                        RedisLuaUtils.setJson(modelJson,category.getFormModelCode(), SysConst.colon);
                    }
                }
            }
        }catch (Exception e){
            log.error("CategoryRedisListener 执行失败：{}",e.getMessage());
        }
    }
}
