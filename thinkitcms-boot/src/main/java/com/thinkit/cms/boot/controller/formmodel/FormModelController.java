package com.thinkit.cms.boot.controller.formmodel;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.thinkit.cms.annotation.Logs;
import com.thinkit.cms.annotation.Site;
import com.thinkit.cms.annotation.ValidGroup4;
import com.thinkit.cms.api.formmodel.FormModelService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.dto.formmodel.ModelTmpContent;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
import java.util.List;
import static com.thinkit.cms.enums.LogModule.MODEL;

/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/formModel")
@Slf4j
public class FormModelController extends BaseController<FormModelService> {



        @Logs(module = MODEL,operation = "模型查看")
        @GetMapping(value="detail")
        public FormModelDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id){
             return service.detail(id);
        }


        @SaCheckPermission("model")
        @Site
        @PostMapping(value = "/page")
        public PageModel<FormModelDto> list(@RequestBody PageModel<FormModelDto> pageDto) {
             return service.page(pageDto);
        }



        @Logs(module = MODEL,operation = "模型片段查看")
        @SaCheckPermission("fragment")
        @Site
        @PostMapping(value = "/pageFragment")
        public PageModel<FormModelDto> pageFragment(@RequestBody PageModel<FormModelDto> pageDto) {
                return service.pageFragment(pageDto);
        }


        @Logs(module = MODEL,operation = "模型片段保存")
        @SaCheckPermission("fragment:add")
        @Site
        @PostMapping(value="saveFragment")
        public void saveFragment(@Validated(value = {ValidGroup4.class}) @RequestBody FormModelDto formModelDto) {
                service.saveFragment(formModelDto);
        }


        @Sentinel
        @Logs(module = MODEL,operation = "模型片段更新")
        @SaCheckPermission("fragment:edit")
        @Site
        @PostMapping(value="updateFragment")
        public void updateFragment(@Validated(value = {ValidGroup4.class}) @RequestBody FormModelDto formModelDto) {
                service.updateFragment(formModelDto);
        }


        /**
         * 获取文件内容
         * @param formCode
         * @return
         */
        @Logs(module = MODEL,operation = "模型文件内容获取")
        @GetMapping(value="fragmentContent")
        public String fragmentContent(@NotBlank(message = "部件编码 不能为空") String formCode){
                return service.fragmentContent(formCode);
        }

        /**
         * 获取文件内容
         * @param modelTmpContent
         * @return
         */
        @Sentinel
        @Logs(module = MODEL,operation = "模型文件内容更新保存")
        @PutMapping (value="saveFragmentContent")
        public String saveFragmentContent(@Validated @RequestBody ModelTmpContent modelTmpContent){
                return service.saveFragmentContent(modelTmpContent.getFormCode(),modelTmpContent.getContent());
        }


        /**
         * 删除页面片段
         * @param id
         * @return
         */
        @Logs(module = MODEL,operation = "模型删除页面片段")
        @Sentinel
        @SaCheckPermission("fragment:delete")
        @DeleteMapping (value="deleteFragment")
        public void deleteFragment(@NotBlank(message = "页面片段ID不能为空") @RequestParam String id,
                                   @NotBlank(message = "页面片段编码不能为空") @RequestParam String formCode){
                 service.deleteFragment(id,formCode);
        }

        @Site
        @GetMapping(value = "/listModel")
        public List<FormModelDto> listModel(FormModelDto formModelDto) {
                return service.listModel(formModelDto);
        }


        @Logs(module = MODEL,operation = "模型保存")
        @Sentinel
        @SaCheckPermission("model:add")
        @Site
        @PostMapping(value="save")
        public void save(@Validated(value = {ValidGroup4.class}) @RequestBody FormModelDto formModelDto) {
              service.save(formModelDto);
        }


        @Logs(module = MODEL,operation = "模型更新")
        @Sentinel
        @SaCheckPermission("model:edit")
        @PutMapping(value="update")
        public void update(@Validated(value = {ValidGroup4.class}) @RequestBody FormModelDto formModelDto) {
              SiteConf siteConf = getUserCtx().getSite(false);
              if(Checker.BeNull(siteConf)){
                  throw new CustomException(5040);
              }
              String templateId = siteConf.getTemplateId();
              formModelDto.setTemplateId(templateId);
              service.update(formModelDto);
        }


        @Logs(module = MODEL,operation = "模型片段生成")
        @PutMapping(value="genFragment")
        public void genFragment(@NotBlank(message = "编码不能为空") String formModelId) {
                service.genFragment(formModelId);
        }


        @Logs(module = MODEL,operation = "模型删除")
        @Sentinel
        @SaCheckPermission("model:delete")
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

}