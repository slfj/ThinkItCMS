package com.thinkit.cms.boot.entity.modeldesign;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_form_model_config")
public class ModelDesignEntity extends BaseModel  {
       /**
         表单模型主键
       */
        @TableField("form_model_id")
        private String formModelId;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         字段名称
       */
        @TableField("field_name")
        private String fieldName;

       /**
         字段编码
       */
        @TableField("field_code")
        private String fieldCode;

       /**
         字段类型
       */
        @TableField("field_type")
        private String fieldType;

       /**
         提示消息：请输入 xxx
       */
        @TableField("field_placeholder")
        private String fieldPlaceholder;

       /**
         字段默认值（{"val":"[]"}）
       */
        @TableField("field_default_val")
        private String fieldDefaultVal;

       /**
         最大字段长度
       */
        @TableField("length")
        private Integer length;

       /**
         是否必填 1：必填 0：非必填
       */
        @TableField("is_require")
        private Boolean isRequire;

       /**
         选项
       */
        @TableField("options")
        private String options;

       /**
         排序
       */
        @TableField("sort")
        private Integer sort;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;

        /**
         * 是否是系统字段
         */
        @TableField("is_sysfield")
        private String isSysfield;


        /**
         * 是否 索引
         */
        @TableField("is_index")
        private String isIndex;

        /**
         * 索引类型
         */
        @TableField("index_type")
        private String indexType="TEXT";

}