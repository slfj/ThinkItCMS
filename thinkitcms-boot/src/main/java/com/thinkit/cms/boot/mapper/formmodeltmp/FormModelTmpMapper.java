package com.thinkit.cms.boot.mapper.formmodeltmp;
import com.thinkit.cms.dto.formmodeltmp.FormModelTmpDto;
import com.thinkit.cms.model.ModelTemp;
import com.thinkit.cms.model.ModelTempParma;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.formmodeltmp.FormModelTmpEntity;
import org.apache.ibatis.annotations.Param;

/**
* @author LG
*/

@Mapper
public interface FormModelTmpMapper extends BaseMapper<FormModelTmpEntity> {

    /**
     * 获取详情
     * @param modelId
     * @param templateId
     * @param siteId
     * @return
     */
    FormModelTmpDto getDetail(@Param("modelId") String modelId, @Param("templateId") String templateId,@Param("siteId") String siteId);

    /**
     * 更新模板
     * @param modelId
     * @param templateId
     * @param tempPath
     * @param mobileTempPath
     */
    void updateTmp(@Param("modelId") String modelId,
                   @Param("templateId") String templateId,
                   @Param("tempPath") String tempPath,
                   @Param("mobileTempPath") String mobileTempPath,
                   @Param("siteId") String siteId);

    /**
     * 查找是否存在模板
     * @param modelId
     * @param templateId
     * @return
     */
    Integer getHasTemp(@Param("modelId") String modelId,  @Param("templateId")String templateId,@Param("siteId") String siteId);

    /**
     * 查找木板路径
     * @param modelParma
     * @return
     */
    ModelTemp getPaths(@Param("modelParma") ModelTempParma modelParma);
}