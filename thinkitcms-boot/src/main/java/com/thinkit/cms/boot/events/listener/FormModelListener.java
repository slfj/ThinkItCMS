package com.thinkit.cms.boot.events.listener;

import com.thinkit.cms.boot.events.FormModelEvent;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.directive.kit.RedisLuaUtils;
import com.thinkit.cms.dto.formmodel.FormModelDto;
import com.thinkit.cms.model.ModelJson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class FormModelListener extends BaseListener implements ApplicationListener<FormModelEvent> {
    @Override
    public void onApplicationEvent(FormModelEvent event) {
        try {
            if(enable()){
                switch (event.getEventEnum()) {
                    case SAVE_FORM_MODEL,UPDATE_FORM_MODEL -> {
                        FormModelDto formModel = getObj(event);
                        ModelJson modelJson  = getToMap(formModel);
                        RedisLuaUtils.setJson(modelJson,formModel.getFormCode(),SysConst.colon);
                    }
                    case DELETE_FORM_MODEL -> {
                        ModelJson modelJson = getObj(event);
                        RedisLuaUtils.delete(modelJson.getKey());
                    }
                    case SAVE_BATCH_FORM_MODEL -> {
                        List<FormModelDto> formModels = getList(event);
                        formModels.forEach(formModel->{
                            ModelJson modelJson  = getToMap(formModel);
                            RedisLuaUtils.setJson(modelJson,formModel.getFormCode(),SysConst.colon);
                        });
                    }
                }
            }
        }catch (Exception e){
            log.error("FormDataEsListener 执行失败：{}",e.getMessage());
        }
    }
}
