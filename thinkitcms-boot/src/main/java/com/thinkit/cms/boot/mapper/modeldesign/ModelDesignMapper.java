package com.thinkit.cms.boot.mapper.modeldesign;
import com.thinkit.cms.dto.modeldesign.ModelDesignDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.modeldesign.ModelDesignEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author LG
*/

@Mapper
public interface ModelDesignMapper extends BaseMapper<ModelDesignEntity> {


    /**
     * 删除模型设计
     * @param siteId
     * @param modelId
     */
    void deleteDesign(@Param("siteId") String siteId, @Param("modelId") String modelId);

    /**
     * 查找设计详情
     * @param modelId
     * @param siteId
     * @return
     */
    List<ModelDesignDto> detail(@Param("modelId") String modelId, @Param("siteId") String siteId);


    /**
     * 查询模型了下
     * @param siteId
     * @param modelId
     * @return
     */
    String queryFormType(@Param("siteId") String siteId, @Param("modelId") String modelId);
}