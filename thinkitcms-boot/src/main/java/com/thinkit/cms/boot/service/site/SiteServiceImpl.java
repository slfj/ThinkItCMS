package com.thinkit.cms.boot.service.site;

import cn.hutool.extra.spring.SpringUtil;
import com.alicp.jetcache.anno.CacheInvalidate;
import com.alicp.jetcache.anno.CacheType;
import com.alicp.jetcache.anno.Cached;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.thinkit.cms.api.formmodeltmp.FormModelTmpFunService;
import com.thinkit.cms.api.site.SiteDetail;
import com.thinkit.cms.api.site.SiteService;
import com.thinkit.cms.api.sysuser.SysUserService;
import com.thinkit.cms.api.template.TemplateService;
import com.thinkit.cms.boot.entity.site.SiteEntity;
import com.thinkit.cms.boot.events.SiteEvent;
import com.thinkit.cms.boot.jobs.JenSiteTask;
import com.thinkit.cms.boot.jobs.SyncToEsTask;
import com.thinkit.cms.boot.mapper.formmodel.FormModelMapper;
import com.thinkit.cms.boot.mapper.site.SiteMapper;
import com.thinkit.cms.boot.service.template.TemplateServiceImpl;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.core.base.BaseServiceImpl;
import com.thinkit.cms.core.handler.CustomException;
import com.thinkit.cms.directive.emums.ActuatorEnum;
import com.thinkit.cms.directive.kit.Kv;
import com.thinkit.cms.directive.wrapper.ModelAndView;
import com.thinkit.cms.dto.site.SiteDto;
import com.thinkit.cms.dto.template.TemplateDto;
import com.thinkit.cms.enums.EventEnum;
import com.thinkit.cms.model.CmsResult;
import com.thinkit.cms.model.PageModel;
import com.thinkit.cms.user.SiteConf;
import com.thinkit.cms.user.SysUserDetail;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import com.thinkit.cms.utils.CmsConf;
import com.thinkit.cms.utils.CmsFolderUtil;
import com.thinkit.cms.utils.FolderTree;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
* @author LG
*/
@Slf4j
@Service
public class SiteServiceImpl extends BaseServiceImpl<SiteDto,SiteEntity,SiteMapper> implements SiteService{

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private JenSiteTask jenSiteTask;

    @Autowired
    private SyncToEsTask syncToEsTask;

    @Autowired
    private FormModelTmpFunService tmpFunService;
    @Resource
    private FormModelMapper formModelMapper;


    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSiteIds'")
    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSite'")
    @Override
    public void createSite(SiteDto siteDto) {
        checkIsLegal(siteDto);
        initFolder(siteDto);
        super.insert(siteDto);
        publishEvent(new SiteEvent(siteDto, EventEnum.SAVE_CONTENT));
    }

    @Cached(name = "cached::", key = "targetObject.class+'.detail.'+#id" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public SiteDto detail(String id) {
        SiteDto siteDto = super.getByPk(id);
        return siteDto;
    }

    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.detail.'+#siteDto.id")
    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSite'")
    @Override
    public void updateSite(SiteDto siteDto) {
        String key = "cached::"+ TemplateServiceImpl.class +".loadTempPath."+siteDto.getDefTemplateId();
        String redisKey = SysConst.defSiteConf+getSiteId(true);
        redisTemplate.delete(Arrays.asList(key,redisKey));
        checkIsLegal(siteDto);
        super.updateByPk(siteDto);
        publishEvent(new SiteEvent(siteDto, EventEnum.UPDATE_SITE));
    }

    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSite'")
    @Override
    public CmsResult setDefault(String siteId) {
        return sysUserService.setDefaultSite(siteId);
    }

    @Override
    public String getDefault(boolean throwExp) {
        String defSiteId = sysUserService.getDefaultSit(getUserId());
        if(throwExp && Checker.BeBlank(defSiteId)){
             throw new CustomException(5040);
        }
        return defSiteId;
    }

    @Cached(name = "cached::", key = "targetObject.class+'.getDoamin.'+#prefix+'.'+#siteId" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public String getDoamin(String prefix,String siteId) {
        return baseMapper.getDoamin(prefix,siteId);
    }

    @Override
    public String getDefault(boolean throwExp, String userId) {
        String defSiteId = sysUserService.getDefaultSit(userId);
        if(throwExp && Checker.BeBlank(defSiteId)){
            throw new CustomException(5040);
        }
        return defSiteId;
    }

    @Override
    public SiteConf getDefConf(boolean throwExp) {
        String siteId = this.getDefault(throwExp);
        if(Checker.BeNotBlank(siteId)){
            String redisKey = SysConst.defSiteConf+siteId;
            if(redisTemplate.hasKey(redisKey)){
                return (SiteConf) redisTemplate.opsForValue().get(redisKey);
            }else{
                SiteConf siteConf = baseMapper.getConf(siteId);
                if(Checker.BeNotNull(siteConf)){
                    redisTemplate.opsForValue().set(redisKey,siteConf);
                }
                return siteConf;
            }
        }
        return null;
    }


    @Override
    public SiteConf getDefConf(String siteId) {
        if(Checker.BeNotBlank(siteId)){
            String redisKey = SysConst.defSiteConf+siteId;
            if(redisTemplate.hasKey(redisKey)){
                return (SiteConf) redisTemplate.opsForValue().get(redisKey);
            }else{
                SiteConf siteConf = baseMapper.getConf(siteId);
                if(Checker.BeNotNull(siteConf)){
                    redisTemplate.opsForValue().set(redisKey,siteConf);
                }
                return siteConf;
            }
        }
        return null;
    }

    @Override
    public SiteConf getDefConf(boolean throwExp,String userId) {
        String siteId = this.getDefault(throwExp,userId);
        if(Checker.BeNotBlank(siteId)){
            String redisKey = SysConst.defSiteConf+siteId;
            if(redisTemplate.hasKey(redisKey)){
                return (SiteConf) redisTemplate.opsForValue().get(redisKey);
            }else{
                SiteConf siteConf = baseMapper.getConf(siteId);
                if(Checker.BeNotNull(siteConf)){
                    redisTemplate.opsForValue().set(redisKey,siteConf);
                }
                return siteConf;
            }
        }
        return null;
    }

    @Override
    public String getDefaultTemp(boolean throwExp,String siteId) {
        if(Checker.BeNotBlank(siteId)){
            Object obj = redisTemplate.opsForHash().get(SysConst.siteDefConf+siteId,SysConst.templateId);
            if(Checker.BeNotNull(obj)){
                return String.valueOf(obj);
            }else{
                String templateId = baseMapper.getTemplate(siteId);
                if(Checker.BeNotBlank(templateId)){
                    setDefTemplate(templateId,siteId);
                    obj = templateId;
                }
            }
            if(Checker.BeNull(obj) && throwExp){
                throw new CustomException(CmsResult.result(5044));
            }else{
                return String.valueOf(obj);
            }
        }
        return null;
    }

    @Cached(name = "cached::", key = "targetObject.class+'.listSite'" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public CmsResult listSite() {
        List<SiteDto> sites = baseMapper.listSite();
        if(Checker.BeNotEmpty(sites)){
            flagDefSite(sites);
            return CmsResult.result(sites);
        }
        return CmsResult.result(Lists.newArrayList());
    }

    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSite'")
    @Override
    public CmsResult setDefTemplate(String tempId,String siteId) {
        TemplateDto templateDto = templateService.getByPk(tempId);
        if(Checker.BeNull(templateDto)){
            return CmsResult.result(5044);
        }
        String redisKey = SysConst.defSiteConf+siteId;
        redisTemplate.delete(redisKey);
        baseMapper.updateDefTemplate(siteId,tempId);
        Map<String,Object> objectMap = new HashMap<>();
        objectMap.put(SysConst.templateId,tempId);
        redisTemplate.opsForHash().putAll(SysConst.siteDefConf+siteId,objectMap);
        return CmsResult.result();
    }

    @Override
    public CmsResult getDefTemplate() {
        String siteId = getDefault(false);
        if(Checker.BeNotBlank(siteId)){
            Object obj = redisTemplate.opsForHash().get(SysConst.siteDefConf+siteId,SysConst.templateId);
            if(Checker.BeNotNull(obj)){
                String templateId = String.valueOf(obj);
                FolderTree folderTree = templateService.loadTempFolder(templateId);
                return CmsResult.result(folderTree);
            }else{
                String templateId =baseMapper.getTemplate(siteId);
                if(Checker.BeNotBlank(templateId)){
                    setDefTemplate(templateId,siteId);
                    FolderTree folderTree = templateService.loadTempFolder(templateId);
                    return CmsResult.result(folderTree);
                }
            }
        }
        return CmsResult.result();
    }

    @Override
    public PageModel<SiteDto> page(PageModel<SiteDto> pageDto) {
        IPage page = new Page(pageDto.getCurrent(),pageDto.getPageSize());
        IPage<SiteDto> result  = baseMapper.page(page,pageDto.getDto());
        flagDefSite(result.getRecords());
        return  new PageModel(result);
    }

    private void flagDefSite(List<SiteDto> sites){
        String siteId = getDefault(false);
        if(Checker.BeNotBlank(siteId) && Checker.BeNotEmpty(sites)){
            sites.forEach(site->{
                site.setDefsite(site.getId().equals(siteId));
                String http=CmsConf.getAsBoolean(SysConst.enableSSL)?SysConst.https:SysConst.http;
                site.setPcIndex(CmsFolderUtil.appendFolder(http,site.getDomain(),SysConst.prefix,site.getDir(),
                SysConst.prefix,SysConst.home));
                if(Checker.BeNotBlank(site.getMobileDir())){
                    site.setMobileIndex(CmsFolderUtil.appendFolder(http,site.getDomain(),SysConst.prefix,site.getMobileDir(),
                    SysConst.prefix,SysConst.home));
                }
            });
        }
    }

    @Override
    public CmsResult loadTempFolder() {
        return getDefTemplate();
    }

    @Cached(name = "cached::", key = "targetObject.class+'.listSiteIds'" ,expire = 7200, cacheType = CacheType.REMOTE)
    @Override
    public Set<String> listSiteIds() {
        Set<String> ids = baseMapper.listSiteIds();
        return Checker.BeNotEmpty(ids)?ids:new HashSet<>();
    }

    @Override
    public void updateTmpSetNull(String id) {
        baseMapper.updateTmpSetNull(id);
    }

    @Override
    public String createSiteFinish(String siteId) {
          return jenSiteTask.genDoTask(siteId,getUserCtx());
    }

    @Override
    public String syncToEs(String siteId) {
        return syncToEsTask.esTask(siteId,getUserCtx());
    }

    @Override
    public List<String> listDomain() {
        List<String> domains = baseMapper.listDomain();
        return Checker.BeNotEmpty(domains)?domains:Lists.newArrayList();
    }


    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSiteIds'")
    @CacheInvalidate(name = "cached::", key = "targetObject.class+'.listSite'")
    @Override
    public boolean deleteSite(String id) {
        return super.deleteByPk(id);
    }

    @Override
    public void jonIndex(String siteId) {
        SiteService siteService = SpringUtil.getBean(SiteService.class);
        if(Checker.BeNotNull(siteService)){
            String formModelId = formModelMapper.hasIndexModel(siteId,"4");
            if(Checker.BeBlank(formModelId)){
                log.info("站点:{} 未设置首页模型不进行生成",siteId);
                 return;
            }
            SiteDto siteDto = siteService.detail(siteId);
            String templateId = siteDto.getDefTemplateId();
            if(Checker.BeBlank(templateId)){
                log.info("站点:{} 未设置默认模板不进行生成",siteId);
                return;
            }
            User user = new SiteDetail();
            SiteConf siteConf = user.getSite(siteId);
            user.setSiteConf(siteConf);
            Kv kv = Kv.create().setIfNotNull(SysConst.siteId,siteId);
            ModelAndView modelAndView = ModelAndView.create(user,siteConf,siteId,templateId).addParam(SysConst.site,siteDto).
            addActuator(ActuatorEnum.INDEX,formModelId,tmpFunService,kv).finished();
            modelAndView.execute(true);
        }
    }


    /**
     * 校验是否合法
     */
    private void checkIsLegal(SiteDto siteDto){
        Integer count = baseMapper.count(siteDto.getId(),siteDto.getDomain());
        if(count>0){
            throw new CustomException(CmsResult.result(5036));
        }
        Integer codeCount = baseMapper.hasCode(siteDto.getId(),siteDto.getCode());
        if(codeCount>0){
            throw new CustomException(CmsResult.result(5051));
        }
    }

    private void initFolder(SiteDto siteDto){
        try {
            String siteFolder = CmsFolderUtil.initSiteFolder(siteDto.getDomain());
            CmsFolderUtil.create(true,siteFolder,siteDto.getDir());
            CmsFolderUtil.create(false,siteFolder,siteDto.getMobileDir());
        } catch (IOException e) {
            throw new CustomException(CmsResult.result(5037));
        }
    }
}