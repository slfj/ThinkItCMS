package com.thinkit.cms.boot.jobs.executers;

import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;

public class EsExecuter extends Executer {

    public EsExecuter(User user, String siteId, String taskId, TaskEnum taskEnum){
         super(user,siteId,taskId,taskEnum);
    }
    @Override
    public void run() {
        contentService.syncEsData(taskId,siteId,null);
    }
}
