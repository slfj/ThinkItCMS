package com.thinkit.cms.boot.entity.sysdict;
import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;
import lombok.experimental.Accessors;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import com.baomidou.mybatisplus.annotation.TableName;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_sys_dict")
public class SysDictEntity extends BaseModel  {
       /**
         父级主键
       */
        @TableField("pid")
        private String pid;

       /**
         标签名
       */
        @TableField("name")
        private String name;

       /**
         数据值
       */
        @TableField("value")
        private String value;

       /**
         类型
       */
        @TableField("type")
        private String type;

       /**
         描述
       */
        @TableField("description")
        private String description;

       /**
         排序（升序）
       */
        @TableField("num")
        private Integer num;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}