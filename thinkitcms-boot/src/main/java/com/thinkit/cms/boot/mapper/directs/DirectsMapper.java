package com.thinkit.cms.boot.mapper.directs;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.thinkit.cms.dto.directs.DirectsDto;
import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.thinkit.cms.boot.entity.directs.DirectsEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author lg
*/

@Mapper
public interface DirectsMapper extends BaseMapper<DirectsEntity> {


    /**
     * 查询count
     * @param id
     * @param field
     * @param val
     * @return
     */
    int count(@Param("id") String id,@Param("field") String field, @Param("val") String val);


    /**
     * 查询列表
     * @param page
     * @param dto
     * @param siteId
     * @return
     */
    IPage<DirectsDto> page(IPage page, @Param("dto") DirectsDto dto,@Param("siteId") String siteId);


    /**
     * 启用禁用
     * @param id
     * @param enable
     */
    void enable(@Param("id") String id, @Param("enable") Integer enable);


    /**
     * 加载当前站点的指令
     * @param siteId
     * @return
     */
    List<DirectsDto> loadDirects(@Param("siteId") String siteId);
}