package com.thinkit.cms.boot.config;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.json.JSONUtil;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
@Slf4j
@Component
public class ActuatorFilter extends OncePerRequestFilter {
    private static final String PREFIX = "/actuator";
    private static final String OPTIONS = "OPTIONS";
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(!OPTIONS.equals(request.getMethod())){
            boolean isActuator = request.getRequestURI().startsWith(PREFIX);
            if(isActuator){
                String token =  request.getHeader(SysConst.authorization);
                if(Checker.BeBlank(token)){
                    setResponse(response);
                    return;
                }else{
                   try {
                       Object idObj =  StpUtil.getLoginIdByToken(token);
                       if(Checker.BeNull(idObj)){
                           setResponse(response);
                           return;
                       }
                   }catch (Exception e){
                       log.error("异常：{}",e.getMessage());
                       setResponse(response);
                       return;
                   }
                }
            }
        }
        filterChain.doFilter(request,response);
    }

    private void setResponse(HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(JSONUtil.toJsonStr(response()));
    }

    private String response(){
        Map<String,Object> res = new HashMap<>();
        res.put("code",4013);
        res.put("data",null);
        res.put("msg","请登录后访问");
        return JSONUtil.toJsonStr(res);
    }
}
