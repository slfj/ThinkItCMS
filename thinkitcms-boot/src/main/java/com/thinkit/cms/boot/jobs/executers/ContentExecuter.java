package com.thinkit.cms.boot.jobs.executers;

import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.content.CkRow;
import com.thinkit.cms.dto.content.RowsDto;
import com.thinkit.cms.enums.TaskEnum;
import com.thinkit.cms.user.User;
import com.thinkit.cms.utils.Checker;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.CountDownLatch;

@Slf4j
public class ContentExecuter extends Executer {

    public ContentExecuter(User user, String siteId, String taskId, TaskEnum taskEnum, CountDownLatch countDownLatch){
         super(user,siteId,taskId,taskEnum,countDownLatch);
    }
    @Override
    public void run() {
        List<CkRow> ckRowList =  contentService.getContents(siteId,null);
        taskService.upProgress(taskId, SysConst.count,ckRowList.size());
        if(Checker.BeNotEmpty(ckRowList)){
            RowsDto rowsDto = new RowsDto();
            rowsDto.setSiteId(siteId);
            rowsDto.setCkRowList(ckRowList);
            rowsDto.setUser(user);
            rowsDto.setAsync(true);
           try {
               contentService.genContent(rowsDto,taskId);
           }catch (Exception e){
              log.error("生成异常：{}",e.getMessage());
           }finally {
               countDownLatch.countDown();
           }
        }
    }
}
