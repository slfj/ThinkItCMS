package com.thinkit.cms.boot.entity.category;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.thinkit.cms.model.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
* @author LG
*/
@Getter
@Setter
@Accessors(chain = true)
@TableName("tk_category")
public class CategoryEntity extends BaseModel {
       /**
         名称
       */
        @TableField("name")
        private String name;

       /**
         父分类ID
       */
        @TableField("pid")
        private String pid;

       /**
         站点ID
       */
        @TableField("site_id")
        private String siteId;

       /**
         编码
       */
        @TableField("code")
        private String code;

       /**
         封面图片
       */
        @TableField("cover")
        private String cover;

       /**
         生成路径规则
       */
        @TableField("path_rule")
        private String pathRule;

       /**
         模板路径
       */
        @TableField(exist = false)
        private String tmpPath;

       /**
         手机端模板路径
       */
       @TableField(exist = false)
        private String mobileTmpPath;

       /**
         外链跳转地址
       */
        @TableField("go_url")
        private String goUrl;


        /**
         地址
         */
        @TableField("url")
        private String url;


        /**
         * 手机端地址
         */
        @TableField("m_url")
        private String mUrl;

       /**
         每页数据条数
       */
        @TableField("page_size")
        private Integer pageSize;

       /**
         允许投稿
       */
        @TableField("allow_create")
        private Integer allowCreate;

       /**
         顺序
       */
        @TableField("sort")
        private Integer sort;

       /**
         是否在首页隐藏
       */
        @TableField("hidden")
        private Integer hidden;

       /**
         栏目扩展模型ID
       */
        @TableField("form_model_id")
        private String formModelId;

        /**
         模型表 code
         */
        @TableField("form_model_code")
        private String formModelCode;

       /**
         是否是单页
       */
        @TableField("single_page")
        private Integer singlePage;

       /**
         标题(分类标题用于 SEO 关键字优化)
       */
        @TableField("title")
        private String title;

       /**
         关键词用于 SEO 关键字优化
       */
        @TableField("keywords")
        private String keywords;

       /**
         描述用于 SEO 关键字优化
       */
        @TableField("description")
        private String description;

       /**
         是否包含字内容
       */
        @TableField("contain_child")
        private Integer containChild;

       /**
         是否删除 0：正常 1：删除
       */
        @TableField("deleted")
        private Boolean deleted;


}