package com.thinkit.cms.boot.jobs;

import com.thinkit.cms.api.content.ContentService;
import com.thinkit.cms.boot.config.RedisListener;
import com.thinkit.cms.constant.SysConst;
import com.thinkit.cms.dto.content.RowsDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RedisPublishTask extends RedisListener {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private ContentService contentService;

    @Override
    public boolean isFit(String expiredKey) {
        return expiredKey.startsWith(SysConst.expiredContent);
    }

    @Override
    public void doIt(String expiredKey) {
        String key = expiredKey+SysConst.expKey;
        try {
            RowsDto rowsDto = (RowsDto) redisTemplate.opsForValue().get(key);
            contentService.asyncPublish(rowsDto);
        }catch (Exception e){
            log.error("key 失效监听异常：{}",e.getMessage());
        }finally {
            redisTemplate.delete(key);
        }
    }
}
