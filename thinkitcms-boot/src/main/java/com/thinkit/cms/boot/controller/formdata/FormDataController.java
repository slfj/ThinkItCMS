package com.thinkit.cms.boot.controller.formdata;
import com.thinkit.cms.api.formdata.FormDataService;
import com.thinkit.cms.authen.annotation.Sentinel;
import com.thinkit.cms.core.base.BaseController;
import com.thinkit.cms.dto.formdata.FormDataDto;
import com.thinkit.cms.model.PageModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotBlank;
/**
* @author LG
*/

@Validated
@RestController
@RequestMapping("/api/formData")
@Slf4j
public class FormDataController extends BaseController<FormDataService> {


        @GetMapping(value="detail")
        public FormDataDto detail(@NotBlank(message = "ID不能为空!") @RequestParam String id,
              @RequestParam(required = false,defaultValue = "false") Boolean isExtra){
             return service.detail(id,isExtra);
        }

        @PostMapping(value = "/page")
        public PageModel<FormDataDto> page(@RequestBody PageModel<FormDataDto> pageDto) {
             return service.listPage(pageDto);
        }

        @PostMapping(value="saveExtra")
        public void saveExtra(@Validated @RequestBody FormDataDto formDataDto) {
            service.saveExtra(formDataDto);
        }

        @PostMapping(value="updateExtra")
        public void updateExtra(@Validated @RequestBody FormDataDto formDataDto) {
            service.updateExtra(formDataDto);
        }

        @PostMapping(value="save")
        public void save(@Validated @RequestBody FormDataDto formDataDto) {
              service.insert(formDataDto);
        }

        @PutMapping(value="update")
        public void update(@Validated @RequestBody FormDataDto formDataDto) {
              service.updateByPk(formDataDto);
        }


    @Sentinel
        @DeleteMapping(value="delete")
        public boolean deleteByPk(@NotBlank @RequestParam String id){
                return service.delete(id);
        }

}